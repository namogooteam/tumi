package de.passsy.holocircularprogressbar;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.RectF;
import android.os.Build;
import android.os.Bundle;
import android.os.Parcelable;
import android.util.AttributeSet;
import android.util.DisplayMetrics;
import android.view.Gravity;
import android.view.View;

import com.incipio.tumiandroid.R;

public class HoloCircularProgressBar extends View {
    private static final String INSTNACE_STATE_MARKER_PROGRESS = "marker_progress";
    private static final String INSTNACE_STATE_PROGRESS = "progress";
    private static final String INSTNACE_STATE_PROGRESS_BACKGROUND_COLOR = "progress_background_color";
    private static final String INSTNACE_STATE_PROGRESS_COLOR = "progress_color";
    private static final String INSTNACE_STATE_SAVEDSTATE = "saved_state";
    private static final String TAG = HoloCircularProgressBar.class.getSimpleName();
    private Paint mBackgroundColorPaint;
    private final RectF mCircleBounds;
    private int mCircleStrokeWidth;
    private final int mGravity;
    private int mHorizontalInset;
    private boolean mIsInitializing;
    private boolean mIsMarkerEnabled;
    private boolean mIsThumbEnabled;
    private Paint mMarkerColorPaint;
    private float mMarkerProgress;
    private boolean mOverrdraw;
    private float mProgress;
    private int mProgressBackgroundColor;
    private int mProgressColor;
    private Paint mProgressColorPaint;
    private float mRadius;
    private final RectF mSquareRect;
    private Paint mThumbColorPaint;
    private float mThumbPosX;
    private float mThumbPosY;
    private int mThumbRadius;
    private float mTranslationOffsetX;
    private float mTranslationOffsetY;
    private int mVerticalInset;

    public HoloCircularProgressBar(Context context) {
        this(context, (AttributeSet) null);
    }

    public HoloCircularProgressBar(Context context, AttributeSet attrs) {
        this(context, attrs, R.attr.circularProgressBarStyle);
    }

    public HoloCircularProgressBar(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        int wheelSize;
        this.mIsInitializing = true;
        this.mBackgroundColorPaint = new Paint();
        this.mCircleStrokeWidth = 8;
        this.mThumbRadius = 0;
        this.mCircleBounds = new RectF();
        this.mProgress = 0.3f;
        this.mThumbColorPaint = new Paint();
        this.mMarkerProgress = 0.0f;
        this.mIsMarkerEnabled = false;
        this.mHorizontalInset = 0;
        this.mVerticalInset = 0;
        this.mOverrdraw = false;
        this.mSquareRect = new RectF();
        this.mIsThumbEnabled = true;
        DisplayMetrics displaymetrics = new DisplayMetrics();
        ((Activity) context).getWindowManager().getDefaultDisplay().getMetrics(displaymetrics);
        int i = displaymetrics.heightPixels;
        int i2 = displaymetrics.widthPixels;
        int density = displaymetrics.densityDpi;
        if (density <= 120) {
            wheelSize = 18;
        } else if (density <= 160) {
            wheelSize = 20;
        } else if (density <= 240) {
            wheelSize = 25;
        } else if (density <= 320) {
            wheelSize = 35;
        } else if (density <= 480) {
            wheelSize = 50;
        } else if (density <= 640) {
            wheelSize = 55;
        } else {
            wheelSize = 55;
        }
        TypedArray attributes = context.obtainStyledAttributes(attrs, R.styleable.HoloCircularProgressBar, defStyle, 0);
        setProgressColor(attributes.getColor(4, -16711681));
        setProgressBackgroundColor(attributes.getColor(5, Color.parseColor("#ededed")));
        setProgress(attributes.getFloat(2, 0.0f));
        setMarkerProgress(attributes.getFloat(3, 0.0f));
        setWheelSize((int) attributes.getDimension(1, (float) wheelSize));
        this.mIsThumbEnabled = attributes.getBoolean(6, false);
        this.mIsMarkerEnabled = attributes.getBoolean(7, true);
        this.mGravity = attributes.getInt(0, 17);
        attributes.recycle();
        this.mThumbRadius = (int) (((double) this.mCircleStrokeWidth) * 0.5d);
        updateBackgroundColor();
        updateMarkerColor();
        updateProgressColor();
        this.mIsInitializing = false;
    }

    @SuppressLint({"NewApi"})
    private void computeInsets(int dx, int dy) {
        int absoluteGravity = this.mGravity;
        if (Build.VERSION.SDK_INT >= 16) {
            absoluteGravity = Gravity.getAbsoluteGravity(this.mGravity, getLayoutDirection());
        }
        switch (absoluteGravity & 7) {
            case 3:
                this.mHorizontalInset = 0;
                break;
            case 5:
                this.mHorizontalInset = dx;
                break;
            default:
                this.mHorizontalInset = dx / 2;
                break;
        }
        switch (absoluteGravity & 112) {
            case 48:
                this.mVerticalInset = 0;
                return;
            case 80:
                this.mVerticalInset = dy;
                return;
            default:
                this.mVerticalInset = dy / 2;
                return;
        }
    }

    private float getCurrentRotation() {
        return 360.0f * this.mProgress;
    }

    public float getMarkerProgress() {
        return this.mMarkerProgress;
    }

    private float getMarkerRotation() {
        return 360.0f * this.mMarkerProgress;
    }

    public float getProgress() {
        return this.mProgress;
    }

    public int getProgressColor() {
        return this.mProgressColor;
    }

    public boolean isMarkerEnabled() {
        return this.mIsMarkerEnabled;
    }

    public boolean isThumbEnabled() {
        return this.mIsThumbEnabled;
    }

    /* access modifiers changed from: protected */
    public void onDraw(Canvas canvas) {
        canvas.translate(this.mTranslationOffsetX, this.mTranslationOffsetY);
        float progressRotation = getCurrentRotation();
        if (!this.mOverrdraw) {
            canvas.drawArc(this.mCircleBounds, 270.0f, -(360.0f - progressRotation), false, this.mBackgroundColorPaint);
        }
        canvas.drawArc(this.mCircleBounds, 270.0f, this.mOverrdraw ? 360.0f : progressRotation, false, this.mProgressColorPaint);
        if (this.mIsMarkerEnabled) {
            float markerRotation = getMarkerRotation();
            canvas.save();
            canvas.rotate(markerRotation - 90.0f);
            canvas.restore();
        }
        if (isThumbEnabled()) {
            canvas.save();
            canvas.rotate(progressRotation - 90.0f);
            canvas.rotate(45.0f, this.mThumbPosX, this.mThumbPosY);
            this.mSquareRect.left = this.mThumbPosX - ((float) (this.mThumbRadius / 3));
            this.mSquareRect.right = this.mThumbPosX + ((float) (this.mThumbRadius / 3));
            this.mSquareRect.top = this.mThumbPosY - ((float) (this.mThumbRadius / 3));
            this.mSquareRect.bottom = this.mThumbPosY + ((float) (this.mThumbRadius / 3));
            canvas.drawRect(this.mSquareRect, this.mThumbColorPaint);
            canvas.restore();
        }
    }

    /* access modifiers changed from: protected */
    public void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
        int height = getDefaultSize(getSuggestedMinimumHeight(), heightMeasureSpec);
        int width = getDefaultSize(getSuggestedMinimumWidth(), widthMeasureSpec);
        int min = Math.min(width, height);
        setMeasuredDimension(min, height);
        float halfWidth = ((float) min) * 0.2f;
        this.mRadius = halfWidth - ((float) this.mThumbRadius);
        this.mCircleBounds.set(-this.mRadius, -this.mRadius, this.mRadius, this.mRadius);
        this.mThumbPosX = (float) (((double) this.mRadius) * Math.cos(0.0d));
        this.mThumbPosY = (float) (((double) this.mRadius) * Math.sin(0.0d));
        computeInsets(width - min, height - min);
        this.mTranslationOffsetX = ((float) this.mHorizontalInset) + halfWidth;
        this.mTranslationOffsetY = ((float) this.mVerticalInset) + halfWidth;
    }

    /* access modifiers changed from: protected */
    public void onRestoreInstanceState(Parcelable state) {
        if (state instanceof Bundle) {
            Bundle bundle = (Bundle) state;
            setProgress(bundle.getFloat("progress"));
            setMarkerProgress(bundle.getFloat(INSTNACE_STATE_MARKER_PROGRESS));
            int progressColor = bundle.getInt(INSTNACE_STATE_PROGRESS_COLOR);
            if (progressColor != this.mProgressColor) {
                this.mProgressColor = progressColor;
                updateProgressColor();
            }
            int progressBackgroundColor = bundle.getInt(INSTNACE_STATE_PROGRESS_BACKGROUND_COLOR);
            if (progressBackgroundColor != this.mProgressBackgroundColor) {
                this.mProgressBackgroundColor = progressBackgroundColor;
                updateBackgroundColor();
            }
            super.onRestoreInstanceState(bundle.getParcelable(INSTNACE_STATE_SAVEDSTATE));
            return;
        }
        super.onRestoreInstanceState(state);
    }

    /* access modifiers changed from: protected */
    public Parcelable onSaveInstanceState() {
        Bundle bundle = new Bundle();
        bundle.putParcelable(INSTNACE_STATE_SAVEDSTATE, super.onSaveInstanceState());
        bundle.putFloat("progress", this.mProgress);
        bundle.putFloat(INSTNACE_STATE_MARKER_PROGRESS, this.mMarkerProgress);
        bundle.putInt(INSTNACE_STATE_PROGRESS_COLOR, this.mProgressColor);
        bundle.putInt(INSTNACE_STATE_PROGRESS_BACKGROUND_COLOR, this.mProgressBackgroundColor);
        return bundle;
    }

    public void setMarkerEnabled(boolean enabled) {
        this.mIsMarkerEnabled = enabled;
    }

    public void setMarkerProgress(float progress) {
        this.mIsMarkerEnabled = true;
        this.mMarkerProgress = progress;
    }

    public void setProgress(float progress) {
        if (progress != this.mProgress) {
            if (progress == 1.0f) {
                this.mOverrdraw = false;
                this.mProgress = 1.0f;
            } else {
                if (progress >= 1.0f) {
                    this.mOverrdraw = true;
                } else {
                    this.mOverrdraw = false;
                }
                this.mProgress = progress % 1.0f;
            }
            if (!this.mIsInitializing) {
                invalidate();
            }
        }
    }

    public void setProgressBackgroundColor(int color) {
        this.mProgressBackgroundColor = color;
        updateMarkerColor();
        updateBackgroundColor();
    }

    public void setProgressColor(int color) {
        this.mProgressColor = color;
        updateProgressColor();
    }

    public void setThumbEnabled(boolean enabled) {
        this.mIsThumbEnabled = enabled;
    }

    public void setWheelSize(int dimension) {
        this.mCircleStrokeWidth = dimension;
    }

    private void updateBackgroundColor() {
        this.mBackgroundColorPaint = new Paint(1);
        this.mBackgroundColorPaint.setColor(this.mProgressBackgroundColor);
        this.mBackgroundColorPaint.setStyle(Paint.Style.STROKE);
        this.mBackgroundColorPaint.setStrokeWidth((float) this.mCircleStrokeWidth);
        invalidate();
    }

    private void updateMarkerColor() {
        this.mMarkerColorPaint = new Paint(1);
        this.mMarkerColorPaint.setColor(this.mProgressBackgroundColor);
        this.mMarkerColorPaint.setStyle(Paint.Style.STROKE);
        this.mMarkerColorPaint.setStrokeWidth((float) (this.mCircleStrokeWidth / 2));
        invalidate();
    }

    private void updateProgressColor() {
        this.mProgressColorPaint = new Paint(1);
        this.mProgressColorPaint.setColor(this.mProgressColor);
        this.mProgressColorPaint.setStyle(Paint.Style.STROKE);
        this.mProgressColorPaint.setStrokeWidth((float) this.mCircleStrokeWidth);
        this.mThumbColorPaint = new Paint(1);
        this.mThumbColorPaint.setColor(this.mProgressColor);
        this.mThumbColorPaint.setStyle(Paint.Style.FILL_AND_STROKE);
        this.mThumbColorPaint.setStrokeWidth((float) this.mCircleStrokeWidth);
        invalidate();
    }
}
