package com.incipio.tumiandroid.activities;

import android.app.Activity;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.view.View;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ProgressBar;
import com.incipio.tumiandroid.R;

public class WebPageActivity extends Activity {
    private Handler handler = new Handler() {
        public void handleMessage(Message msg) {
        }
    };
    /* access modifiers changed from: private */
    public WebView mWebView;
    private ProgressBar pro;
    /* access modifiers changed from: private */
    public ProgressBar progress;
    /* access modifiers changed from: private */
    public Button refresh;
    private String url = "";

    /* access modifiers changed from: protected */
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.web_layout);
        String text = getIntent().getExtras().getString("PageUrl");
        if (text.equalsIgnoreCase("CustomerService")) {
            this.url = getResources().getString(R.string.customer_service);
        } else if (text.equalsIgnoreCase("About")) {
            this.url = getResources().getString(R.string.about_tumi);
        } else if (text.equalsIgnoreCase("StoreLocator")) {
            this.url = getResources().getString(R.string.store_locator);
        } else {
            this.url = getResources().getString(R.string.shop_tumi);
        }
        this.mWebView = (WebView) findViewById(R.id.webView1);
        this.mWebView.getSettings().setJavaScriptEnabled(true);
        this.mWebView.setInitialScale(100);
        this.mWebView.getSettings().setBuiltInZoomControls(true);
        this.mWebView.setWebViewClient(new HelloWebViewClient());
        this.mWebView.loadUrl(this.url);
        ((Button) findViewById(R.id.button_close)).setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                WebPageActivity.this.finish();
            }
        });
        ((ImageView) findViewById(R.id.button_back)).setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                WebPageActivity.this.mWebView.goBack();
            }
        });
        ((ImageView) findViewById(R.id.button_farward)).setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                WebPageActivity.this.mWebView.goForward();
            }
        });
        this.progress = (ProgressBar) findViewById(R.id.progressBar1);
        this.progress.setVisibility(0);
        this.refresh = (Button) findViewById(R.id.button_refresh);
        this.refresh.setVisibility(8);
        this.refresh.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                WebPageActivity.this.mWebView.clearFormData();
                WebPageActivity.this.refresh.setVisibility(8);
                WebPageActivity.this.mWebView.reload();
            }
        });
        findViewById(R.id.back).setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                WebPageActivity.this.finish();
            }
        });
    }

    private class HelloWebViewClient extends WebViewClient {
        private HelloWebViewClient() {
        }

        public void onPageStarted(WebView view, String url, Bitmap favicon) {
            WebPageActivity.this.progress.setVisibility(View.VISIBLE);
            WebPageActivity.this.refresh.setVisibility(View.GONE);
            super.onPageStarted(view, url, favicon);
        }

        public void onLoadResource(WebView view, String url) {
            super.onLoadResource(view, url);
        }

        public void onPageFinished(WebView view, String url) {
            super.onPageFinished(view, url);
            WebPageActivity.this.progress.setVisibility(View.GONE);
            WebPageActivity.this.refresh.setVisibility(View.VISIBLE);
        }
    }
}
