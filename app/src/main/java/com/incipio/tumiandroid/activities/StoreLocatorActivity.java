package com.incipio.tumiandroid.activities;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentSender;
import android.location.Location;
import android.location.LocationManager;
import android.net.Uri;
import android.net.http.Headers;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.TranslateAnimation;
import android.widget.AdapterView;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.fragment.app.FragmentActivity;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GooglePlayServicesUtil;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.location.LocationListener;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.incipio.tumiandroid.R;
import com.incipio.tumiandroid.adapters.MenuListViewAdapter;
import com.incipio.tumiandroid.audio.activities.AmbientSoundsActivity;
import com.incipio.tumiandroid.audio.activities.SelectDeviceActivity;
import com.incipio.tumiandroid.handelers.StoreListHandeler;
import com.incipio.tumiandroid.tumibank.activities.DeviceScanActivity;
import com.incipio.tumiandroid.utils.AnimParams;
import com.incipio.tumiandroid.utils.Check_Internetconnection;
import com.incipio.tumiandroid.utils.FlipAnimation;
import com.incipio.tumiandroid.utils.JSONParser;
import com.incipio.tumiandroid.utils.StoreDetails;
import java.net.URL;
import java.text.DecimalFormat;
import java.util.ArrayList;
import javax.xml.parsers.SAXParserFactory;
//import kaaes.spotify.webapi.samplesearch.AddAlbumFragment1;
import org.json.JSONException;
import org.json.JSONObject;
import org.xml.sax.InputSource;
import org.xml.sax.XMLReader;

public class StoreLocatorActivity extends FragmentActivity implements View.OnClickListener, LocationListener, GoogleApiClient.ConnectionCallbacks, GoogleApiClient.OnConnectionFailedListener, Animation.AnimationListener {
    private static final int CONNECTION_FAILURE_RESOLUTION_REQUEST = 9000;
    protected static final int REQUEST_CHECK_SETTINGS = 1;
    private LinearLayout ambient_sounds_ll;
    private AnimParams animParams = new AnimParams();
    private Button back;
    private LinearLayout backLayout_operations;
    private View cardBack;
    private View cardFace;
    /* access modifiers changed from: private */
    public Check_Internetconnection check_Internetconnection;
    private ImageView current_address_img;
    private String current_lat;
    private String current_longi;
    /* access modifiers changed from: private */
    public ProgressDialog dialog;
    private FlipAnimation flipAnimation;
    /* access modifiers changed from: private */
    public DecimalFormat form;
    private RelativeLayout frontLayout_operations;
    private TextView header;
    private boolean isInbackground = false;
    /* access modifiers changed from: private */
    public boolean isMenuSelected = false;
    /* access modifiers changed from: private */
    public ListView locations_listview;
    private GoogleApiClient mGoogleApiClient;
    private LocationRequest mLocationRequest;
    /* access modifiers changed from: private */
    public GoogleMap mMap;
    private ImageView menu;
    private ListView menuListView;
    private boolean menuOut = false;
    private LinearLayout play_music_ll;
    private View rootLayout;
    /* access modifiers changed from: private */
    public EditText search_et;
    private TextView search_tv;
    /* access modifiers changed from: private */
    public int selectedPositionFromMenu;
    private TextView settings;

    /* access modifiers changed from: protected */
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.store_locator);
        topBarInit();
        BottomBarInit();
        this.backLayout_operations = (LinearLayout) findViewById(R.id.backLayout_operations);
        this.frontLayout_operations = (RelativeLayout) findViewById(R.id.frontLayout_operations);
        this.menuListView = (ListView) findViewById(R.id.menuListView);
        this.menuListView.setAdapter(new MenuListViewAdapter(this, 0));
        this.menuListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            public void onItemClick(AdapterView<?> adapterView, View arg1, int position, long arg3) {
                boolean unused = StoreLocatorActivity.this.isMenuSelected = true;
                int unused2 = StoreLocatorActivity.this.selectedPositionFromMenu = position;
                StoreLocatorActivity.this.startAnimation();
            }
        });
        this.search_et = (EditText) findViewById(R.id.search_et);
        this.search_tv = (TextView) findViewById(R.id.search_tv);
        this.locations_listview = (ListView) findViewById(R.id.locations_listview);
        this.current_address_img = (ImageView) findViewById(R.id.current_address_img);
        this.search_tv.setOnClickListener(this);
        this.current_address_img.setOnClickListener(this);
        this.rootLayout = findViewById(R.id.main_activity_root);
        this.cardFace = findViewById(R.id.main_activity_card_face);
        this.cardBack = findViewById(R.id.main_activity_card_back);
        this.check_Internetconnection = new Check_Internetconnection(this);
        this.form = new DecimalFormat("##.0");
        setUpMapIfNeeded();
        this.mMap.setOnInfoWindowClickListener(new GoogleMap.OnInfoWindowClickListener() {
            public void onInfoWindowClick(Marker marker) {
                String lat = String.valueOf(marker.getPosition().latitude);
                String lng = String.valueOf(marker.getPosition().longitude);
                Intent intent = new Intent("android.intent.action.VIEW");
                intent.setData(Uri.parse("geo:0,0?q=" + lat + ", " + lng));
                try {
                    StoreLocatorActivity.this.startActivity(intent);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        });
        this.mGoogleApiClient = new GoogleApiClient.Builder(this).addConnectionCallbacks(this).addOnConnectionFailedListener(this).addApi(LocationServices.API).build();
        this.mLocationRequest = LocationRequest.create().setPriority(102).setInterval(3600000).setFastestInterval(60000);
        int status = GooglePlayServicesUtil.isGooglePlayServicesAvailable(getBaseContext());
        if (status != 0) {
            GooglePlayServicesUtil.getErrorDialog(status, this, 10).show();
        } else if (!((LocationManager) getSystemService(Headers.LOCATION)).isProviderEnabled("gps")) {
            buildAlertMessageNoGps();
        }
    }

    /* access modifiers changed from: protected */
    public void onResume() {
        super.onResume();
        this.mGoogleApiClient.connect();
        if (this.check_Internetconnection.isConnectingToInternet()) {
            new LatLangAsynckTask().execute(new String[]{getString(R.string.lat_lang_url) + "&address=92605"});
            return;
        }
        Toast.makeText(getApplicationContext(), getResources().getString(R.string.no_network), 0).show();
    }

    /* access modifiers changed from: protected */
    public void onPause() {
        super.onPause();
        if (this.mGoogleApiClient.isConnected()) {
            LocationServices.FusedLocationApi.removeLocationUpdates(this.mGoogleApiClient, (LocationListener) this);
            this.mGoogleApiClient.disconnect();
        }
    }

    private void flipCard() {
        this.flipAnimation = new FlipAnimation(this.cardFace, this.cardBack);
        if (this.cardFace.getVisibility() == 8) {
            this.flipAnimation.reverse();
        }
        this.rootLayout.startAnimation(this.flipAnimation);
    }

    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.play_music_ll /*2131558546*/:
                startActivity(new Intent(this, SelectDeviceActivity.class));
                return;
            case R.id.ambient_sounds_ll /*2131558549*/:
                startActivity(new Intent(this, AmbientSoundsActivity.class));
                return;
            case R.id.back /*2131558601*/:
                finish();
                return;
            case R.id.settings /*2131558700*/:
                if (this.settings.getText().toString().equalsIgnoreCase(getResources().getString(R.string.list))) {
                    this.settings.setText(getResources().getString(R.string.map));
                } else {
                    this.settings.setText(getResources().getString(R.string.list));
                }
                flipCard();
                return;
            case R.id.menu /*2131558733*/:
                startAnimation();
                return;
            case R.id.current_address_img /*2131558858*/:
                if (this.check_Internetconnection.isConnectingToInternet()) {
                    new LatLangAsynckTask().execute(new String[]{getString(R.string.address_url) + "latlng=" + this.current_lat + "," + this.current_longi + "&sensor=true"});
                    return;
                }
                Toast.makeText(getApplicationContext(), getResources().getString(R.string.no_network), 0).show();
                return;
            case R.id.search_tv /*2131558859*/:
                String address = this.search_et.getText().toString().trim();
                if (address.length() <= 0) {
                    Toast.makeText(getApplicationContext(), getResources().getString(R.string.search_alert), 0).show();
                    return;
                } else if (this.check_Internetconnection.isConnectingToInternet()) {
                    new LatLangAsynckTask().execute(new String[]{getString(R.string.lat_lang_url) + "&address=" + address});
                    return;
                } else {
                    Toast.makeText(getApplicationContext(), getResources().getString(R.string.no_network), 0).show();
                    return;
                }
            default:
                return;
        }
    }

    private void setUpMapIfNeeded() {
        if (this.mMap == null) {
            ((SupportMapFragment) getSupportFragmentManager().findFragmentById(R.id.map)).getMapAsync(new OnMapReadyCallback() {
                @Override
                public void onMapReady(GoogleMap googleMap) {
                    StoreLocatorActivity.this.mMap = googleMap;
                    StoreLocatorActivity.this.mMap.clear();
                }
            });

        }
    }

    private void buildAlertMessageNoGps() {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setMessage("Your GPS seems to be disabled, do you want to enable it?").setCancelable(false).setPositiveButton("Yes", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
                StoreLocatorActivity.this.startActivity(new Intent("android.settings.LOCATION_SOURCE_SETTINGS"));
            }
        }).setNegativeButton("No", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
                dialog.cancel();
            }
        });
        builder.create().show();
    }

    /* access modifiers changed from: private */
    public void displyNewLocation(String lat, String lang, String name, String address) {
        LatLng latLng = new LatLng(Double.parseDouble(lat), Double.parseDouble(lang));
        this.mMap.addMarker(new MarkerOptions().position(latLng).title(name).snippet(address));
        this.mMap.moveCamera(CameraUpdateFactory.newLatLng(latLng));
        this.mMap.animateCamera(CameraUpdateFactory.zoomTo(7.0f));
    }

    private void topBarInit() {
        this.back = (Button) findViewById(R.id.back);
        this.settings = (TextView) findViewById(R.id.settings);
        this.header = (TextView) findViewById(R.id.header);
        this.back.setOnClickListener(this);
        this.settings.setOnClickListener(this);
        this.settings.setText(getResources().getString(R.string.list));
        this.header.setText(getResources().getString(R.string.find_braven));
        this.back.setVisibility(8);
        this.menu = (ImageView) findViewById(R.id.menu);
        this.menu.setVisibility(0);
        this.menu.setOnClickListener(this);
    }

    private void BottomBarInit() {
        this.play_music_ll = (LinearLayout) findViewById(R.id.play_music_ll);
        this.ambient_sounds_ll = (LinearLayout) findViewById(R.id.ambient_sounds_ll);
        this.play_music_ll.setOnClickListener(this);
        this.ambient_sounds_ll.setOnClickListener(this);
    }

    public void onAnimationStart(Animation animation) {
    }

    public void onAnimationEnd(Animation animation) {
        this.menuOut = !this.menuOut;
        if (!this.menuOut) {
            this.backLayout_operations.setVisibility(4);
        }
        layoutApp(this.menuOut);
        if (this.isMenuSelected) {
            this.isMenuSelected = false;
            if (this.selectedPositionFromMenu == 0) {
                this.menuListView.setAdapter(new MenuListViewAdapter(this, 0));
                startActivity(new Intent(this, DeviceScanActivity.class));
            } else if (this.selectedPositionFromMenu == 1) {
                this.menuListView.setAdapter(new MenuListViewAdapter(this, 1));
                //startActivity(new Intent(this, AddAlbumFragment1.class));
            } else if (this.selectedPositionFromMenu == 2) {
                this.menuListView.setAdapter(new MenuListViewAdapter(this, 2));
                startActivity(new Intent(this, WebPageActivity.class));
            } else if (this.selectedPositionFromMenu == 3) {
                this.menuListView.setAdapter(new MenuListViewAdapter(this, 3));
            } else if (this.selectedPositionFromMenu == 4) {
                this.menuListView.setAdapter(new MenuListViewAdapter(this, 4));
                startActivity(new Intent(this, WebPageActivity.class));
            }
        }
    }

    public void onAnimationRepeat(Animation animation) {
    }

    private class LatLangAsynckTask extends AsyncTask<String, Void, String> {
        private String address;
        private String latString;
        private String lngString;
        private String url;

        private LatLangAsynckTask() {
            this.latString = "";
            this.lngString = "";
            this.address = "";
            this.url = "";
        }

        /* access modifiers changed from: protected */
        public void onPreExecute() {
            super.onPreExecute();
            ProgressDialog unused = StoreLocatorActivity.this.dialog = new ProgressDialog(StoreLocatorActivity.this);
            StoreLocatorActivity.this.dialog.setMessage(StoreLocatorActivity.this.getResources().getString(R.string.plz_wait));
            StoreLocatorActivity.this.dialog.setCanceledOnTouchOutside(false);
            StoreLocatorActivity.this.dialog.show();
        }

        /* access modifiers changed from: protected */
        public String doInBackground(String... params) {
            try {
                this.url = params[0].replace(" ", "%20");
                Log.v("TAG", "url = " + this.url);
                JSONObject jsonObject = JSONParser.makeHttpRequest(StoreLocatorActivity.this, this.url, "GET", (Object) null);
                if (jsonObject != null) {
                    JSONObject object = jsonObject.getJSONArray("results").getJSONObject(0);
                    this.address = object.getString("formatted_address");
                    Log.v("TAG", "address = " + this.address);
                    JSONObject locationObject = object.getJSONObject("geometry").getJSONObject(Headers.LOCATION);
                    this.latString = locationObject.getString("lat");
                    this.lngString = locationObject.getString("lng");
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }
            return null;
        }

        /* access modifiers changed from: protected */
        public void onPostExecute(String result) {
            super.onPostExecute(result);
            if (this.url.contains(StoreLocatorActivity.this.getResources().getString(R.string.address_url))) {
                StoreLocatorActivity.this.search_et.setText(this.address);
                StoreLocatorActivity.this.dialog.dismiss();
            } else if (StoreLocatorActivity.this.check_Internetconnection.isConnectingToInternet()) {
                new StoresAsynckTask(this.latString, this.lngString).execute(new Void[0]);
            } else {
                Toast.makeText(StoreLocatorActivity.this.getApplicationContext(), StoreLocatorActivity.this.getResources().getString(R.string.no_network), 0).show();
            }
        }
    }

    private class StoresAsynckTask extends AsyncTask<Void, Void, Void> {
        private String latString = "";
        private String lngString = "";

        public StoresAsynckTask(String latString2, String lngString2) {
            this.latString = latString2;
            this.lngString = lngString2;
        }

        /* access modifiers changed from: protected */
        public Void doInBackground(Void... params) {
            try {
                XMLReader xmlR = SAXParserFactory.newInstance().newSAXParser().getXMLReader();
                URL url = new URL(StoreLocatorActivity.this.getResources().getString(R.string.base_url) + "lat=" + this.latString + "&lng=" + this.lngString + "&radius=50&store=anystore&device=any");
                Log.v("TEST", "url " + url);
                xmlR.setContentHandler(new StoreListHandeler());
                xmlR.parse(new InputSource(url.openStream()));
                return null;
            } catch (Exception e) {
                System.out.println(e);
                return null;
            }
        }

        /* access modifiers changed from: protected */
        public void onPostExecute(Void result) {
            super.onPostExecute(result);
            StoreLocatorActivity.this.mMap.clear();
            if (StoreListHandeler.storeListDetails == null || StoreListHandeler.storeListDetails.size() <= 0) {
                Toast.makeText(StoreLocatorActivity.this.getApplicationContext(), StoreLocatorActivity.this.getResources().getString(R.string.no_stores), 0).show();
            } else {
                Log.v("TAG", "size = " + StoreListHandeler.storeListDetails.size());
                for (int i = 0; i < StoreListHandeler.storeListDetails.size(); i++) {
                    String lat = StoreListHandeler.storeListDetails.get(i).getLat().trim();
                    String lng = StoreListHandeler.storeListDetails.get(i).getLng().trim();
                    String name = StoreListHandeler.storeListDetails.get(i).getName().trim();
                    String address = StoreListHandeler.storeListDetails.get(i).getAddress().trim();
                    if (!name.equalsIgnoreCase(StoreLocatorActivity.this.getResources().getString(R.string.no_stores))) {
                        StoreLocatorActivity.this.displyNewLocation(lat, lng, name, address);
                    }
                }
                if (StoreListHandeler.storeListDetails.size() == 1) {
                    if (!StoreListHandeler.storeListDetails.get(0).getName().trim().equalsIgnoreCase("No Results Found")) {
                        StoreLocatorActivity.this.locations_listview.setAdapter(new StoreAdapter(StoreListHandeler.storeListDetails));
                    } else {
                        StoreLocatorActivity.this.mMap.clear();
                        StoreLocatorActivity.this.locations_listview.setVisibility(8);
                        Toast.makeText(StoreLocatorActivity.this.getApplicationContext(), StoreLocatorActivity.this.getResources().getString(R.string.no_stores), 0).show();
                    }
                } else if (StoreListHandeler.storeListDetails.size() >= 2) {
                    StoreLocatorActivity.this.locations_listview.setAdapter(new StoreAdapter(StoreListHandeler.storeListDetails));
                } else {
                    StoreLocatorActivity.this.mMap.clear();
                    StoreLocatorActivity.this.locations_listview.setVisibility(8);
                    Toast.makeText(StoreLocatorActivity.this.getApplicationContext(), StoreLocatorActivity.this.getResources().getString(R.string.no_stores), 0).show();
                }
            }
            StoreLocatorActivity.this.dialog.dismiss();
        }
    }

    private class StoreAdapter extends BaseAdapter {
        private ArrayList<StoreDetails> storeListDetails;

        public StoreAdapter(ArrayList<StoreDetails> storeListDetails2) {
            this.storeListDetails = storeListDetails2;
        }

        public int getCount() {
            return this.storeListDetails.size();
        }

        public Object getItem(int arg0) {
            return null;
        }

        public long getItemId(int arg0) {
            return 0;
        }

        public View getView(int index, View convertView, ViewGroup arg2) {
            View view = convertView;
            View v = ((LayoutInflater) StoreLocatorActivity.this.getSystemService("layout_inflater")).inflate(R.layout.custom_store_item, (ViewGroup) null);
            ((TextView) v.findViewById(R.id.name_tv)).setText(this.storeListDetails.get(index).getName());
            ((TextView) v.findViewById(R.id.address_tv)).setText(this.storeListDetails.get(index).getAddress());
            ((TextView) v.findViewById(R.id.distance_tv)).setText(StoreLocatorActivity.this.form.format(Double.parseDouble(this.storeListDetails.get(index).getDistance())) + " miles");
            final String lat = this.storeListDetails.get(index).getLat().trim();
            final String lng = this.storeListDetails.get(index).getLng().trim();
            v.setOnClickListener(new View.OnClickListener() {
                public void onClick(View arg0) {
                    Intent intent = new Intent("android.intent.action.VIEW");
                    intent.setData(Uri.parse("geo:0,0?q=" + lat + ", " + lng));
                    try {
                        StoreLocatorActivity.this.startActivity(intent);
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            });
            return v;
        }
    }

    public void onConnectionFailed(ConnectionResult connectionResult) {
        if (connectionResult.hasResolution()) {
            try {
                connectionResult.startResolutionForResult(this, CONNECTION_FAILURE_RESOLUTION_REQUEST);
            } catch (IntentSender.SendIntentException e) {
                e.printStackTrace();
            }
        } else {
            Log.v("TAG", "Location services connection failed with code " + connectionResult.getErrorCode());
        }
    }

    public void onConnected(Bundle arg0) {
    }

    public void onConnectionSuspended(int arg0) {
    }

    public void onLocationChanged(Location location) {
        this.current_lat = String.valueOf(location.getLatitude());
        this.current_longi = String.valueOf(location.getLongitude());
        Log.v("TAG", "current_lat = " + this.current_lat);
    }

    public void startAnimation() {
        Animation anim;
        int w = this.frontLayout_operations.getMeasuredWidth();
        int h = this.frontLayout_operations.getMeasuredHeight();
        int left = (int) (((double) this.frontLayout_operations.getMeasuredWidth()) * 0.6d);
        if (!this.menuOut) {
            anim = new TranslateAnimation(0.0f, (float) left, 0.0f, 0.0f);
            this.backLayout_operations.setVisibility(0);
            this.animParams.init(left, 0, left + w, h);
        } else {
            anim = new TranslateAnimation(0.0f, (float) (-left), 0.0f, 0.0f);
            this.animParams.init(0, 0, w, h);
        }
        anim.setDuration(400);
        anim.setAnimationListener(this);
        anim.setFillAfter(true);
        this.frontLayout_operations.startAnimation(anim);
    }

    /* access modifiers changed from: package-private */
    public void layoutApp(boolean menuOut2) {
        this.frontLayout_operations.layout(this.animParams.left, this.animParams.top, this.animParams.right, this.animParams.bottom);
        this.frontLayout_operations.clearAnimation();
    }
}
