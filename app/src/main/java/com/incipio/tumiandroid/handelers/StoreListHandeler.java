package com.incipio.tumiandroid.handelers;

import android.util.Log;
import com.incipio.tumiandroid.utils.StoreDetails;
import java.util.ArrayList;
//import kaaes.spotify.webapi.samplesearch.DBHelper;
import org.xml.sax.Attributes;
import org.xml.sax.SAXException;
import org.xml.sax.helpers.DefaultHandler;

public class StoreListHandeler extends DefaultHandler {
    public static ArrayList<StoreDetails> storeListDetails = null;
    Boolean elementOn = false;
    String elementValue = null;
    private StoreDetails storeDetails;

    public void startElement(String uri, String localName, String qName, Attributes attributes) throws SAXException {
        this.elementOn = true;
        if (localName.equals("storess")) {
            storeListDetails = new ArrayList<>();
            storeListDetails.clear();
        } else if (localName.equals("marker")) {
            this.storeDetails = new StoreDetails();
            this.storeDetails.setName(attributes.getValue("Song_Name"));//(DBHelper.PlayList_SONG_NAME));
            this.storeDetails.setAddress(attributes.getValue("address"));
            this.storeDetails.setLat(attributes.getValue("lat"));
            this.storeDetails.setLng(attributes.getValue("lng"));
            this.storeDetails.setDistance(attributes.getValue("distance"));
            this.storeDetails.setPhone(attributes.getValue("phone"));
            this.storeDetails.setNr(attributes.getValue("nr"));
            Log.v("TAG", "name = " + attributes.getValue("Song_Name"));//(DBHelper.PlayList_SONG_NAME));
            Log.v("TAG", "address = " + attributes.getValue("address"));
            Log.v("TAG", "distance = " + attributes.getValue("distance"));
            storeListDetails.add(this.storeDetails);
        }
    }

    public void characters(char[] ch, int start, int length) throws SAXException {
        if (this.elementOn.booleanValue()) {
            this.elementValue = new String(ch, start, length);
            this.elementOn = false;
        }
    }

    public void endElement(String uri, String localName, String qName) throws SAXException {
        this.elementOn = false;
        this.elementValue = "";
    }
}
