package com.incipio.tumiandroid.adapters;

import android.content.Context;
import android.graphics.Color;
import android.graphics.Typeface;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;
import com.incipio.tumiandroid.R;

public class MenuListViewAdapter extends BaseAdapter {
    private int index = 100;
    private Context mContext;
    private String[] menuItems = {"POWER", "AUDIO", "CUSTOMER SERVICE", "ABOUT TUMI", "STORE LOCATOR", "SHOP TUMI.com"};

    public MenuListViewAdapter(Context applicationContext) {
        this.mContext = applicationContext;
    }

    public MenuListViewAdapter(Context applicationContext, int p) {
        this.mContext = applicationContext;
        this.index = p;
    }

    public int getCount() {
        return this.menuItems.length;
    }

    public Object getItem(int position) {
        return null;
    }

    public long getItemId(int position) {
        return 0;
    }

    public View getView(int position, View convertView, ViewGroup parent) {
        View view = convertView;
        View v = ((LayoutInflater) this.mContext.getSystemService("layout_inflater")).inflate(R.layout.custommenuitems, (ViewGroup) null);
        TextView name = (TextView) v.findViewById(R.id.categoryText);
        if (this.index == position) {
            name.setText(Html.fromHtml("<u>" + this.menuItems[position] + "</u>"));
            name.setTextColor(Color.parseColor("#b70e30"));
        } else {
            name.setText(this.menuItems[position]);
            name.setTextColor(Color.parseColor("#000000"));
        }
        name.setTypeface(Typeface.createFromAsset(this.mContext.getAssets(), "fonts/gotham_medium.otf"));
        return v;
    }
}
