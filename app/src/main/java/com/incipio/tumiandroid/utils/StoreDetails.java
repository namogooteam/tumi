package com.incipio.tumiandroid.utils;

public class StoreDetails {
    public String address;
    public String distance;
    public String lat;
    public String lng;
    public String name;
    public String nr;
    public String phone;

    public String getName() {
        return this.name;
    }

    public void setName(String name2) {
        this.name = name2;
    }

    public String getAddress() {
        return this.address;
    }

    public void setAddress(String address2) {
        this.address = address2;
    }

    public String getLat() {
        return this.lat;
    }

    public void setLat(String lat2) {
        this.lat = lat2;
    }

    public String getLng() {
        return this.lng;
    }

    public void setLng(String lng2) {
        this.lng = lng2;
    }

    public String getDistance() {
        return this.distance;
    }

    public void setDistance(String distance2) {
        this.distance = distance2;
    }

    public String getPhone() {
        return this.phone;
    }

    public void setPhone(String phone2) {
        this.phone = phone2;
    }

    public String getNr() {
        return this.nr;
    }

    public void setNr(String nr2) {
        this.nr = nr2;
    }
}
