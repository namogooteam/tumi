package com.incipio.tumiandroid.utils;

import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;

public class Check_Internetconnection {
    private Context _context;

    public Check_Internetconnection(Context context) {
        this._context = context;
    }

    public boolean isConnectingToInternet() {
        NetworkInfo[] info;
        ConnectivityManager connectivity = (ConnectivityManager) this._context.getSystemService("connectivity");
        if (!(connectivity == null || (info = connectivity.getAllNetworkInfo()) == null)) {
            for (NetworkInfo state : info) {
                if (state.getState() == NetworkInfo.State.CONNECTED) {
                    return true;
                }
            }
        }
        return false;
    }
}
