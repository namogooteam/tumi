package com.incipio.tumiandroid.utils;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.Rect;
import android.graphics.Typeface;
import android.text.TextPaint;
import android.util.AttributeSet;
import android.util.TypedValue;
import android.view.View;
import android.view.ViewTreeObserver;
import java.util.ArrayList;
import java.util.List;

public class JustifiedTextView extends View {
    private int colIndex = 0;
    boolean hasTextBeenDrown = false;
    private int lineHeight;
    private List<String> lineList = new ArrayList();
    private int lineSpace = 0;
    private Context mContext;
    private XmlToClassAttribHandler mXmlParser;
    private int measuredViewHeight;
    private int measuredViewWidth;
    private int rowIndex = 0;
    private String text;
    private int textAreaWidth;
    private TextPaint textPaint;

    public JustifiedTextView(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        constructor(context, attrs);
    }

    public JustifiedTextView(Context context, AttributeSet attrs) {
        super(context, attrs);
        constructor(context, attrs);
    }

    public JustifiedTextView(Context context) {
        super(context);
        constructor(context, (AttributeSet) null);
    }

    private void constructor(Context context, AttributeSet attrs) {
        this.mContext = context;
        this.mXmlParser = new XmlToClassAttribHandler(this.mContext, attrs);
        initTextPaint();
        if (attrs != null) {
            String text2 = this.mXmlParser.getTextValue();
            int textColor = this.mXmlParser.getColorValue();
            int textSize = this.mXmlParser.getTextSize();
            int textSizeUnit = this.mXmlParser.gettextSizeUnit();
            setText(text2);
            setTextColor(textColor);
            if (textSizeUnit == -1) {
                setTextSize((float) textSize);
            } else {
                setTextSize(textSizeUnit, (float) textSize);
            }
        }
        getViewTreeObserver().addOnGlobalLayoutListener(new ViewTreeObserver.OnGlobalLayoutListener() {
            public void onGlobalLayout() {
                if (!JustifiedTextView.this.hasTextBeenDrown) {
                    JustifiedTextView.this.hasTextBeenDrown = true;
                    JustifiedTextView.this.setTextAreaWidth(JustifiedTextView.this.getWidth() - (JustifiedTextView.this.getPaddingLeft() + JustifiedTextView.this.getPaddingRight()));
                    JustifiedTextView.this.calculate();
                }
            }
        });
    }

    /* access modifiers changed from: private */
    public void calculate() {
        setLineHeight(getTextPaint());
        this.lineList.clear();
        this.lineList = divideOriginalTextToStringLineList(getText());
        setMeasuredDimentions(this.lineList.size(), getLineHeight(), getLineSpace());
        measure(getMeasuredViewWidth(), getMeasuredViewHeight());
    }

    private void initTextPaint() {
        this.textPaint = new TextPaint(1);
        this.textPaint.setTextAlign(Paint.Align.LEFT);
    }

    /* access modifiers changed from: protected */
    public void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
        if (getMeasuredViewWidth() > 0) {
            requestLayout();
            setMeasuredDimension(getMeasuredViewWidth(), getMeasuredViewHeight());
        } else {
            super.onMeasure(widthMeasureSpec, heightMeasureSpec);
        }
        invalidate();
    }

    /* access modifiers changed from: protected */
    public void onDraw(Canvas canvas) {
        this.rowIndex = getPaddingTop();
        if (getAlignment() == Paint.Align.RIGHT) {
            this.colIndex = getPaddingLeft() + getTextAreaWidth();
        } else {
            this.colIndex = getPaddingLeft();
        }
        for (int i = 0; i < this.lineList.size(); i++) {
            this.rowIndex += getLineHeight() + getLineSpace();
            canvas.drawText(this.lineList.get(i), (float) this.colIndex, (float) this.rowIndex, getTextPaint());
        }
    }

    private List<String> divideOriginalTextToStringLineList(String originalText) {
        List<String> listStringLine = new ArrayList<>();
        String line = "";
        String[] listParageraphes = originalText.split("\n");
        for (String split : listParageraphes) {
            String[] arrayWords = split.split(" ");
            int i = 0;
            while (i < arrayWords.length) {
                line = line + arrayWords[i] + " ";
                float textWidth = getTextPaint().measureText(line);
                if (((float) getTextAreaWidth()) == textWidth) {
                    listStringLine.add(line);
                    line = "";
                } else if (((float) getTextAreaWidth()) < textWidth) {
                    line = line.substring(0, (line.length() - arrayWords[i].length()) - 1);
                    if (line.trim().length() != 0) {
                        listStringLine.add(justifyTextLine(this.textPaint, line.trim(), getTextAreaWidth()));
                        line = "";
                        i--;
                    }
                } else if (i == arrayWords.length - 1) {
                    listStringLine.add(line);
                    line = "";
                }
                i++;
            }
        }
        return listStringLine;
    }

    private String justifyTextLine(TextPaint textPaint2, String lineString, int textAreaWidth2) {
        int gapIndex = 0;
        float lineWidth = textPaint2.measureText(lineString);
        while (lineWidth < ((float) textAreaWidth2) && lineWidth > 0.0f && ((gapIndex = lineString.indexOf(" ", gapIndex + 2)) != -1 || (gapIndex = lineString.indexOf(" ", 1)) != -1)) {
            lineString = lineString.substring(0, gapIndex) + "  " + lineString.substring(gapIndex + 1, lineString.length());
            lineWidth = textPaint2.measureText(lineString);
        }
        return lineString;
    }

    private void setLineHeight(TextPaint textPaint2) {
        Rect bounds = new Rect();
        textPaint2.getTextBounds("این حسین کیست که عالم همه دیوانه اوست", 0, "این حسین کیست که عالم همه دیوانه اوست".length(), bounds);
        setLineHeight(bounds.height());
    }

    public void setMeasuredDimentions(int lineListSize, int lineHeigth, int lineSpace2) {
        setMeasuredViewHeight(((lineHeigth + lineSpace2) * lineListSize) + lineSpace2 + getPaddingRight() + getPaddingLeft());
        setMeasuredViewWidth(getWidth());
    }

    private int getTextAreaWidth() {
        return this.textAreaWidth;
    }

    /* access modifiers changed from: private */
    public void setTextAreaWidth(int textAreaWidth2) {
        this.textAreaWidth = textAreaWidth2;
    }

    private int getLineHeight() {
        return this.lineHeight;
    }

    private int getMeasuredViewHeight() {
        return this.measuredViewHeight;
    }

    private void setMeasuredViewHeight(int measuredViewHeight2) {
        this.measuredViewHeight = measuredViewHeight2;
    }

    private int getMeasuredViewWidth() {
        return this.measuredViewWidth;
    }

    private void setMeasuredViewWidth(int measuredViewWidth2) {
        this.measuredViewWidth = measuredViewWidth2;
    }

    private void setLineHeight(int lineHeight2) {
        this.lineHeight = lineHeight2;
    }

    public String getText() {
        return this.text;
    }

    public void setText(String text2) {
        this.text = text2;
        calculate();
        invalidate();
    }

    public void setText(int resid) {
        setText(this.mContext.getResources().getString(resid));
    }

    public Typeface getTypeFace() {
        return getTextPaint().getTypeface();
    }

    public void setTypeFace(Typeface typeFace) {
        getTextPaint().setTypeface(typeFace);
    }

    public float getTextSize() {
        return getTextPaint().getTextSize();
    }

    public void setTextSize(int unit, float textSize) {
        setTextSize(TypedValue.applyDimension(unit, textSize, this.mContext.getResources().getDisplayMetrics()));
    }

    private void setTextSize(float textSize) {
        getTextPaint().setTextSize(textSize);
        calculate();
        invalidate();
    }

    public TextPaint getTextPaint() {
        return this.textPaint;
    }

    public void setTextPaint(TextPaint textPaint2) {
        this.textPaint = textPaint2;
    }

    public void setTextColor(int textColor) {
        getTextPaint().setColor(textColor);
        invalidate();
    }

    public void setLineSpacing(int lineSpace2) {
        this.lineSpace = lineSpace2;
        invalidate();
    }

    public int getTextColor() {
        return getTextPaint().getColor();
    }

    public int getLineSpace() {
        return this.lineSpace;
    }

    public Paint.Align getAlignment() {
        return getTextPaint().getTextAlign();
    }

    public void setAlignment(Paint.Align align) {
        getTextPaint().setTextAlign(align);
        invalidate();
    }
}
