package com.incipio.tumiandroid.utils;

import android.content.Context;
import android.content.SharedPreferences;
import android.util.Log;
import com.spotify.sdk.android.authentication.AuthenticationResponse;
import io.fabric.sdk.android.services.settings.SettingsJsonConstants;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
//import kaaes.spotify.webapi.samplesearch.CredentialsHandler;
import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.client.params.HttpClientParams;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.params.BasicHttpParams;
import org.apache.http.params.HttpConnectionParams;
import org.apache.http.params.HttpParams;
import org.json.JSONException;
import org.json.JSONObject;

public class JSONParser {
    private static String INTERNAL_SERVER_ERROR = "Internal server error";
    private static String SERVICE_UNAVAILABLE = "Service unavailable";
    private static Context cntx;
    private static HttpClient httpClient;
    private static HttpConnectionParams httpConnectionParams;
    private static HttpParams httpParams;
    private static HttpResponse httpResponse;
    private static InputStream inputStream = null;
    private static JSONObject jsonObj = null;
    private static String jsonString = "";
    public static String statusCode = "";

    public static JSONObject makeHttpRequest(Context context, String url, String method, Object params) {
        try {
            cntx = context;
            httpParams = new BasicHttpParams();
            HttpConnectionParams httpConnectionParams2 = httpConnectionParams;
            HttpConnectionParams.setSoTimeout(httpParams, 100000);
            HttpConnectionParams httpConnectionParams3 = httpConnectionParams;
            HttpConnectionParams.setConnectionTimeout(httpParams, 100000);
            HttpClientParams.setRedirecting(httpParams, false);
            httpClient = new DefaultHttpClient(httpParams);
            httpResponse = null;
            if (method.equalsIgnoreCase("POST")) {
                Log.v("TAG", "params = " + params.toString());
                HttpPost httpPost = new HttpPost(url);
                httpPost.setHeader("Content-Type", "application/json");
                httpPost.setEntity(new StringEntity(params.toString(), "UTF-8"));
                httpResponse = httpClient.execute(httpPost);
            } else if (method.equalsIgnoreCase("GET")) {
                httpResponse = httpClient.execute(new HttpGet(url));
            } else {
                HttpGet httpGet = new HttpGet(url);
                httpGet.addHeader("Authorization", "Bearer " + getToken(cntx));
                httpResponse = httpClient.execute(httpGet);
            }
            Log.v("TAG", "getStatusCode =" + httpResponse.getStatusLine().getStatusCode());
            statusCode = "" + httpResponse.getStatusLine().getStatusCode();
            jsonObj = null;
            jsonObj = getStatusData(httpResponse.getStatusLine().getStatusCode(), url);
        } catch (IOException e) {
            jsonObj = null;
            e.printStackTrace();
        } catch (Exception e2) {
            jsonObj = null;
            e2.printStackTrace();
        }
        return jsonObj;
    }

    private static JSONObject getStatusData(int statusCode2, String url) {
        if (statusCode2 != 200 && statusCode2 != 400 && statusCode2 != 401 && statusCode2 != 404 && statusCode2 != 503 && statusCode2 != 307) {
            return getResposeDefaultData(statusCode2, INTERNAL_SERVER_ERROR);
        }
        if (statusCode2 != 401 || statusCode2 != 404 || statusCode2 != 503) {
            try {
                HttpEntity httpEntity = httpResponse.getEntity();
                inputStream = null;
                inputStream = httpEntity.getContent();
                BufferedReader reader = new BufferedReader(new InputStreamReader(inputStream, "UTF-8"), 8);
                StringBuilder sb = new StringBuilder();
                while (true) {
                    String line = reader.readLine();
                    if (line != null) {
                        sb.append(line + "\n");
                    } else {
                        inputStream.close();
                        jsonString = sb.toString();
                        Log.v("TAG", "json = " + jsonString);
                        return new JSONObject(jsonString);
                    }
                }
            } catch (Exception e) {
                e.printStackTrace();
                if (statusCode2 == 401) {
                    return null;
                }
                if (statusCode2 == 404 || statusCode2 == 503) {
                    return getResposeDefaultData(statusCode2, SERVICE_UNAVAILABLE);
                }
                return null;
            }
        } else if (statusCode2 == 401) {
            return null;
        } else {
            if (statusCode2 == 404 || statusCode2 == 503) {
                return getResposeDefaultData(statusCode2, SERVICE_UNAVAILABLE);
            }
            return null;
        }
    }

    private static JSONObject getResposeDefaultData(int statusCode2, String data) {
        String messageValue = "";
        JSONObject jsonObject = null;
        if (statusCode2 == 401) {
            messageValue = "Unauthorized";
        } else if (statusCode2 == 404 || statusCode2 == 503) {
            try {
                messageValue = SERVICE_UNAVAILABLE;
            } catch (Exception e) {
                e = e;
            }
        }
        JSONObject jsonObject2 = new JSONObject();
        try {
            jsonObject2.put("status", "" + statusCode2);
            //jsonObject2.put(AuthenticationResponse.QueryParams.CODE, "0");
            jsonObject2.put(SettingsJsonConstants.PROMPT_MESSAGE_KEY, messageValue);
            jsonObject2.put("data", data);
            return jsonObject2;
        } catch (JSONException e2) {
            //e = e2;
            jsonObject = jsonObject2;
            e2.printStackTrace();
            return jsonObject;
        }
    }

    public static String getToken(Context context) {
        SharedPreferences sharedPref = context.getApplicationContext().getSharedPreferences("webapi.credentials.access_token", 0);
        String token = sharedPref.getString("access_token", (String) null);
        long expiresAt = sharedPref.getLong("expires_at", 0);
        if (token == null || expiresAt < System.currentTimeMillis()) {
            return null;
        }
        return token;
    }
}
