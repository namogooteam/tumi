package com.incipio.tumiandroid.utils;

public class AnimParams {
    public int bottom;
    public int left;
    public int right;
    public int top;

    public void init(int left2, int top2, int right2, int bottom2) {
        this.left = left2;
        this.top = top2;
        this.right = right2;
        this.bottom = bottom2;
    }
}
