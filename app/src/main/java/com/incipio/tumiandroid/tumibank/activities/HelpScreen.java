package com.incipio.tumiandroid.tumibank.activities;

import android.app.Activity;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import com.incipio.tumiandroid.R;

public class HelpScreen extends Activity {
    /* access modifiers changed from: private */
    public String activity = "";
    int[] backgrounds;
    /* access modifiers changed from: private */
    public int count = 0;
    /* access modifiers changed from: private */
    public ImageView img;

    static /* synthetic */ int access$108(HelpScreen x0) {
        int i = x0.count;
        x0.count = i + 1;
        return i;
    }

    /* access modifiers changed from: protected */
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.help_screen);
        this.activity = getIntent().getExtras().get("activity").toString();
        final int[] backgrounds2 = {R.drawable.help_1, R.drawable.help_2a, R.drawable.help_2b, R.drawable.help_3, R.drawable.help_4a, R.drawable.help_4b};
        final int[] backgrounds1 = {R.drawable.mydevice_1a, R.drawable.notreachable_1b, R.drawable.addnew_1c, R.drawable.done};
        this.count = 0;
        this.img = (ImageView) findViewById(R.id.img);
        if (this.activity.equals("0")) {
            this.img.setBackgroundResource(backgrounds1[this.count]);
        } else {
            this.img.setBackgroundResource(backgrounds2[this.count]);
        }
        this.img.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                if (HelpScreen.this.activity.equals("0")) {
                    if (HelpScreen.this.count == 3) {
                        HelpScreen.this.finish();
                        return;
                    }
                    HelpScreen.access$108(HelpScreen.this);
                    HelpScreen.this.img.setBackgroundResource(backgrounds1[HelpScreen.this.count]);
                } else if (HelpScreen.this.count == 5) {
                    HelpScreen.this.finish();
                } else {
                    HelpScreen.access$108(HelpScreen.this);
                    HelpScreen.this.img.setBackgroundResource(backgrounds2[HelpScreen.this.count]);
                }
            }
        });
    }
}
