package com.incipio.tumiandroid.tumibank.activities;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentSender;
import android.content.pm.PackageManager;
import android.graphics.Typeface;
import android.location.Location;
import android.location.LocationManager;
import android.net.http.Headers;
import android.os.Build;
import android.os.Bundle;
import android.text.Html;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewTreeObserver;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.view.animation.TranslateAnimation;
import android.widget.AdapterView;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.Switch;
import android.widget.TextView;
import android.widget.ToggleButton;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.FragmentActivity;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GooglePlayServicesUtil;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.drive.DriveFile;
import com.google.android.gms.location.LocationListener;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;
import com.incipio.tumiandroid.R;
import com.incipio.tumiandroid.activities.WebPageActivity;
import com.incipio.tumiandroid.adapters.MenuListViewAdapter;
import com.incipio.tumiandroid.tumibank.utils.SavedPreferences;
import com.incipio.tumiandroid.utils.AnimParams;
//import kaaes.spotify.webapi.samplesearch.AddAlbumFragment1;

public class DummyActivity extends FragmentActivity implements LocationListener, GoogleApiClient.ConnectionCallbacks, GoogleApiClient.OnConnectionFailedListener, Animation.AnimationListener {
    private static final int CONNECTION_FAILURE_RESOLUTION_REQUEST = 9000;
    private AnimParams animParams = new AnimParams();
    /* access modifiers changed from: private */
    public ImageView arrow_img;
    private LinearLayout back;
    private LinearLayout backLayout_operations;
    /* access modifiers changed from: private */
    public RelativeLayout bottom_ll;
    private TextView connect;
    /* access modifiers changed from: private */
    public ViewGroup content;
    private LinearLayout find_ll;
    private TextView findme;
    private FrameLayout frontLayout_operations;
    private TextView header;
    private ImageView header_img;
    private LinearLayout header_img_ll;
    private ImageView help;
    private boolean isInbackground = false;
    /* access modifiers changed from: private */
    public boolean isMenuSelected = false;
    private boolean isOpen = false;
    private double lat;
    private double lng;
    private GoogleApiClient mGoogleApiClient;
    private LocationRequest mLocationRequest;
    private GoogleMap mMap;
    private LinearLayout mask_ll;
    private LinearLayout menu;
    private ListView menuListView;
    private boolean menuOut = false;
    private Switch port1;
    private ToggleButton port1tg;
    private Switch port2;
    private ToggleButton port2tg;
    private TextView privacy;
    private SavedPreferences savedPreferences;
    /* access modifiers changed from: private */
    public int screenHeight = 0;
    /* access modifiers changed from: private */
    public int selectedPositionFromMenu;
    private TextView settings;
    private ImageView settings_img;
    private TextView title;
    private TextView version;

    /* access modifiers changed from: protected */
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.dummy);
        topBarInit();
        this.mGoogleApiClient = new GoogleApiClient.Builder(this).addConnectionCallbacks(this).addOnConnectionFailedListener(this).addApi(LocationServices.API).build();
        this.mLocationRequest = LocationRequest.create().setPriority(100).setInterval(3600000).setFastestInterval(60000);
        int status = GooglePlayServicesUtil.isGooglePlayServicesAvailable(getBaseContext());
        if (status != 0) {
            GooglePlayServicesUtil.getErrorDialog(status, this, 10).show();
        } else if (!((LocationManager) getSystemService(Headers.LOCATION)).isProviderEnabled("gps")) {
            buildAlertMessageNoGps();
        }
        this.savedPreferences = new SavedPreferences(getApplicationContext());
        String latlng = this.savedPreferences.getLastLocation(RecordLocation.LATITUDE, RecordLocation.LONGITUDE);
        this.lat = Double.parseDouble(latlng.split(":")[0]);
        this.lng = Double.parseDouble(latlng.split(":")[1]);
        Log.v("TEST", "lat*****************" + this.lat);
        Log.v("TEST", "lng*****************" + this.lng);
        this.backLayout_operations = (LinearLayout) findViewById(R.id.backLayout_operations);
        this.frontLayout_operations = (FrameLayout) findViewById(R.id.frontLayout_operations);
        this.menuListView = (ListView) findViewById(R.id.menuListView);
        this.menuListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            public void onItemClick(AdapterView<?> adapterView, View arg1, int position, long arg3) {
                boolean unused = DummyActivity.this.isMenuSelected = true;
                int unused2 = DummyActivity.this.selectedPositionFromMenu = position;
                DummyActivity.this.startAnimation();
            }
        });
        this.mask_ll = (LinearLayout) findViewById(R.id.mask_ll);
        this.mask_ll.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                DummyActivity.this.startAnimation();
            }
        });
        this.privacy = (TextView) findViewById(R.id.privacypolicy);
        this.privacy.setText(Html.fromHtml("<u>Privacy Policy</u>"));
        this.privacy.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                boolean unused = DummyActivity.this.isMenuSelected = true;
                int unused2 = DummyActivity.this.selectedPositionFromMenu = 10;
                DummyActivity.this.startAnimation();
            }
        });
        String versionName = null;
        try {
            versionName = getPackageManager().getPackageInfo(getPackageName(), 0).versionName;
        } catch (PackageManager.NameNotFoundException e) {
            e.printStackTrace();
        }
        this.version = (TextView) findViewById(R.id.versionnumber);
        this.version.setText("v " + versionName);
        setUpMapIfNeeded();
        this.connect = (TextView) findViewById(R.id.connectagain);
        this.findme = (TextView) findViewById(R.id.findme);
        this.findme.setBackgroundColor(0);
        this.title = (TextView) findViewById(R.id.header);
        this.port1tg = (ToggleButton) findViewById(R.id.port1tg);
        this.port2tg = (ToggleButton) findViewById(R.id.port2tg);
        this.port1 = (Switch) findViewById(R.id.port1);
        this.port2 = (Switch) findViewById(R.id.port2);
        this.bottom_ll = (RelativeLayout) findViewById(R.id.bottom_ll);
        this.content = (ViewGroup) findViewById(R.id.content);
        this.find_ll = (LinearLayout) findViewById(R.id.find_ll);
        this.arrow_img = (ImageView) findViewById(R.id.arrow_img);
        this.bottom_ll.getViewTreeObserver().addOnGlobalLayoutListener(new ViewTreeObserver.OnGlobalLayoutListener() {
            public void onGlobalLayout() {
                int unused = DummyActivity.this.screenHeight = DummyActivity.this.bottom_ll.getHeight();
                Log.v("TAG", "drawer.getHeight() = " + DummyActivity.this.screenHeight);
                DummyActivity.this.bottom_ll.getViewTreeObserver().removeGlobalOnLayoutListener(this);
            }
        });
        this.find_ll.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                DummyActivity.this.slideUpDown(DummyActivity.this);
            }
        });
        String str = Build.VERSION.RELEASE;
        int i = Build.VERSION.SDK_INT;
        this.port1tg.setVisibility(0);
        this.port2tg.setVisibility(0);
        this.port1.setVisibility(8);
        this.port2.setVisibility(8);
        Typeface tf = Typeface.createFromAsset(getAssets(), "fonts/proximanova_regular.otf");
        this.settings.setTypeface(tf);
        this.connect.setTypeface(tf);
        this.title.setTypeface(Typeface.createFromAsset(getAssets(), "fonts/gothammedium.otf"));
        this.connect.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                DummyActivity.this.finish();
            }
        });
    }

    /* access modifiers changed from: protected */
    public void onResume() {
        this.mGoogleApiClient.connect();
        super.onResume();
        this.menuListView.setAdapter(new MenuListViewAdapter(this, 0));
    }

    /* access modifiers changed from: protected */
    public void onPause() {
        super.onPause();
        if (this.mGoogleApiClient.isConnected()) {
            LocationServices.FusedLocationApi.removeLocationUpdates(this.mGoogleApiClient, (LocationListener) this);
            this.mGoogleApiClient.disconnect();
        }
        if (this.menuOut) {
            startAnimation();
        }
    }

    /* access modifiers changed from: protected */
    public void slideUpDown(DummyActivity fragmentActivity) {
        if (!this.isOpen) {
            Log.v("TAG", "getTop = " + (this.bottom_ll.getTop() - ((this.screenHeight * 25) / 100)));
            Log.v("TAG", "getBottom = " + (this.bottom_ll.getBottom() - ((this.screenHeight * 25) / 100)));
            Log.v("TAG", "(screenHeight * 25/100) = " + ((this.screenHeight * 25) / 100));
            this.bottom_ll.layout(this.bottom_ll.getLeft(), this.bottom_ll.getTop() - ((this.screenHeight * 25) / 100), this.bottom_ll.getRight(), this.bottom_ll.getBottom() - ((this.screenHeight * 25) / 100));
            this.content.setVisibility(0);
            this.bottom_ll.startAnimation(AnimationUtils.loadAnimation(fragmentActivity, R.anim.bottom_up));
            this.isOpen = true;
            this.arrow_img.setBackgroundResource(R.drawable.arrow_down_icon);
            return;
        }
        this.isOpen = false;
        Animation bottomDown = AnimationUtils.loadAnimation(fragmentActivity, R.anim.top_down);
        AnimationUtils.loadAnimation(fragmentActivity, R.anim.top_down);
        bottomDown.setAnimationListener(new Animation.AnimationListener() {
            public void onAnimationStart(Animation arg0) {
            }

            public void onAnimationRepeat(Animation arg0) {
            }

            public void onAnimationEnd(Animation arg0) {
                Log.v("TAG", "getTop = " + (DummyActivity.this.bottom_ll.getTop() + ((DummyActivity.this.screenHeight * 25) / 100)));
                Log.v("TAG", "getBottom = " + (DummyActivity.this.bottom_ll.getBottom() + ((DummyActivity.this.screenHeight * 25) / 100)));
                DummyActivity.this.content.setVisibility(8);
                DummyActivity.this.arrow_img.setBackgroundResource(R.drawable.arrowup_icon);
                DummyActivity.this.bottom_ll.layout(DummyActivity.this.bottom_ll.getLeft(), DummyActivity.this.bottom_ll.getTop() + ((DummyActivity.this.screenHeight * 25) / 100), DummyActivity.this.bottom_ll.getRight(), DummyActivity.this.bottom_ll.getBottom() + ((DummyActivity.this.screenHeight * 25) / 100));
            }
        });
        this.bottom_ll.startAnimation(bottomDown);
    }

    private void setUpMapIfNeeded() {
        if (this.mMap == null) {
            ((SupportMapFragment) getSupportFragmentManager().findFragmentById(R.id.map)).getMapAsync(new OnMapReadyCallback() {
                @Override
                public void onMapReady(GoogleMap googleMap) {
                    DummyActivity.this.mMap = googleMap;
                    DummyActivity.this.mMap.clear();
                }
            });

        }
    }

    private void handleNewLocation(double latitude, double longitude) {
        LatLng latLng = new LatLng(latitude, longitude);
        MarkerOptions options = new MarkerOptions().position(latLng).draggable(false);
        this.mMap.clear();
        this.mMap.addMarker(options);
        this.mMap.moveCamera(CameraUpdateFactory.newLatLng(latLng));
        this.mMap.animateCamera(CameraUpdateFactory.zoomTo(13.0f));
    }

    private void topBarInit() {
        this.header_img_ll = (LinearLayout) findViewById(R.id.header_img_ll);
        this.header_img_ll.setVisibility(0);
        this.header = (TextView) findViewById(R.id.header);
        this.header.setVisibility(8);
        this.header_img = (ImageView) findViewById(R.id.header_img);
        this.header_img.setBackgroundResource(R.drawable.power);
        this.back = (LinearLayout) findViewById(R.id.back);
        this.back.setVisibility(8);
        this.menu = (LinearLayout) findViewById(R.id.menu);
        this.menu.setVisibility(0);
        this.menu.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                DummyActivity.this.startAnimation();
            }
        });
        this.settings_img = (ImageView) findViewById(R.id.settings_img);
        this.settings = (TextView) findViewById(R.id.settings);
        this.settings_img.setVisibility(0);
        this.settings.setVisibility(8);
        this.settings.setText(getResources().getString(R.string.settings));
        this.settings.setEnabled(false);
        this.help = (ImageView) findViewById(R.id.help);
        this.help.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                Intent intent1 = new Intent(DummyActivity.this, HelpScreen.class);
                intent1.putExtra("activity", "1");
                DummyActivity.this.startActivity(intent1);
            }
        });
    }

    public void startAnimation() {
        Animation anim;
        int w = this.frontLayout_operations.getMeasuredWidth();
        int h = this.frontLayout_operations.getMeasuredHeight();
        int left = (int) (((double) this.frontLayout_operations.getMeasuredWidth()) * 0.5d);
        if (!this.menuOut) {
            anim = new TranslateAnimation(0.0f, (float) left, 0.0f, 0.0f);
            this.backLayout_operations.setVisibility(0);
            this.mask_ll.setVisibility(0);
            this.animParams.init(left, 0, left + w, h);
            anim.setDuration(300);
        } else {
            anim = new TranslateAnimation(0.0f, (float) (-left), 0.0f, 0.0f);
            this.animParams.init(0, 0, w, h);
            anim.setDuration(10);
        }
        anim.setAnimationListener(this);
        anim.setFillAfter(true);
        this.frontLayout_operations.startAnimation(anim);
    }

    /* access modifiers changed from: package-private */
    public void layoutApp(boolean menuOut2) {
        this.frontLayout_operations.layout(this.animParams.left, this.animParams.top, this.animParams.right, this.animParams.bottom);
        this.frontLayout_operations.clearAnimation();
    }

    public void onAnimationStart(Animation animation) {
    }

    public void onAnimationEnd(Animation animation) {
        this.menuOut = !this.menuOut;
        if (!this.menuOut) {
            this.backLayout_operations.setVisibility(4);
            this.mask_ll.setVisibility(8);
        }
        layoutApp(this.menuOut);
        if (this.isMenuSelected) {
            this.isMenuSelected = false;
            if (this.selectedPositionFromMenu == 0) {
                this.menuListView.setAdapter(new MenuListViewAdapter(this, 0));
                Intent intent = new Intent(this, DeviceScanActivity.class);
                intent.setFlags(DriveFile.MODE_READ_ONLY);
                intent.setFlags(67108864);
                startActivity(intent);
            } else if (this.selectedPositionFromMenu == 1) {
                this.menuListView.setAdapter(new MenuListViewAdapter(this, 1));
                //Intent intent2 = new Intent(this, AddAlbumFragment1.class);
                //intent2.setFlags(DriveFile.MODE_READ_ONLY);
                //intent2.setFlags(67108864);
                //startActivity(intent2);
            } else if (this.selectedPositionFromMenu == 2) {
                this.menuListView.setAdapter(new MenuListViewAdapter(this, 2));
                Intent intent3 = new Intent(this, WebPageActivity.class);
                intent3.setFlags(DriveFile.MODE_READ_ONLY);
                intent3.setFlags(67108864);
                intent3.putExtra("PageUrl", "CustomerService");
                startActivity(intent3);
            } else if (this.selectedPositionFromMenu == 3) {
                this.menuListView.setAdapter(new MenuListViewAdapter(this, 3));
                Intent intent4 = new Intent(this, WebPageActivity.class);
                intent4.setFlags(DriveFile.MODE_READ_ONLY);
                intent4.setFlags(67108864);
                intent4.putExtra("PageUrl", "About");
                startActivity(intent4);
            } else if (this.selectedPositionFromMenu == 4) {
                this.menuListView.setAdapter(new MenuListViewAdapter(this, 4));
                Intent intent5 = new Intent(this, WebPageActivity.class);
                intent5.setFlags(DriveFile.MODE_READ_ONLY);
                intent5.setFlags(67108864);
                intent5.putExtra("PageUrl", "StoreLocator");
                startActivity(intent5);
            } else if (this.selectedPositionFromMenu == 5) {
                this.menuListView.setAdapter(new MenuListViewAdapter(this, 5));
                Intent intent6 = new Intent(this, WebPageActivity.class);
                intent6.setFlags(DriveFile.MODE_READ_ONLY);
                intent6.setFlags(67108864);
                intent6.putExtra("PageUrl", "Shop");
                startActivity(intent6);
            } else if (this.selectedPositionFromMenu == 10) {
                Intent intent7 = new Intent(this, WebActivity.class);
                intent7.setFlags(DriveFile.MODE_READ_ONLY);
                intent7.setFlags(67108864);
                startActivity(intent7);
            }
        }
    }

    public void onAnimationRepeat(Animation animation) {
    }

    public void onConnected(@Nullable Bundle bundle) {
        Location location = LocationServices.FusedLocationApi.getLastLocation(this.mGoogleApiClient);
        if (location == null) {
            Log.v("TEST", "onConnected if");
            LocationServices.FusedLocationApi.requestLocationUpdates(this.mGoogleApiClient, this.mLocationRequest, (LocationListener) this);
            return;
        }
        Log.v("TEST", "onConnected else");
        if (this.lat == 0.0d && this.lng == 0.0d) {
            handleNewLocation(location.getLatitude(), location.getLongitude());
        } else {
            handleNewLocation(this.lat, this.lng);
        }
    }

    public void onConnectionSuspended(int i) {
    }

    public void onLocationChanged(Location location) {
        if (this.lat == 0.0d && this.lng == 0.0d) {
            handleNewLocation(location.getLatitude(), location.getLongitude());
        } else {
            handleNewLocation(this.lat, this.lng);
        }
    }

    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {
        if (connectionResult.hasResolution()) {
            try {
                connectionResult.startResolutionForResult(this, CONNECTION_FAILURE_RESOLUTION_REQUEST);
            } catch (IntentSender.SendIntentException e) {
                e.printStackTrace();
            }
        } else {
            Log.v("TAG", "Location services connection failed with code " + connectionResult.getErrorCode());
        }
    }

    private void buildAlertMessageNoGps() {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setMessage("Your GPS seems to be disabled, do you want to enable it?").setCancelable(false).setPositiveButton("Yes", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
                DummyActivity.this.startActivity(new Intent("android.settings.LOCATION_SOURCE_SETTINGS"));
            }
        }).setNegativeButton("No", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
                dialog.cancel();
            }
        });
        builder.create().show();
    }
}
