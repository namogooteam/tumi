package com.incipio.tumiandroid.tumibank.activities;

import android.location.Criteria;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.location.LocationProvider;
import android.net.http.Headers;
import android.os.Bundle;
import android.view.KeyEvent;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import androidx.fragment.app.FragmentActivity;

import com.google.android.gms.common.GooglePlayServicesUtil;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.incipio.tumiandroid.R;

public class RecordLocation extends FragmentActivity implements LocationListener {
    public static final String LATITUDE = "Latitude";
    public static final String LONGITUDE = "Longitude";
    private static final String SHARED_PREFERENCES_NAME = null;
    Button back;
    EditText devicename;
    GoogleMap googleMap;
    Location location;
    LocationManager locationManager;
    GoogleApiClient mGoogleApiClient;
    Button save;

    /* access modifiers changed from: protected */
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.settings);
        this.devicename = (EditText) findViewById(R.id.devicename);
        this.back = (Button) findViewById(R.id.back);
        try {
            this.devicename.setText(DeviceScanActivity.session.loadSavedPreferences("DEVICENAME_" + DeviceControlActivity_Above.current_page));
        } catch (Exception e) {
        }
        System.out.println("DEVICE UUID in record location::::" + DeviceScanActivity.deviceUUId);
        int status = GooglePlayServicesUtil.isGooglePlayServicesAvailable(getBaseContext());
        if (status != 0) {
            GooglePlayServicesUtil.getErrorDialog(status, this, 10).show();
            return;
        }
        System.out.println("google play services avaialble..................");
        ((SupportMapFragment) getSupportFragmentManager().findFragmentById(R.id.map)).getMapAsync(new OnMapReadyCallback() {
            @Override
            public void onMapReady(GoogleMap googleMap) {
                RecordLocation.this.googleMap = googleMap;
                RecordLocation.this.googleMap.setMyLocationEnabled(true);


                RecordLocation.this.locationManager = (LocationManager) getSystemService(Headers.LOCATION);
                String provider = RecordLocation.this.locationManager.getBestProvider(new Criteria(), true);
                LocationProvider provider2 = RecordLocation.this.locationManager.getProvider("network");
                System.out.println("provider................." + provider);
                RecordLocation.this.location = RecordLocation.this.locationManager.getLastKnownLocation("network");
                if (RecordLocation.this.location != null) {
                    System.out.println("location is not null...........................");
                    onLocationChanged(RecordLocation.this.location);
                } else {
                    finish();
                }
                RecordLocation.this.locationManager.requestLocationUpdates("network", 20000, 0.0f, RecordLocation.this);
            }
        });
    }

    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if (keyCode == 4) {
            DeviceScanActivity.session.savePreferences("DEVICENAME_" + DeviceControlActivity_Above.current_page, this.devicename.getText().toString());
            System.out.println("SAVED DATA:" + DeviceScanActivity.session.loadSavedPreferences(DeviceScanActivity.deviceUUId));
            Toast.makeText(this, "Device Name Saved", 1).show();
            finish();
        }
        return super.onKeyDown(keyCode, event);
    }

    public void onLocationChanged(Location arg0) {
        System.out.println("changinggggggggggggggggggggggggggggggggggggggggggggggggggggggg");
        double latitude = this.location.getLatitude();
        double longitude = this.location.getLongitude();
        System.out.println("changinggggggggggggggggggggggggggggggggggggggggggggggggggggggg" + latitude + "   " + longitude);
        this.googleMap.moveCamera(CameraUpdateFactory.newLatLng(new LatLng(latitude, longitude)));
        this.googleMap.animateCamera(CameraUpdateFactory.zoomTo(15.0f));
        DeviceScanActivity.session.setLastLocation(LATITUDE, this.location.getLatitude(), LONGITUDE, this.location.getLongitude());
        finish();
    }

    public void onStatusChanged(String provider, int status, Bundle extras) {
    }

    public void onProviderEnabled(String provider) {
    }

    public void onProviderDisabled(String provider) {
    }
}
