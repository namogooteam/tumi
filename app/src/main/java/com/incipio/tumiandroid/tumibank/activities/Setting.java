package com.incipio.tumiandroid.tumibank.activities;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Typeface;
import android.location.Location;
import android.os.Build;
import android.os.Bundle;
import android.text.Editable;
import android.text.Html;
import android.text.TextWatcher;
import android.util.Log;
import android.view.KeyEvent;
import android.view.MotionEvent;
import android.view.View;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.SeekBar;
import android.widget.Switch;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.ToggleButton;

import androidx.fragment.app.FragmentActivity;

import com.google.android.gms.drive.DriveFile;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;
import com.incipio.tumiandroid.R;
import com.incipio.tumiandroid.audio.activities.AmbientSoundsActivity;
import com.incipio.tumiandroid.audio.activities.SelectDeviceActivity;
import com.incipio.tumiandroid.tumibank.utils.SavedPreferences;
import com.incipio.tumiandroid.tumibank.utils.Utility;

public class Setting extends FragmentActivity {
    public static boolean ischecked = true;
    public static SavedPreferences session;
    public static int threshhold;
    private LinearLayout ambient_sounds_ll;
    private LinearLayout back;
    private Boolean checked = true;
    /* access modifiers changed from: private */
    public String device_name_str = "";
    /* access modifiers changed from: private */
    public EditText devicename;
    private TextView disconnect_bank;
    int fromscan = 0;
    ImageView mClearText;
    private GoogleMap mMap;
    private LinearLayout map_height;
    private LinearLayout parrent_ll;
    private LinearLayout play_music_ll;
    private TextView privacy;
    /* access modifiers changed from: private */
    public int progressChanged = 0;
    private RelativeLayout relativeLayout1;
    private SeekBar seekControl = null;
    /* access modifiers changed from: private */
    public TextView seekval;
    private String seekval_string = "";
    private int seekvalue = 0;
    /* access modifiers changed from: private */
    public Switch swith;
    /* access modifiers changed from: private */
    public Switch swith1;
    private Boolean swithchecked = false;
    private TextView title;
    private ToggleButton toggle;
    /* access modifiers changed from: private */
    public ToggleButton toggle1;
    private Boolean togglechecked = false;
    private TextView version;

    /* access modifiers changed from: protected */
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.settings);
        BottomBarInit();
        session = new SavedPreferences(getApplicationContext());
        this.devicename = (EditText) findViewById(R.id.devicename);
        this.back = (LinearLayout) findViewById(R.id.connectagain);
        this.swith = (Switch) findViewById(R.id.swith);
        this.swith1 = (Switch) findViewById(R.id.swith1);
        this.toggle = (ToggleButton) findViewById(R.id.toggle);
        this.toggle1 = (ToggleButton) findViewById(R.id.toggle1);
        this.seekval = (TextView) findViewById(R.id.txtvalseek);
        this.title = (TextView) findViewById(R.id.title);
        this.map_height = (LinearLayout) findViewById(R.id.map_ll);
        this.map_height.setVisibility(8);
        this.mClearText = (ImageView) findViewById(R.id.clearText);
        this.mClearText.setVisibility(4);
        this.mClearText.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                Setting.this.devicename.setText("");
                Setting.this.mClearText.setVisibility(8);
            }
        });
        this.devicename.addTextChangedListener(new TextWatcher() {
            public void afterTextChanged(Editable s) {
            }

            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }

            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if (s.length() != 0) {
                    Setting.this.mClearText.setVisibility(0);
                } else {
                    Setting.this.mClearText.setVisibility(8);
                }
            }
        });
        this.relativeLayout1 = (RelativeLayout) findViewById(R.id.relativeLayout1);
        this.relativeLayout1.setOnTouchListener(new View.OnTouchListener() {
            public boolean onTouch(View view, MotionEvent motionEvent) {
                Utility.hideKeyboard(Setting.this, view, false);
                return false;
            }
        });
        this.parrent_ll = (LinearLayout) findViewById(R.id.parrent_ll);
        this.parrent_ll.setOnTouchListener(new View.OnTouchListener() {
            public boolean onTouch(View view, MotionEvent motionEvent) {
                Utility.hideKeyboard(Setting.this, view, false);
                return false;
            }
        });
        this.disconnect_bank = (TextView) findViewById(R.id.disconnect_bank);
        this.disconnect_bank.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                Setting.this.fromscan = 1;
                Intent i = new Intent(Setting.this, DeviceScanActivity.class);
                i.addFlags(67108864);
                i.addFlags(DriveFile.MODE_READ_ONLY);
                Setting.this.startActivity(i);
                Setting.this.finish();
            }
        });
        this.privacy = (TextView) findViewById(R.id.privacypolicy);
        this.privacy.setText(Html.fromHtml("<u>Privacy Policy</u>"));
        this.privacy.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                Setting.this.fromscan = 2;
                Intent intent = new Intent(Setting.this, WebActivity.class);
                intent.setFlags(DriveFile.MODE_READ_ONLY);
                intent.setFlags(67108864);
                Setting.this.startActivity(intent);
            }
        });
        String versionName = null;
        try {
            versionName = getPackageManager().getPackageInfo(getPackageName(), 0).versionName;
        } catch (PackageManager.NameNotFoundException e) {
            e.printStackTrace();
        }
        this.version = (TextView) findViewById(R.id.versionnumber);
        this.version.setText("v " + versionName);
        String str = Build.VERSION.RELEASE;
        int i = Build.VERSION.SDK_INT;
        this.toggle.setVisibility(0);
        this.toggle1.setVisibility(0);
        this.swith.setVisibility(8);
        this.swith1.setVisibility(8);
        this.device_name_str = DeviceControlActivity_Above.EXTRAS_DEVICE_NAME.trim();
        this.devicename.setText(this.device_name_str);
        this.checked = Boolean.valueOf(Boolean.parseBoolean(DeviceScanActivity.session.loadSavedPreferences2("DEVICENAME_" + DeviceControlActivity_Above.current_page + "|CHECKED")));
        this.seekval_string = DeviceScanActivity.session.loadSavedPreferences3("DEVICENAME_" + DeviceControlActivity_Above.current_page + "|THRESHOLD") + " %";
        this.seekvalue = Integer.parseInt(DeviceScanActivity.session.loadSavedPreferences3("DEVICENAME_" + DeviceControlActivity_Above.current_page + "|THRESHOLD"));
        this.swithchecked = Boolean.valueOf(Boolean.parseBoolean(DeviceScanActivity.session.loadSavedPreferences2("DEVICENAME_" + DeviceControlActivity_Above.current_page + "|CHECKED")));
        this.togglechecked = Boolean.valueOf(Boolean.parseBoolean(DeviceScanActivity.session.loadSavedPreferences2("DEVICENAME_" + DeviceControlActivity_Above.current_page + "|CHECKED")));
        Typeface createFromAsset = Typeface.createFromAsset(getAssets(), "fonts/proximanova_regular.otf");
        this.title.setTypeface(Typeface.createFromAsset(getAssets(), "fonts/gothammedium.otf"));
        try {
            this.fromscan = getIntent().getExtras().getInt("fromscan");
        } catch (Exception e2) {
        }
        Log.v("TAG", "seekvalue = " + this.seekvalue);
        this.seekControl = (SeekBar) findViewById(R.id.seek1);
        if (this.checked.booleanValue()) {
            this.toggle.setChecked(true);
            this.swith.setChecked(true);
        } else {
            this.toggle.setChecked(false);
            this.swith.setChecked(false);
        }
        if (session.getBLENotifications("Notifications")) {
            this.toggle1.setChecked(true);
            this.swith1.setChecked(true);
        } else {
            this.toggle1.setChecked(false);
            this.swith1.setChecked(false);
        }
        try {
            this.seekval.setText(this.seekval_string);
            this.seekControl.setProgress(this.seekvalue);
            this.swith.setChecked(this.swithchecked.booleanValue());
            this.toggle.setChecked(this.togglechecked.booleanValue());
        } catch (Exception e3) {
            this.seekval.setText("0 %");
            this.seekControl.setProgress(0);
            this.swith.setChecked(false);
            this.toggle.setChecked(false);
        }
        if (this.seekvalue == 0) {
            this.seekvalue = 30;
            this.seekControl.setProgress(this.seekvalue);
            this.seekval.setText("30 %");
        }
        this.progressChanged = this.seekvalue;
        this.seekControl.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
                int unused = Setting.this.progressChanged = progress;
            }

            public void onStartTrackingTouch(SeekBar seekBar) {
            }

            public void onStopTrackingTouch(SeekBar seekBar) {
                Setting.this.seekval.setText(Setting.this.progressChanged + " %");
                DeviceControlActivity_Above.isShowing = false;
            }
        });
        this.swith.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                Setting.ischecked = isChecked;
                if (isChecked) {
                    Setting.this.swith.setChecked(true);
                } else {
                    Setting.this.swith.setChecked(false);
                }
            }
        });
        this.swith1.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked) {
                    Setting.this.swith1.setChecked(true);
                } else {
                    Setting.this.swith1.setChecked(false);
                }
            }
        });
        this.toggle.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                Setting.ischecked = isChecked;
                if (isChecked) {
                }
            }
        });
        this.toggle1.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked) {
                    Setting.this.toggle1.setChecked(true);
                } else {
                    Setting.this.toggle1.setChecked(false);
                }
            }
        });
        System.out.println("DEVICE UUID::::" + DeviceScanActivity.deviceUUId);
        this.back.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                if (Setting.this.fromscan == 0) {
                    Setting.threshhold = Setting.this.progressChanged;
                    System.out.println("Saving d device::::" + Setting.this.devicename.getText().toString());
                    if (Setting.this.devicename.getText().toString().trim().length() == 0) {
                        Toast.makeText(Setting.this, "Device name should not be empty.", 0).show();
                        return;
                    }
                    String str = Build.VERSION.RELEASE;
                    int i = Build.VERSION.SDK_INT;
                    System.out.println("Current device no.............." + DeviceControlActivity_Above.current_page);
                    String deviceUUId = DeviceScanActivity.session.loadSavedPreferences("DEVICE_" + DeviceControlActivity_Above.current_page);
                    DeviceScanActivity.session.savePreferences("DEVICENAME_" + DeviceControlActivity_Above.current_page, Setting.this.devicename.getText().toString());
                    DeviceScanActivity.session.savePreferences("DEVICENAME_" + deviceUUId, Setting.this.devicename.getText().toString().trim());
                    DeviceScanActivity.session.savePreferences2("DEVICENAME_" + DeviceControlActivity_Above.current_page + "|CHECKED", Setting.ischecked + "", "DEVICENAME_" + DeviceControlActivity_Above.current_page + "|THRESHOLD", Setting.threshhold + "");
                    String loadSavedPreferences = DeviceScanActivity.session.loadSavedPreferences("DEVICEBLEADDRESS_" + DeviceControlActivity_Above.current_page);
                    Setting.session.BLENotifications("Notifications", Boolean.valueOf(Setting.this.toggle1.isChecked()));
                    if (!Setting.this.device_name_str.equals(Setting.this.devicename.getText().toString().trim())) {
                        DeviceControlActivity_Above.EXTRAS_DEVICE_NAME = Setting.this.devicename.getText().toString().trim();
                        Toast.makeText(Setting.this, "Successfully device name changed.", 1).show();
                    }
                    Setting.this.finish();
                } else if (Setting.this.fromscan != 2 && Setting.this.fromscan != 0) {
                    Setting.this.finish();
                }
            }
        });
    }

    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if (keyCode != 4 && keyCode != 3) {
            return super.onKeyDown(keyCode, event);
        }
        Log.v("TEST", "KEYCODE_BACK");
        Log.v("INFO", "fromscan = " + this.fromscan);
        if (this.fromscan == 0) {
            threshhold = this.progressChanged;
            System.out.println("Saving d device::::" + this.devicename.getText().toString());
            if (this.devicename.getText().toString().trim().length() == 0) {
                Toast.makeText(this, "Device name should not be empty.", 0).show();
                return false;
            }
            String str = Build.VERSION.RELEASE;
            int i = Build.VERSION.SDK_INT;
            System.out.println("Current device no.............." + DeviceControlActivity_Above.current_page);
            String deviceUUId = DeviceScanActivity.session.loadSavedPreferences("DEVICE_" + DeviceControlActivity_Above.current_page);
            DeviceScanActivity.session.savePreferences("DEVICENAME_" + DeviceControlActivity_Above.current_page, this.devicename.getText().toString());
            DeviceScanActivity.session.savePreferences("DEVICENAME_" + deviceUUId, this.devicename.getText().toString());
            DeviceScanActivity.session.savePreferences2("DEVICENAME_" + DeviceControlActivity_Above.current_page + "|CHECKED", ischecked + "", "DEVICENAME_" + DeviceControlActivity_Above.current_page + "|THRESHOLD", threshhold + "");
            String loadSavedPreferences = DeviceScanActivity.session.loadSavedPreferences("DEVICEBLEADDRESS_" + DeviceControlActivity_Above.current_page);
            session.BLENotifications("Notifications", Boolean.valueOf(this.toggle1.isChecked()));
            if (!this.device_name_str.equals(this.devicename.getText().toString().trim())) {
                DeviceControlActivity_Above.EXTRAS_DEVICE_NAME = this.devicename.getText().toString().trim();
                Toast.makeText(this, "Successfully device name changed.", 1).show();
            }
            finish();
            return super.onKeyDown(keyCode, event);
        }
        finish();
        return super.onKeyDown(keyCode, event);
    }

    private void setUpMapIfNeeded() {
        if (this.mMap == null) {
            ((SupportMapFragment) getSupportFragmentManager().findFragmentById(R.id.map)).getMapAsync(new OnMapReadyCallback() {
                @Override
                public void onMapReady(GoogleMap googleMap) {
                    Setting.this.mMap = googleMap;
                    Setting.this.mMap.clear();
                }
            });

        }
    }

    private void handleNewLocation(Location location) {
        LatLng latLng = new LatLng(location.getLatitude(), location.getLongitude());
        MarkerOptions options = new MarkerOptions().position(latLng).draggable(false);
        this.mMap.clear();
        this.mMap.addMarker(options);
        this.mMap.moveCamera(CameraUpdateFactory.newLatLng(latLng));
        this.mMap.animateCamera(CameraUpdateFactory.zoomTo(18.0f));
    }

    private void buildAlertMessageNoGps() {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setMessage("Your GPS seems to be disabled, do you want to enable it?").setCancelable(false).setPositiveButton("Yes", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
                Setting.this.startActivity(new Intent("android.settings.LOCATION_SOURCE_SETTINGS"));
            }
        }).setNegativeButton("No", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
                dialog.cancel();
            }
        });
        builder.create().show();
    }

    /* access modifiers changed from: protected */
    public void onUserLeaveHint() {
        super.onUserLeaveHint();
        Log.v("TEST", "KEYCODE_HOME");
        if (this.fromscan == 0) {
            threshhold = this.progressChanged;
            if (this.devicename.getText().toString().trim().length() == 0) {
                Toast.makeText(this, "Device name should not be empty.", 0).show();
                return;
            }
            System.out.println("Saving d device::::" + this.devicename.getText().toString());
            String str = Build.VERSION.RELEASE;
            int i = Build.VERSION.SDK_INT;
            System.out.println("Current device no.............." + DeviceControlActivity_Above.current_page);
            String deviceUUId = DeviceScanActivity.session.loadSavedPreferences("DEVICE_" + DeviceControlActivity_Above.current_page);
            DeviceScanActivity.session.savePreferences("DEVICENAME_" + DeviceControlActivity_Above.current_page, this.devicename.getText().toString());
            DeviceScanActivity.session.savePreferences("DEVICENAME_" + deviceUUId, this.devicename.getText().toString());
            DeviceScanActivity.session.savePreferences2("DEVICENAME_" + DeviceControlActivity_Above.current_page + "|CHECKED", ischecked + "", "DEVICENAME_" + DeviceControlActivity_Above.current_page + "|THRESHOLD", threshhold + "");
            String loadSavedPreferences = DeviceScanActivity.session.loadSavedPreferences("DEVICEBLEADDRESS_" + DeviceControlActivity_Above.current_page);
            session.BLENotifications("Notifications", Boolean.valueOf(this.toggle1.isChecked()));
            if (!this.device_name_str.equals(this.devicename.getText().toString().trim())) {
                DeviceControlActivity_Above.EXTRAS_DEVICE_NAME = this.devicename.getText().toString().trim();
                Toast.makeText(this, "Successfully device name changed.", 1).show();
            }
            finish();
            return;
        }
        finish();
    }

    private void BottomBarInit() {
        this.play_music_ll = (LinearLayout) findViewById(R.id.play_music_ll);
        this.ambient_sounds_ll = (LinearLayout) findViewById(R.id.ambient_sounds_ll);
        this.play_music_ll.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                Log.v("TAG", "play_music_ll");
                Setting.this.fromscan = 1;
                Setting.this.startActivity(new Intent(Setting.this, SelectDeviceActivity.class));
            }
        });
        this.ambient_sounds_ll.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                Log.v("TAG", "ambient_sounds_ll");
                Setting.this.fromscan = 1;
                Setting.this.startActivity(new Intent(Setting.this, AmbientSoundsActivity.class));
            }
        });
    }
}
