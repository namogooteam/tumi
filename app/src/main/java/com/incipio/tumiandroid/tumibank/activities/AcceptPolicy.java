package com.incipio.tumiandroid.tumibank.activities;

import android.app.Activity;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import com.incipio.tumiandroid.R;
import com.incipio.tumiandroid.audio.activities.SelectDeviceActivity;

public class AcceptPolicy extends Activity {
    Button agree;
    Button policy;
    SharedPreferences prefs = null;

    /* access modifiers changed from: protected */
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.policy);
        this.policy = (Button) findViewById(R.id.policy);
        this.agree = (Button) findViewById(R.id.accept);
        this.prefs = getSharedPreferences("com.incipio.tumiandroid.tumibank.activities", 0);
        if (isFirstLaunch() || this.prefs.getBoolean("firstrun", false)) {
            this.policy.setOnClickListener(new View.OnClickListener() {
                public void onClick(View v) {
                    AcceptPolicy.this.startActivity(new Intent(AcceptPolicy.this, WebActivity.class));
                }
            });
            this.agree.setOnClickListener(new View.OnClickListener() {
                public void onClick(View v) {
                    AcceptPolicy.this.startActivity(new Intent(AcceptPolicy.this, SelectDeviceActivity.class));
                    AcceptPolicy.this.finish();
                }
            });
            return;
        }
        startActivity(new Intent(this, SelectDeviceActivity.class));
        finish();
    }

    private boolean isFirstLaunch() {
        return this.prefs.getBoolean("firstrun", true);
    }

    /* access modifiers changed from: protected */
    public void onResume() {
        super.onResume();
        if (this.prefs.getBoolean("firstrun", true)) {
            this.prefs.edit().putBoolean("firstrun", false).commit();
        }
    }
}
