package com.incipio.tumiandroid.tumibank.activities;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.ImageView;
import com.incipio.tumiandroid.R;

public class OffersActivity extends Activity {
    WebView wv1;

    /* access modifiers changed from: protected */
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.offers);
        this.wv1 = (WebView) findViewById(R.id.webView);
        this.wv1.setWebViewClient(new MyBrowser());
        this.wv1.getSettings().setLoadsImagesAutomatically(true);
        this.wv1.getSettings().setJavaScriptEnabled(true);
        this.wv1.setScrollBarStyle(0);
        this.wv1.loadUrl("http://dev.ncpo.cc/apps/tumi/tumiadpage.html ");
        ((ImageView) findViewById(R.id.close)).setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                OffersActivity.this.startActivity(new Intent(OffersActivity.this, DeviceScanActivity.class));
                OffersActivity.this.finish();
            }
        });
    }

    private class MyBrowser extends WebViewClient {
        private MyBrowser() {
        }

        public boolean shouldOverrideUrlLoading(WebView view, String url) {
            view.loadUrl(url);
            return true;
        }
    }
}
