package com.incipio.tumiandroid.tumibank.activities;

import android.app.Activity;
import android.os.Bundle;
import android.view.View;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.Button;
import com.incipio.tumiandroid.R;

public class WebActivity extends Activity {
    Button close;
    private WebView wv1;

    /* access modifiers changed from: protected */
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.privacy);
        this.wv1 = (WebView) findViewById(R.id.webView);
        this.wv1.setWebViewClient(new MyBrowser());
        this.wv1.getSettings().setLoadsImagesAutomatically(true);
        this.wv1.getSettings().setJavaScriptEnabled(true);
        this.wv1.setScrollBarStyle(0);
        this.wv1.loadUrl("http://dev.ncpo.cc/apps/tumi/privacypolicy.html");
        this.close = (Button) findViewById(R.id.close);
        this.close.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                WebActivity.this.finish();
            }
        });
    }

    private class MyBrowser extends WebViewClient {
        private MyBrowser() {
        }

        public boolean shouldOverrideUrlLoading(WebView view, String url) {
            view.loadUrl(url);
            return true;
        }
    }
}
