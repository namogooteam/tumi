package com.incipio.tumiandroid.tumibank.activities;

import android.app.Service;
import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.bluetooth.BluetoothGatt;
import android.bluetooth.BluetoothGattCallback;
import android.bluetooth.BluetoothGattCharacteristic;
import android.bluetooth.BluetoothGattService;
import android.bluetooth.BluetoothManager;
import android.content.Intent;
import android.os.Binder;
import android.os.Bundle;
import android.os.IBinder;
import com.incipio.tumiandroid.tumibank.utils.SampleGattAttributes;
import java.util.List;
import java.util.UUID;

public class BluetoothLeService extends Service {
    public static final String ACTION_DATA_AVAILABLE = "com.example.bluetooth.le.ACTION_DATA_AVAILABLE";
    public static final String ACTION_GATT_CONNECTED = "com.example.bluetooth.le.ACTION_GATT_CONNECTED";
    public static final String ACTION_GATT_DISCONNECTED = "com.example.bluetooth.le.ACTION_GATT_DISCONNECTED";
    public static final String ACTION_GATT_SERVICES_DISCOVERED = "com.example.bluetooth.le.ACTION_GATT_SERVICES_DISCOVERED";
    public static final String ACTION_PORTDATA_AVAILABLE = "com.example.bluetooth.le.ACTION_PORTDATA_AVAILABLE";
    public static final String EXTRA_CHARGE_STATUS = "com.example.bluetooth.le.EXTRA_CHARGE_STATUS";
    public static final String EXTRA_DATA = "com.example.bluetooth.le.EXTRA_DATA";
    public static final String EXTRA_HIGHBYTES = "com.example.bluetooth.le.EXTRA_HIGHBYTES";
    public static final String EXTRA_HOURS = "com.example.bluetooth.le.EXTRA_HOURS";
    public static final String EXTRA_LOWBYTES = "com.example.bluetooth.le.EXTRA_LOWBYTES";
    public static final String EXTRA_MIN = "com.example.bluetooth.le.EXTRA_MIN";
    public static final String EXTRA_PORT_DATA = "com.example.bluetooth.le.EXTRA_PORTDATA";
    private static final int STATE_CONNECTED = 2;
    private static final int STATE_CONNECTING = 1;
    private static final int STATE_DISCONNECTED = 0;
    private static final String TAG = BluetoothLeService.class.getSimpleName();
    public static final UUID UUID_BatteryService = UUID.fromString(SampleGattAttributes.BATTERY_SERVICE);
    public static final UUID UUID_FINDME = UUID.fromString(SampleGattAttributes.OffGRID_CHARACTERISITC);
    public static final UUID UUID_HEART_RATE_MEASUREMENT = UUID.fromString(SampleGattAttributes.HEART_RATE_MEASUREMENT);
    public static final UUID UUID_NotifyServiceCharacteristic = UUID.fromString(SampleGattAttributes.NOTIFY_CHARACTERISTIC);
    public static final UUID UUID_PORT1 = UUID.fromString(SampleGattAttributes.OffGRID_CHARACTERISITC);
    public static final UUID UUID_PORT2 = UUID.fromString(SampleGattAttributes.OffGRID_CHARACTERISITC);
    public static final UUID UUID_PrtNotifyServiceCharacteristic = UUID.fromString(SampleGattAttributes.PORT_NOTIFY_CHARACTERISTIC);
    private final IBinder mBinder = new LocalBinder();
    private BluetoothAdapter mBluetoothAdapter;
    private String mBluetoothDeviceAddress;
    /* access modifiers changed from: private */
    public BluetoothGatt mBluetoothGatt;
    private BluetoothManager mBluetoothManager;
    /* access modifiers changed from: private */
    public int mConnectionState = 0;
    private final BluetoothGattCallback mGattCallback = new BluetoothGattCallback() {
        public void onConnectionStateChange(BluetoothGatt gatt, int status, int newState) {
            if (newState == 2) {
                int unused = BluetoothLeService.this.mConnectionState = 2;
                BluetoothLeService.this.broadcastUpdate(BluetoothLeService.ACTION_GATT_CONNECTED);
                BluetoothLeService.this.mBluetoothGatt.discoverServices();
            } else if (newState == 0) {
                int unused2 = BluetoothLeService.this.mConnectionState = 0;
                BluetoothLeService.this.broadcastUpdate(BluetoothLeService.ACTION_GATT_DISCONNECTED);
            }
        }

        public void onServicesDiscovered(BluetoothGatt gatt, int status) {
            System.out.println("Services discovered:" + status);
            if (status == 0) {
                BluetoothLeService.this.broadcastUpdate(BluetoothLeService.ACTION_GATT_SERVICES_DISCOVERED);
            }
        }

        public void onCharacteristicRead(BluetoothGatt gatt, BluetoothGattCharacteristic characteristic, int status) {
            System.out.println("STATUS OF READ:" + status);
            if (status == 0) {
                System.out.println("DATA is AVAIALBEL IN READ");
                BluetoothLeService.this.broadcastUpdate(BluetoothLeService.ACTION_DATA_AVAILABLE, characteristic);
            }
        }

        public void onCharacteristicChanged(BluetoothGatt gatt, BluetoothGattCharacteristic characteristic) {
            System.out.println("DATA is AVAIALBEL IN CHANGE");
            BluetoothLeService.this.broadcastUpdate(BluetoothLeService.ACTION_DATA_AVAILABLE, characteristic);
        }
    };

    /* access modifiers changed from: private */
    public void broadcastUpdate(String action) {
        sendBroadcast(new Intent(action));
    }

    /* access modifiers changed from: private */
    public void broadcastUpdate(String action, BluetoothGattCharacteristic characteristic) {
        Intent intent = new Intent(action);
        System.out.println("in broadcastupdate............" + characteristic.getValue());
        if (SampleGattAttributes.PORT_NOTIFY_CHARACTERISTIC.toString().equalsIgnoreCase(characteristic.getUuid().toString())) {
            System.out.println("getting the port broadcast valuessss%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%" + characteristic.getValue());
            intent.putExtra(EXTRA_PORT_DATA, String.valueOf(characteristic.getValue()[8]));
        }
        if (SampleGattAttributes.NOTIFY_CHARACTERISTIC.toString().equalsIgnoreCase(characteristic.getUuid().toString())) {
            System.out.println("in broadcastupdate....dfdfv........");
            byte[] data = characteristic.getValue();
            System.out.println("DATA ASSOCIATED:" + data);
            if (data != null && data.length > 0) {
                System.out.println("DATA----------------------------------" + data[0] + "...." + data[1] + "-------" + data[2] + "---" + data[3] + "...." + data[4] + "...." + data[5] + "---------" + data[6] + "......" + data[7]);
                Bundle b = new Bundle();
                b.putString(EXTRA_CHARGE_STATUS, String.valueOf(data[1]));
                b.putString(EXTRA_DATA, String.valueOf(data[3]));
                b.putString(EXTRA_MIN, String.valueOf(data[7]));
                b.putString(EXTRA_HOURS, String.valueOf(data[6]));
                b.putString(EXTRA_HIGHBYTES, String.valueOf(data[4]));
                b.putString(EXTRA_LOWBYTES, String.valueOf(data[5]));
                intent.putExtra(EXTRA_PORT_DATA, "100");
                intent.putExtras(b);
            }
        }
        sendBroadcast(intent);
    }

    public class LocalBinder extends Binder {
        public LocalBinder() {
        }

        /* access modifiers changed from: package-private */
        public BluetoothLeService getService() {
            return BluetoothLeService.this;
        }
    }

    public IBinder onBind(Intent intent) {
        return this.mBinder;
    }

    public boolean onUnbind(Intent intent) {
        close();
        return super.onUnbind(intent);
    }

    public boolean initialize() {
        if (this.mBluetoothManager == null) {
            this.mBluetoothManager = (BluetoothManager) getSystemService("bluetooth");
            if (this.mBluetoothManager == null) {
                return false;
            }
        }
        this.mBluetoothAdapter = this.mBluetoothManager.getAdapter();
        if (this.mBluetoothAdapter == null) {
            return false;
        }
        return true;
    }

    public boolean connect(String address) {
        if (this.mBluetoothAdapter == null || address == null) {
            return false;
        }
        if (this.mBluetoothDeviceAddress == null || !address.equals(this.mBluetoothDeviceAddress) || this.mBluetoothGatt == null) {
            BluetoothDevice device = this.mBluetoothAdapter.getRemoteDevice(address);
            if (device == null) {
                return false;
            }
            this.mBluetoothGatt = device.connectGatt(this, false, this.mGattCallback);
            this.mBluetoothDeviceAddress = address;
            this.mConnectionState = 1;
            return true;
        } else if (!this.mBluetoothGatt.connect()) {
            return false;
        } else {
            this.mConnectionState = 1;
            return true;
        }
    }

    public void disconnect() {
        if (this.mBluetoothAdapter != null && this.mBluetoothGatt != null) {
            this.mBluetoothGatt.disconnect();
        }
    }

    public void close() {
        if (this.mBluetoothGatt != null) {
            this.mBluetoothGatt.close();
            this.mBluetoothGatt = null;
        }
    }

    public void readCharacteristic(BluetoothGattCharacteristic characteristic) {
        if (this.mBluetoothAdapter != null && this.mBluetoothGatt != null) {
            System.out.println("Read characterisitc:" + String.valueOf(characteristic));
            this.mBluetoothGatt.readCharacteristic(characteristic);
        }
    }

    public boolean writeCharacteristic(BluetoothGattCharacteristic i) {
        if (this.mBluetoothGatt == null || this.mBluetoothGatt.getService(UUID_BatteryService) == null) {
            return false;
        }
        return this.mBluetoothGatt.writeCharacteristic(i);
    }

    public boolean writeCharacteristic(byte[] value, int type) {
        BluetoothGattService Service;
        if (this.mBluetoothGatt == null || (Service = this.mBluetoothGatt.getService(UUID_BatteryService)) == null) {
            return false;
        }
        BluetoothGattCharacteristic charac = null;
        if (type == 1) {
            charac = Service.getCharacteristic(UUID_PORT1);
        } else if (type == 2) {
            charac = Service.getCharacteristic(UUID_PORT2);
        } else if (type == 3) {
            charac = Service.getCharacteristic(UUID_FINDME);
        }
        if (charac == null) {
            return false;
        }
        charac.setValue(value);
        return this.mBluetoothGatt.writeCharacteristic(charac);
    }

    public void setCharacteristicNotification(BluetoothGattCharacteristic characteristic, boolean enabled) {
        if (this.mBluetoothAdapter != null && this.mBluetoothGatt != null) {
            this.mBluetoothGatt.setCharacteristicNotification(characteristic, enabled);
            System.out.println("BATTERY  LEVEL" + characteristic.getValue());
            if (UUID_NotifyServiceCharacteristic.equals(characteristic.getUuid())) {
                System.out.println("vAUE:::" + characteristic.getValue());
            }
            if (UUID_PrtNotifyServiceCharacteristic.equals(characteristic.getUuid())) {
                System.out.println("PORTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTT:::" + characteristic.getValue());
            }
        }
    }

    public List<BluetoothGattService> getSupportedGattServices() {
        if (this.mBluetoothGatt == null) {
            return null;
        }
        return this.mBluetoothGatt.getServices();
    }
}
