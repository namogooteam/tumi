package com.incipio.tumiandroid.tumibank.activities;

import android.app.Activity;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.BitmapDrawable;
import android.os.Bundle;
import android.util.DisplayMetrics;
import android.util.Log;
import android.widget.RelativeLayout;
import android.widget.TextView;
import com.incipio.tumiandroid.R;
import com.incipio.tumiandroid.tumibank.activities.RealViewSwitcher;

public class LearnMore extends Activity {
    RelativeLayout mainlayout;
    private final RealViewSwitcher.OnScreenSwitchListener onScreenSwitchListener = new RealViewSwitcher.OnScreenSwitchListener() {
        public void onScreenSwitched(int screen) {
            if (screen == 6) {
                LearnMore.this.finish();
            }
            Log.d("RealViewSwitcher", "switched to screen: " + screen);
        }
    };
    RealViewSwitcher realViewSwitcher;

    /* access modifiers changed from: protected */
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(1);
        getWindow().setFlags(1024, 1024);
        setContentView(R.layout.learnmore);
        DisplayMetrics metrics = getApplicationContext().getResources().getDisplayMetrics();
        int width = metrics.widthPixels;
        int height = metrics.heightPixels;
        System.out.println("WIDTH:" + width + "/n Height:" + height + "\n Density:" + metrics.densityDpi);
        this.realViewSwitcher = (RealViewSwitcher) findViewById(R.id.realviewswitcher);
        this.mainlayout = (RelativeLayout) findViewById(R.id.mainlayout);
        int[] backgrounds = {R.drawable.help_1, R.drawable.help_2a, R.drawable.help_2b, R.drawable.help_3, R.drawable.help_4a, R.drawable.help_4b};
        for (int i = 0; i < 6; i++) {
            TextView textView = new TextView(getApplicationContext());
            textView.setLayoutParams(new RelativeLayout.LayoutParams(width, height));
            textView.setGravity(16);
            try {
                textView.setBackground(new BitmapDrawable(getResources(), decodeSampledBitmapFromResource(getResources(), backgrounds[i], width, height)));
            } catch (Exception e) {
                e.printStackTrace();
            }
            this.realViewSwitcher.addView(textView);
        }
        this.realViewSwitcher.setOnScreenSwitchListener(this.onScreenSwitchListener);
    }

    public static Bitmap decodeSampledBitmapFromResource(Resources res, int resId, int reqWidth, int reqHeight) {
        BitmapFactory.Options options = new BitmapFactory.Options();
        options.inJustDecodeBounds = true;
        BitmapFactory.decodeResource(res, resId, options);
        options.inSampleSize = calculateInSampleSize(options, reqWidth, reqHeight);
        options.inJustDecodeBounds = false;
        return BitmapFactory.decodeResource(res, resId, options);
    }

    public static int calculateInSampleSize(BitmapFactory.Options options, int reqWidth, int reqHeight) {
        int height = options.outHeight;
        int width = options.outWidth;
        int inSampleSize = 1;
        if (height > reqHeight || width > reqWidth) {
            int halfHeight = height / 2;
            int halfWidth = width / 2;
            while (halfHeight / inSampleSize > reqHeight && halfWidth / inSampleSize > reqWidth) {
                inSampleSize *= 2;
            }
        }
        return inSampleSize;
    }
}
