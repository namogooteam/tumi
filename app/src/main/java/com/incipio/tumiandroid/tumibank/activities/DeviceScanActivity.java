package com.incipio.tumiandroid.tumibank.activities;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.bluetooth.BluetoothManager;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.Typeface;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.provider.Settings;
import android.text.Html;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.AdapterView;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;
import com.google.android.gms.drive.DriveFile;
import com.incipio.tumiandroid.R;
import com.incipio.tumiandroid.audio.activities.AmbientSoundsActivity;
import com.incipio.tumiandroid.audio.activities.SelectDeviceActivity;
import com.incipio.tumiandroid.tumibank.utils.SavedPreferences;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;
import net.hockeyapp.android.CrashManager;
import net.hockeyapp.android.UpdateManager;

public class DeviceScanActivity extends Activity {
    private static final String LATITUDE = "Latitude";
    private static final String LONGITUDE = "Longitude";
    private static final int REQUEST_ENABLE_BT = 1;
    private static final long SCAN_PERIOD = 6000;
    private static final String SHARED_PREFERENCES_NAME = null;
    static String deviceUUId;
    static SavedPreferences session;
    static String universaldevice;
    private UUID[] UUIDS;
    private Button add;
    private LinearLayout ambient_sounds_ll;
    /* access modifiers changed from: private */
    public ArrayList<String> bluetoothaddresses = new ArrayList<>();
    /* access modifiers changed from: private */
    public CodeLearnAdapter chapterListAdapter;
    /* access modifiers changed from: private */
    public int countval = 0;
    /* access modifiers changed from: private */
    public ArrayList<String> deviceaddressssaved = new ArrayList<>();
    /* access modifiers changed from: private */
    public ArrayList<String> deviceavailable = new ArrayList<>();
    /* access modifiers changed from: private */
    public ArrayList<String> devicenamessaved = new ArrayList<>();
    private TextView devices;
    private TextView devices1;
    /* access modifiers changed from: private */
    public Dialog dialog;
    /* access modifiers changed from: private */
    public TextView edit;
    /* access modifiers changed from: private */
    public Typeface gotham_light_tf;
    ImageView help;
    /* access modifiers changed from: private */
    public ListView listView;
    /* access modifiers changed from: private */
    public ListView listView1;
    /* access modifiers changed from: private */
    public BluetoothAdapter mBluetoothAdapter;
    private Handler mHandler;
    /* access modifiers changed from: private */
    public LeDeviceListAdapter mLeDeviceListAdapter;
    /* access modifiers changed from: private */
    public BluetoothAdapter.LeScanCallback mLeScanCallback = new BluetoothAdapter.LeScanCallback() {
        public void onLeScan(final BluetoothDevice device, int rssi, byte[] scanRecord) {
            DeviceScanActivity.this.runOnUiThread(new Runnable() {
                public void run() {
                    try {
                        Log.v("TEST", "device::" + device.getName());
                        DeviceScanActivity.this.mLeDeviceListAdapter.addDevice(device);
                        String deviceId = new UUID((long) device.hashCode(), ((long) device.hashCode()) << 32).toString();
                        System.out.println("current device iddd::" + deviceId);
                        Log.v("TEST", "current device iddd::" + deviceId);
                        for (int i = 0; i < DeviceScanActivity.this.totaldevicessaved; i++) {
                            System.out.println("saved device iddd::" + ((String) DeviceScanActivity.this.deviceaddressssaved.get(i)));
                            View v = DeviceScanActivity.this.listView.getChildAt(i - DeviceScanActivity.this.listView.getFirstVisiblePosition());
                            TextView someText = (TextView) v.findViewById(R.id.device_connected);
                            if (deviceId.equalsIgnoreCase((String) DeviceScanActivity.this.deviceaddressssaved.get(i))) {
                                System.out.println("matching!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!111");
                                if (v != null) {
                                    someText.setText("");
                                } else {
                                    return;
                                }
                            } else {
                                System.out.println("not matching!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!111");
                            }
                        }
                        DeviceScanActivity.this.mLeDeviceListAdapter.notifyDataSetChanged();
                        DeviceScanActivity.this.chapterListAdapter.notifyDataSetChanged();
                    } catch (Exception e) {
                    }
                    if (DeviceScanActivity.this.mLeDeviceListAdapter.getCount() <= 0 || DeviceScanActivity.this.chapterListAdapter.getCount() == DeviceScanActivity.this.mLeDeviceListAdapter.getCount()) {
                        DeviceScanActivity.this.newdevies_txt.setVisibility(0);
                        DeviceScanActivity.this.listView1.setVisibility(8);
                    } else {
                        DeviceScanActivity.this.newdevies_txt.setVisibility(8);
                        DeviceScanActivity.this.listView1.setVisibility(0);
                    }
                    if (DeviceScanActivity.this.chapterListAdapter.getCount() > 0) {
                        DeviceScanActivity.this.mydevies_txt.setVisibility(8);
                        DeviceScanActivity.this.listView.setVisibility(0);
                        return;
                    }
                    DeviceScanActivity.this.mydevies_txt.setVisibility(0);
                    DeviceScanActivity.this.listView.setVisibility(8);
                }
            });
        }
    };
    /* access modifiers changed from: private */
    public boolean mScanning;
    /* access modifiers changed from: private */
    public TextView mydevies_txt;
    /* access modifiers changed from: private */
    public TextView newdevies_txt;
    private LinearLayout play_music_ll;
    private TextView privacy;
    /* access modifiers changed from: private */
    public ArrayList<String> saveddevicenames = new ArrayList<>();
    private TextView scan;
    private TextView skip;
    private TextView title;
    /* access modifiers changed from: private */
    public int totaldevicessaved = 0;
    private TextView version;

    static /* synthetic */ int access$1710(DeviceScanActivity x0) {
        int i = x0.countval;
        x0.countval = i - 1;
        return i;
    }

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.main);
        BottomBarInit();
        session = new SavedPreferences(getApplicationContext());
        this.newdevies_txt = (TextView) findViewById(R.id.newdevies_txt);
        this.mydevies_txt = (TextView) findViewById(R.id.mydevies_txt);
        this.listView = (ListView) findViewById(R.id.listvw);
        this.listView1 = (ListView) findViewById(R.id.listvw1);
        this.skip = (TextView) findViewById(R.id.skip);
        this.help = (ImageView) findViewById(R.id.help);
        this.add = (Button) findViewById(R.id.add);
        this.edit = (TextView) findViewById(R.id.edit);
        this.privacy = (TextView) findViewById(R.id.privacypolicy);
        this.privacy.setText(Html.fromHtml("<u>Privacy Policy</u>"));
        this.title = (TextView) findViewById(R.id.title);
        this.scan = (TextView) findViewById(R.id.scan);
        this.devices = (TextView) findViewById(R.id.devices);
        this.devices1 = (TextView) findViewById(R.id.devices1);
        Typeface tf = Typeface.createFromAsset(getAssets(), "fonts/gotham_medium.otf");
        this.gotham_light_tf = Typeface.createFromAsset(getAssets(), "fonts/gotham_light.otf");
        this.scan.setTypeface(tf);
        this.devices.setTypeface(tf);
        this.devices1.setTypeface(tf);
        this.privacy.setTypeface(tf);
        this.title.setTypeface(tf);
        this.skip.setTypeface(this.gotham_light_tf);
        this.mydevies_txt.setTypeface(tf);
        this.newdevies_txt.setTypeface(tf);
        this.edit.setTypeface(tf);
        SharedPreferences prefs2 = getApplicationContext().getSharedPreferences(SHARED_PREFERENCES_NAME, MODE_PRIVATE);
        double lat = (double) prefs2.getFloat("Latitude", 0.0f);
        double lon = (double) prefs2.getFloat("Longitude", 0.0f);
        if (lat == 0.0d && lon == 0.0d) {
            startActivityForResult(new Intent(this, RecordLocation.class), 3000);
            System.out.println("SAVING THE LOCATION!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!");
        }
        this.privacy.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                Intent i = new Intent(DeviceScanActivity.this, WebActivity.class);
                i.setFlags(DriveFile.MODE_READ_ONLY);
                i.setFlags(67108864);
                DeviceScanActivity.this.startActivity(i);
            }
        });
        this.help.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                Intent i = new Intent(DeviceScanActivity.this, HelpScreen.class);
                i.putExtra("activity", "0");
                DeviceScanActivity.this.startActivity(i);
            }
        });
        try {
            Settings.Secure.putString(getApplicationContext().getContentResolver(), "location_providers_allowed", String.format("%s,%s", new Object[]{Settings.Secure.getString(getApplicationContext().getContentResolver(), "location_providers_allowed"), "gps"}));
        } catch (Exception e) {
        }
        this.edit.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                if (DeviceScanActivity.this.edit.getText().toString().equalsIgnoreCase("edit")) {
                    DeviceScanActivity.this.edit.setText("DONE");
                    DeviceScanActivity.this.chapterListAdapter.hideIcons();
                    for (int i = 0; i < DeviceScanActivity.this.listView.getChildCount(); i++) {
                        DeviceScanActivity.this.listView.getChildAt(i).findViewById(R.id.delete).setVisibility(0);
                        DeviceScanActivity.this.listView.getChildAt(i).findViewById(R.id.device_connected).setVisibility(8);
                    }
                    return;
                }
                DeviceScanActivity.this.edit.setText("EDIT");
                for (int i2 = 0; i2 < DeviceScanActivity.this.listView.getChildCount(); i2++) {
                    DeviceScanActivity.this.listView.getChildAt(i2).findViewById(R.id.delete).setVisibility(8);
                    DeviceScanActivity.this.listView.getChildAt(i2).findViewById(R.id.device_connected).setVisibility(0);
                }
            }
        });
        this.UUIDS = new UUID[]{UUID.fromString("0000180f-0000-1000-8000-00805f9b34fb")};
        this.mLeDeviceListAdapter = new LeDeviceListAdapter();
        this.chapterListAdapter = new CodeLearnAdapter();
        String versionName = null;
        try {
            versionName = getPackageManager().getPackageInfo(getPackageName(), 0).versionName;
        } catch (PackageManager.NameNotFoundException e2) {
            e2.printStackTrace();
        }
        this.version = (TextView) findViewById(R.id.versionnumber);
        this.version.setText("v " + versionName);
        this.version.setTypeface(tf);
        this.add.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
            }
        });
        this.skip.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                int parseInt = Integer.parseInt(DeviceScanActivity.session.loadSavedPreferencescount("COUNT"));
                DeviceScanActivity.this.startActivity(new Intent(DeviceScanActivity.this, DummyActivity.class));
            }
        });
        this.listView1.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            public void onItemClick(AdapterView<?> adapterView, View arg1, int position, long arg3) {
                int pos;
                LinearLayout ll = (LinearLayout) DeviceScanActivity.this.listView1.getChildAt(position);
                Log.v("TAG", "text " + ((TextView) ll.getChildAt(1)).getText().toString());
                if (DeviceScanActivity.this.devicenamessaved.contains(((TextView) ll.getChildAt(1)).getText().toString())) {
                    pos = DeviceScanActivity.this.devicenamessaved.indexOf(((TextView) ll.getChildAt(1)).getText().toString());
                } else {
                    pos = position;
                }
                Log.v("TAG", "clicked = " + ((String) DeviceScanActivity.this.devicenamessaved.get(pos)));
                DeviceScanActivity.this.saveddevicenames.add(DeviceScanActivity.this.devicenamessaved.get(pos));
                String str = Build.VERSION.RELEASE;
                int i = Build.VERSION.SDK_INT;
                Intent intent = new Intent(DeviceScanActivity.this, DeviceControlActivity_Above.class);
                DeviceControlActivity_Above.EXTRAS_DEVICE_NAME = (String) DeviceScanActivity.this.devicenamessaved.get(pos);
                DeviceControlActivity_Above.EXTRAS_DEVICE_ADDRESS = (String) DeviceScanActivity.this.bluetoothaddresses.get(pos);
                intent.putExtra("Activity", "First");
                intent.putExtra(DeviceControlActivity_Above.EXTRAS_DEVICE_NAME, (String) DeviceScanActivity.this.devicenamessaved.get(pos));
                intent.putExtra(DeviceControlActivity_Above.EXTRAS_DEVICE_ADDRESS, (String) DeviceScanActivity.this.bluetoothaddresses.get(pos));
                DeviceScanActivity.deviceUUId = (String) DeviceScanActivity.this.deviceaddressssaved.get(pos);
                DeviceScanActivity.session.savePreferences("DEVICENAME_" + DeviceScanActivity.deviceUUId, (String) DeviceScanActivity.this.devicenamessaved.get(pos));
                int countval = Integer.parseInt(DeviceScanActivity.session.loadSavedPreferencescount("COUNT")) + 1;
                intent.putExtra("PAGE", countval + "");
                DeviceScanActivity.session.savePreferences("COUNT", countval + "");
                DeviceScanActivity.session.savePreferences("DEVICE_" + countval, DeviceScanActivity.deviceUUId);
                DeviceScanActivity.session.savePreferences("DEVICENAME_" + countval, (String) DeviceScanActivity.this.devicenamessaved.get(pos));
                DeviceScanActivity.session.savePreferences("DEVICEADDRESS_" + countval, (String) DeviceScanActivity.this.deviceaddressssaved.get(pos));
                DeviceScanActivity.session.savePreferences("DEVICEBLEADDRESS_" + countval, (String) DeviceScanActivity.this.bluetoothaddresses.get(pos));
                DeviceControlActivity_Above.current_page = countval;
                if (DeviceScanActivity.this.mScanning) {
                    DeviceScanActivity.this.mBluetoothAdapter.stopLeScan(DeviceScanActivity.this.mLeScanCallback);
                    boolean unused = DeviceScanActivity.this.mScanning = false;
                }
                intent.putExtra("Alert", true);
                DeviceScanActivity.this.startActivity(intent);
            }
        });
        this.listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            public void onItemClick(AdapterView<?> adapterView, View arg1, int position, long arg3) {
                try {
                    if (DeviceScanActivity.this.mScanning) {
                        DeviceScanActivity.this.mBluetoothAdapter.stopLeScan(DeviceScanActivity.this.mLeScanCallback);
                        boolean unused = DeviceScanActivity.this.mScanning = false;
                    }
                    if (((TextView) arg1.findViewById(R.id.device_connected)).getText().toString().trim().equalsIgnoreCase("")) {
                        String str = Build.VERSION.RELEASE;
                        int i = Build.VERSION.SDK_INT;
                        Log.v("TAG", "lollipop and above");
                        Intent intent = new Intent(DeviceScanActivity.this, DeviceControlActivity_Above.class);
                        DeviceControlActivity_Above.EXTRAS_DEVICE_NAME = (String) DeviceScanActivity.this.devicenamessaved.get(position);
                        DeviceControlActivity_Above.EXTRAS_DEVICE_ADDRESS = (String) DeviceScanActivity.this.bluetoothaddresses.get(position);
                        intent.putExtra("Activity", "First");
                        intent.putExtra(DeviceControlActivity_Above.EXTRAS_DEVICE_NAME, (String) DeviceScanActivity.this.devicenamessaved.get(position));
                        intent.putExtra(DeviceControlActivity_Above.EXTRAS_DEVICE_ADDRESS, (String) DeviceScanActivity.this.bluetoothaddresses.get(position));
                        intent.putExtra("PAGE", position + 1);
                        intent.putExtra("Alert", false);
                        DeviceScanActivity.deviceUUId = (String) DeviceScanActivity.this.deviceaddressssaved.get(position);
                        if (DeviceScanActivity.session.loadSavedPreferences("DEVICENAME_" + DeviceScanActivity.deviceUUId) != null) {
                            System.out.println("here in loading of sessionhlhlhhhhhhhh000b6666666666666666666666600000000---------------------!!!!!!!!!!!!!!!!!!!!1");
                        } else {
                            System.out.println("here in loading of sessio1111111111111343454n---------------------!!!!!!!!!!!!!!!!!!!!1");
                            DeviceScanActivity.session.savePreferences("DEVICENAME_" + DeviceScanActivity.deviceUUId, (String) DeviceScanActivity.this.devicenamessaved.get(position));
                            int countval = Integer.parseInt(DeviceScanActivity.session.loadSavedPreferencescount("COUNT")) + 1;
                            DeviceScanActivity.session.savePreferences("COUNT", countval + "");
                            DeviceScanActivity.session.savePreferences("DEVICE_" + countval, DeviceScanActivity.deviceUUId);
                            DeviceScanActivity.session.savePreferences("DEVICENAME_" + DeviceScanActivity.deviceUUId, (String) DeviceScanActivity.this.devicenamessaved.get(position));
                            DeviceScanActivity.session.savePreferences("DEVICEADDRESS_" + countval, (String) DeviceScanActivity.this.deviceaddressssaved.get(position));
                            DeviceScanActivity.session.savePreferences("DEVICEBLEADDRESS_" + countval, (String) DeviceScanActivity.this.bluetoothaddresses.get(position));
                            System.out.println("count vallll:::" + countval);
                        }
                        System.out.println("here.............................");
                        DeviceScanActivity.this.startActivity(intent);
                    } else {
                        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(DeviceScanActivity.this);
                        alertDialogBuilder.setTitle("Alert");
                        alertDialogBuilder.setMessage("TUMI Bank not available. Ensure Bluetooth is activated. Proceed to view Last Known Location without an active connection?").setCancelable(false).setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {
                                Intent i = new Intent(DeviceScanActivity.this, DummyActivity.class);
                                i.putExtra("fromscan", 1);
                                DeviceScanActivity.this.startActivity(i);
                            }
                        }).setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int which) {
                                dialog.dismiss();
                            }
                        });
                        try {
                            alertDialogBuilder.create().show();
                        } catch (Exception e) {
                        }
                    }
                    DeviceScanActivity.this.devicenamessaved.clear();
                    DeviceScanActivity.this.deviceaddressssaved.clear();
                    DeviceScanActivity.this.deviceavailable.clear();
                    DeviceScanActivity.this.bluetoothaddresses.clear();
                } catch (Exception e2) {
                }
            }
        });
        this.mHandler = new Handler();
        if (!getPackageManager().hasSystemFeature("android.hardware.bluetooth_le")) {
            Toast.makeText(this, R.string.ble_not_supported, 0).show();
            finish();
        }
        this.mBluetoothAdapter = ((BluetoothManager) getSystemService("bluetooth")).getAdapter();
        if (this.mBluetoothAdapter == null) {
            Toast.makeText(this, R.string.error_bluetooth_not_supported, 0).show();
            finish();
            return;
        }
        scanLeDevice(true);
    }

    /* access modifiers changed from: protected */
    public void onRestart() {
        super.onRestart();
        System.out.println("on restart..............!!!!!!!!!!!!!!!!!11");
        if (!this.mBluetoothAdapter.isEnabled() && !this.mBluetoothAdapter.isEnabled()) {
            startActivityForResult(new Intent("android.bluetooth.adapter.action.REQUEST_ENABLE"), 1);
        }
    }

    /* access modifiers changed from: protected */
    public void onResume() {
        super.onResume();
        System.out.println("on resumeeeeeeeeeeeeeeeeeeeee..............!!!!!!!!!!!!!!!!!11");
        if (!this.mBluetoothAdapter.isEnabled() && !this.mBluetoothAdapter.isEnabled()) {
            startActivityForResult(new Intent("android.bluetooth.adapter.action.REQUEST_ENABLE"), 1);
        }
        this.countval = Integer.parseInt(session.loadSavedPreferencescount("COUNT"));
        this.totaldevicessaved = this.countval;
        System.out.println("total devices saved.............______________________" + this.totaldevicessaved);
        this.deviceaddressssaved.clear();
        this.devicenamessaved.clear();
        this.bluetoothaddresses.clear();
        this.deviceavailable.clear();
        this.saveddevicenames.clear();
        for (int i = 0; i < this.totaldevicessaved; i++) {
            String uuid = session.loadSavedPreferences("DEVICE_" + (i + 1));
            System.out.println("device name______________" + session.loadSavedPreferences("DEVICENAME_" + uuid));
            System.out.println("device uuid______________" + session.loadSavedPreferences("DEVICE_" + (i + 1)));
            this.saveddevicenames.add(session.loadSavedPreferences("DEVICENAME_" + uuid));
            Log.v("TEST", "DEVICENAME_ = " + session.loadSavedPreferences("DEVICENAME_" + uuid));
            this.devicenamessaved.add(session.loadSavedPreferences("DEVICENAME_" + uuid));
            this.deviceaddressssaved.add(uuid);
            this.bluetoothaddresses.add(session.loadSavedPreferences("DEVICEBLEADDRESS_" + (i + 1)));
            this.deviceavailable.add("Not Avaialable");
        }
        this.edit.setText("EDIT");
        if (this.saveddevicenames.size() > 0) {
            this.edit.setVisibility(0);
        } else {
            this.edit.setVisibility(8);
        }
        Log.v("TAG", "devicenamessaved = " + this.devicenamessaved.size());
        Log.v("TAG", "saveddevicenames = " + this.saveddevicenames.size());
        this.chapterListAdapter = new CodeLearnAdapter();
        this.listView.setAdapter(this.chapterListAdapter);
        this.mLeDeviceListAdapter = new LeDeviceListAdapter();
        this.listView1.setAdapter(this.mLeDeviceListAdapter);
        scanLeDevice(true);
    }

    /* access modifiers changed from: protected */
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == 1 && resultCode == 0) {
            finish();
        } else {
            super.onActivityResult(requestCode, resultCode, data);
        }
    }

    /* access modifiers changed from: protected */
    public void onPause() {
        super.onPause();
        scanLeDevice(false);
        this.mLeDeviceListAdapter.clear();
        System.out.println("on pause@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@2");
        UpdateManager.unregister();
    }

    /* access modifiers changed from: private */
    public void scanLeDevice(boolean enable) {
        if (enable) {
            this.mHandler.postDelayed(new Runnable() {
                public void run() {
                    boolean unused = DeviceScanActivity.this.mScanning = false;
                    DeviceScanActivity.this.mBluetoothAdapter.stopLeScan(DeviceScanActivity.this.mLeScanCallback);
                }
            }, SCAN_PERIOD);
            this.mScanning = true;
            this.mBluetoothAdapter.startLeScan(this.mLeScanCallback);
            return;
        }
        this.mScanning = false;
        this.mBluetoothAdapter.stopLeScan(this.mLeScanCallback);
    }

    private class LeDeviceListAdapter extends BaseAdapter {
        private LayoutInflater mInflator;
        private ArrayList<BluetoothDevice> mLeDevices = new ArrayList<>();

        public LeDeviceListAdapter() {
            this.mInflator = DeviceScanActivity.this.getLayoutInflater();
        }

        public void addDevice(BluetoothDevice device) {
            Log.v("TEST", "device.getName() = " + device.getName());
            if (this.mLeDevices.contains(device)) {
                return;
            }
            if (device.getName().startsWith("offGRID") || device.getName().startsWith("TUMI")) {
                Log.v("TEST", "device.getName()ifffffffffffffffffff = " + device.getName());
                this.mLeDevices.add(device);
            }
        }

        public BluetoothDevice getDevice(int position) {
            return this.mLeDevices.get(position);
        }

        public void clear() {
            this.mLeDevices.clear();
        }

        public int getCount() {
            return this.mLeDevices.size();
        }

        public Object getItem(int i) {
            return this.mLeDevices.get(i);
        }

        public long getItemId(int i) {
            return (long) i;
        }

        public View getView(int i, View view, ViewGroup viewGroup) {
            ViewHolder viewHolder;
            if (view == null) {
                view = this.mInflator.inflate(R.layout.listitem_device, (ViewGroup) null);
                viewHolder = new ViewHolder();
                viewHolder.deviceAddress = (TextView) view.findViewById(R.id.device_address);
                viewHolder.deviceName = (TextView) view.findViewById(R.id.device_name);
                viewHolder.isconnected = (TextView) view.findViewById(R.id.device_connected);
                viewHolder.deviceName.setTypeface(DeviceScanActivity.this.gotham_light_tf);
                view.setTag(viewHolder);
            } else {
                viewHolder = (ViewHolder) view.getTag();
            }
            BluetoothDevice device = this.mLeDevices.get(i);
            String deviceName = device.getName();
            System.out.println("NOW!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!1" + deviceName);
            String deviceId = new UUID((long) device.hashCode(), ((long) device.hashCode()) << 32).toString();
            DeviceScanActivity.deviceUUId = deviceId;
            DeviceScanActivity.universaldevice = deviceName;
            viewHolder.isconnected.setText("Not in range");
            if (DeviceScanActivity.session.loadSavedPreferences("DEVICENAME_" + DeviceScanActivity.deviceUUId) != null) {
                viewHolder.deviceName.setVisibility(8);
                viewHolder.isconnected.setVisibility(8);
                System.out.println("saved here in loading of session---------------------" + DeviceScanActivity.session.loadSavedPreferences("DEVICENAME_" + DeviceScanActivity.deviceUUId));
                viewHolder.deviceName.setText(DeviceScanActivity.session.loadSavedPreferences("DEVICENAME_" + DeviceScanActivity.deviceUUId));
                viewHolder.isconnected.setText("");
            } else {
                System.out.println("Device names saved::" + DeviceScanActivity.this.devicenamessaved);
                viewHolder.deviceName.setVisibility(0);
                viewHolder.isconnected.setVisibility(8);
                viewHolder.deviceName.setText(device.getName());
                if (DeviceScanActivity.this.deviceaddressssaved.size() == 0) {
                    System.out.println("SIZE IS ZEROOOO");
                    DeviceScanActivity.this.devicenamessaved.add(device.getName());
                    DeviceScanActivity.this.deviceaddressssaved.add(deviceId);
                    DeviceScanActivity.this.bluetoothaddresses.add(device.getAddress());
                } else if (!DeviceScanActivity.this.deviceaddressssaved.contains(deviceId)) {
                    System.out.println("ADDING NON DUPLICATE");
                    DeviceScanActivity.this.devicenamessaved.add(device.getName());
                    DeviceScanActivity.this.deviceaddressssaved.add(deviceId);
                    DeviceScanActivity.this.bluetoothaddresses.add(device.getAddress());
                }
            }
            return view;
        }
    }

    static class ViewHolder {
        TextView deviceAddress;
        TextView deviceName;
        TextView isconnected;

        ViewHolder() {
        }
    }

    public class CodeLearnAdapter extends BaseAdapter {
        List<String> codeLearnChapterList = DeviceScanActivity.this.saveddevicenames;
        private boolean showIcons = false;

        public CodeLearnAdapter() {
        }

        public int getCount() {
            return this.codeLearnChapterList.size();
        }

        public String getItem(int arg0) {
            return this.codeLearnChapterList.get(arg0);
        }

        public long getItemId(int arg0) {
            return (long) arg0;
        }

        public void hideIcons() {
            this.showIcons = false;
            notifyDataSetChanged();
        }

        public View getView(final int arg0, View arg1, ViewGroup arg2) {
            if (arg1 == null) {
                arg1 = ((LayoutInflater) DeviceScanActivity.this.getSystemService("layout_inflater")).inflate(R.layout.listitem_device, arg2, false);
            }
            TextView chapterName = (TextView) arg1.findViewById(R.id.device_name);
            chapterName.setTypeface(DeviceScanActivity.this.gotham_light_tf);
            ((TextView) arg1.findViewById(R.id.device_connected)).setTypeface(DeviceScanActivity.this.gotham_light_tf);
            ((ImageView) arg1.findViewById(R.id.delete)).setOnClickListener(new View.OnClickListener() {
                public void onClick(View v) {
                    AlertDialog alertDialog = new AlertDialog.Builder(DeviceScanActivity.this).create();
                    alertDialog.setMessage("Remove device from memory? You will need to re-setup this device if deleted.");
                    alertDialog.setButton("OK", new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int which) {
                            String count = DeviceScanActivity.session.loadSavedPreferencescount("COUNT");
                            try {
                                DeviceScanActivity.session.delPreferences("DEVICENAME_" + (arg0 + 1), "DEVICEADDRESS_" + (arg0 + 1), "DEVICEBLEADDRESS_" + (arg0 + 1), "DEVICE_" + ((String) DeviceScanActivity.this.deviceaddressssaved.get(arg0)), "DEVICENAME_" + ((String) DeviceScanActivity.this.deviceaddressssaved.get(arg0)), "DEVICE_" + (arg0 + 1));
                                for (int i = arg0 + 1; i < DeviceScanActivity.this.countval; i++) {
                                    DeviceScanActivity.session.savePreferences("DEVICENAME_" + i, DeviceScanActivity.session.loadSavedPreferences("DEVICENAME_" + (i + 1)));
                                    DeviceScanActivity.session.savePreferences("DEVICEADDRESS_" + i, DeviceScanActivity.session.loadSavedPreferences("DEVICEADDRESS_" + (i + 1)));
                                    DeviceScanActivity.session.savePreferences("DEVICEBLEADDRESS_" + i, DeviceScanActivity.session.loadSavedPreferences("DEVICEBLEADDRESS_" + (i + 1)));
                                    DeviceScanActivity.session.savePreferences("DEVICE_" + i, DeviceScanActivity.session.loadSavedPreferences("DEVICEADDRESS_" + (i + 1)));
                                    DeviceScanActivity.session.savePreferences("DEVICENAME_" + DeviceScanActivity.session.loadSavedPreferences("DEVICEADDRESS_" + (i + 1)), DeviceScanActivity.session.loadSavedPreferences("DEVICENAME_" + (i + 1)));
                                    DeviceScanActivity.session.savePreferences("DEVICE_" + DeviceScanActivity.session.loadSavedPreferences("DEVICEADDRESS_" + (i + 1)), DeviceScanActivity.session.loadSavedPreferences("DEVICEADDRESS_" + (i + 1)));
                                }
                                int unused = DeviceScanActivity.this.countval = Integer.parseInt(count);
                                DeviceScanActivity.access$1710(DeviceScanActivity.this);
                                DeviceScanActivity.session.savePreferences("COUNT", DeviceScanActivity.this.countval + "");
                                DeviceScanActivity.this.devicenamessaved.remove(arg0);
                                DeviceScanActivity.this.deviceaddressssaved.remove(arg0);
                                DeviceScanActivity.this.deviceavailable.remove(arg0);
                                DeviceScanActivity.this.bluetoothaddresses.remove(arg0);
                                DeviceScanActivity.this.saveddevicenames.remove(arg0);
                                CodeLearnAdapter.this.notifyDataSetChanged();
                            } catch (Exception e) {
                            }
                            if (CodeLearnAdapter.this.codeLearnChapterList.size() > 0) {
                                DeviceScanActivity.this.mydevies_txt.setVisibility(8);
                                DeviceScanActivity.this.listView.setVisibility(0);
                                DeviceScanActivity.this.edit.setVisibility(0);
                            } else {
                                DeviceScanActivity.this.mydevies_txt.setVisibility(0);
                                DeviceScanActivity.this.listView.setVisibility(8);
                                DeviceScanActivity.this.edit.setVisibility(8);
                            }
                            DeviceScanActivity.this.mLeDeviceListAdapter.clear();
                            DeviceScanActivity.this.scanLeDevice(true);
                        }
                    });
                    alertDialog.setButton2("Cancel", new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int which) {
                            dialog.dismiss();
                        }
                    });
                    alertDialog.show();
                }
            });
            chapterName.setText(this.codeLearnChapterList.get(arg0));
            return arg1;
        }
    }

    private void checkForCrashes() {
        CrashManager.register(this, "397c7c49d106471011411933e0a45afe");
    }

    private void checkForUpdates() {
        UpdateManager.register(this, "397c7c49d106471011411933e0a45afe");
    }

    /* access modifiers changed from: package-private */
    public void offersDialog() {
        this.dialog = new Dialog(this, 16973834);
        this.dialog.requestWindowFeature(1);
        this.dialog.setContentView(R.layout.offers);
        this.dialog.setCancelable(false);
        final WebView mWebView = (WebView) this.dialog.findViewById(R.id.webView);
        mWebView.getSettings().setJavaScriptEnabled(true);
        mWebView.setInitialScale(100);
        mWebView.getSettings().setBuiltInZoomControls(true);
        mWebView.setWebViewClient(new AppWebViewClients((ProgressBar) this.dialog.findViewById(R.id.progress_bar)));
        mWebView.loadUrl("http://dev.ncpo.cc/apps/tumi/tumiadpage.html ");
        ((Button) this.dialog.findViewById(R.id.button_back)).setOnClickListener(new View.OnClickListener() {
            public void onClick(View arg0) {
                mWebView.goBack();
            }
        });
        ((Button) this.dialog.findViewById(R.id.button_farward)).setOnClickListener(new View.OnClickListener() {
            public void onClick(View arg0) {
                mWebView.goForward();
            }
        });
        ((ImageView) this.dialog.findViewById(R.id.close)).setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                DeviceScanActivity.this.offersAlert();
            }
        });
        this.dialog.show();
    }

    /* access modifiers changed from: package-private */
    public void offersAlert() {
        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(this);
        alertDialogBuilder.setTitle("Alert");
        alertDialogBuilder.setMessage("Stop seeing special offers and sales?").setCancelable(false).setPositiveButton("Cancel", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog1, int id) {
                dialog1.dismiss();
                DeviceScanActivity.this.dialog.dismiss();
            }
        }).setNegativeButton("Ok", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog1, int which) {
                DeviceScanActivity.session.stopOffers("Offers", true);
                dialog1.dismiss();
                DeviceScanActivity.this.dialog.dismiss();
            }
        });
        try {
            alertDialogBuilder.create().show();
        } catch (Exception e) {
        }
    }

    public class AppWebViewClients extends WebViewClient {
        private ProgressBar progress_bar;

        public AppWebViewClients(ProgressBar progress_bar2) {
            this.progress_bar = progress_bar2;
        }

        public void onPageStarted(WebView view, String url, Bitmap favicon) {
            Log.v("TEST", "onPageStarted");
            this.progress_bar.setVisibility(0);
        }

        public void onLoadResource(WebView view, String url) {
            super.onLoadResource(view, url);
        }

        public void onPageFinished(WebView view, String url) {
            Log.v("TEST", "onPageFinished");
            this.progress_bar.setVisibility(8);
        }
    }

    private void BottomBarInit() {
        this.play_music_ll = (LinearLayout) findViewById(R.id.play_music_ll);
        this.ambient_sounds_ll = (LinearLayout) findViewById(R.id.ambient_sounds_ll);
        this.play_music_ll.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                DeviceScanActivity.this.startActivity(new Intent(DeviceScanActivity.this, SelectDeviceActivity.class));
            }
        });
        this.ambient_sounds_ll.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                DeviceScanActivity.this.startActivity(new Intent(DeviceScanActivity.this, AmbientSoundsActivity.class));
            }
        });
    }
}
