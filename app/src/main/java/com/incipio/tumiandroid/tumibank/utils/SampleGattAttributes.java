package com.incipio.tumiandroid.tumibank.utils;

import java.util.HashMap;

public class SampleGattAttributes {
    public static String BATTERY_SERVICE = "00001c00-d102-11e1-9b23-000efb0000a5";
    public static String CLIENT_CHARACTERISTIC_CONFIG = "00002902-0000-1000-8000-00805f9b34fb";
    public static String CLIENT_CHARACTERISTIC_CONFIG_2 = "00002a85-0000-1000-8000-00805f9b34fb";
    public static String CLIENT_CHARACTERISTIC_CONFIG_BATTERY = "00002a19-0000-1000-8000-00805f9b34fb";
    public static String EXTRA_CHARACTERISITC = "00001C0F-D102-11E1-9B23-000EFB0000B6";
    public static String FINDME_CHARACTERSITIC = "0000ffe3-0000-1000-8000-00805f9b34fb";
    public static String HEART_RATE_MEASUREMENT = "00002a37-0000-1000-8000-00805f9b34fb";
    public static String LEUCP_SERVICE = "82c797f1-62e5-6614-ab1b-7b60c03ea8b3";
    public static String NOTIFY_CHARACTERISTIC = "00001C0F-D102-11E1-9B23-000EFB0000A5";
    public static String OffGRID_CHARACTERISITC = "00001C01-D102-11E1-9B23-000EFB0000A5";
    public static String PORT1_CHARACTERSITIC = "0000ffe4-0000-1000-8000-00805f9b34fb";
    public static String PORT2_CHARACTERSITIC = "0000ffe5-0000-1000-8000-00805f9b34fb";
    public static String PORT_NOTIFY_CHARACTERISTIC = "00001C0F-D102-11E1-9B23-000EFB0000B6";
    public static String SIMPLE_KEYS_SERVICE = "0000ffe0-0000-1000-8000-00805f9b34fb";
    private static HashMap<String, String> attributes = new HashMap<>();

    static {
        attributes.put(BATTERY_SERVICE, "Battery Service");
        attributes.put("00002a19-0000-1000-8000-00805f9b34fb", CLIENT_CHARACTERISTIC_CONFIG_BATTERY);
        attributes.put(SIMPLE_KEYS_SERVICE, "Simple Keys Service");
    }

    public static String lookup(String uuid, String defaultName) {
        String name = attributes.get(uuid);
        return name == null ? defaultName : name;
    }
}
