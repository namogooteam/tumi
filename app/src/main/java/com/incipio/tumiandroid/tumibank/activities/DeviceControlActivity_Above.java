package com.incipio.tumiandroid.tumibank.activities;

import android.animation.Animator;
import android.animation.ObjectAnimator;
import android.animation.ValueAnimator;
import android.app.AlertDialog;
import android.app.Dialog;
import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.ProgressDialog;
import android.bluetooth.BluetoothGattCharacteristic;
import android.bluetooth.BluetoothGattService;
import android.content.BroadcastReceiver;
import android.content.ComponentName;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.IntentSender;
import android.content.ServiceConnection;
import android.content.SharedPreferences;
import android.content.pm.ApplicationInfo;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.graphics.Typeface;
import android.graphics.drawable.AnimationDrawable;
import android.graphics.drawable.ColorDrawable;
import android.location.Location;
import android.location.LocationManager;
import android.net.http.Headers;
import android.os.Bundle;
import android.os.Handler;
import android.os.IBinder;
import android.text.Html;
import android.text.InputFilter;
import android.text.Spanned;
import android.text.format.Time;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewTreeObserver;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.view.animation.RotateAnimation;
import android.view.animation.TranslateAnimation;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.ToggleButton;

import androidx.core.app.ActivityCompat;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentActivity;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentPagerAdapter;
import androidx.viewpager.widget.ViewPager;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GooglePlayServicesUtil;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.drive.DriveFile;
import com.google.android.gms.location.LocationListener;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.common.base.Ascii;
import com.incipio.tumiandroid.BuildConfig;
import com.incipio.tumiandroid.R;
import com.incipio.tumiandroid.activities.WebPageActivity;
import com.incipio.tumiandroid.adapters.MenuListViewAdapter;
import com.incipio.tumiandroid.tumibank.utils.AccelerometerManager;
import com.incipio.tumiandroid.tumibank.utils.SampleGattAttributes;
import com.incipio.tumiandroid.tumibank.utils.SavedPreferences;
import com.incipio.tumiandroid.utils.AnimParams;
import de.passsy.holocircularprogressbar.HoloCircularProgressBar;
import java.io.File;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
//import kaaes.spotify.webapi.samplesearch.AddAlbumFragment1;

public class DeviceControlActivity_Above extends FragmentActivity implements LocationListener, GoogleApiClient.ConnectionCallbacks, GoogleApiClient.OnConnectionFailedListener, Animation.AnimationListener {
    private static final int CONNECTION_FAILURE_RESOLUTION_REQUEST = 9000;
    static String EXTRAS_DEVICE_ADDRESS = "DEVICE_ADDRESS";
    static String EXTRAS_DEVICE_NAME = "DEVICE_NAME";
    public static final String LIST_NAME = "NAME";
    public static final String LIST_UUID = "UUID";
    public static String SHARED_PREFERENCES_NAME = null;
    private static final String TAG = DeviceControlActivity_Above.class.getSimpleName();
    public static AlertDialog alertDialog;
    public static AlertDialog alertDialog1;
    private static AnimParams animParams = new AnimParams();
    public static ImageView anim_timer;
    public static AnimationDrawable animation;
    public static AnimationDrawable animation_green;
    public static ImageView arrow_img;
    public static LinearLayout back;
    public static LinearLayout backLayout_operations;
    public static TextView battery;
    public static ImageView batterylevel_iv;
    public static String batteryvaluet = "0";
    public static RelativeLayout bottom_ll;
    public static String chargeColor = "";
    public static TextView charge_txt;
    public static String chargedata = "";
    public static String charging_status = "0";
    public static TextView connect;
    public static boolean connectBool = false;
    public static ViewGroup content;
    public static Context context;
    static int current_page = 1;
    public static TextView devname;
    public static LinearLayout find_ll;
    public static LinearLayout find_me_ll;
    public static TextView findme;
    public static SupportMapFragment fragment;
    public static int from_settings = 0;
    public static FrameLayout frontLayout_operations;
    public static int hascharac = 0;
    public static TextView header;
    public static ImageView header_img;
    public static LinearLayout header_img_ll;
    public static ImageView help;
    public static String hours = "00";
    static TextView hrs;
    public static TextView imhr;
    public static TextView immin;
    public static LinearLayout info;
    public static String isCharge = "0";
    /* access modifiers changed from: private */
    public static boolean isInbackground = false;
    public static boolean isMenuSelected = false;
    public static boolean isOpen = false;
    public static boolean isShowing = false;
    public static boolean is_showing = false;
    public static int isdisconnected = 0;
    public static String lowbyte;
    public static BluetoothLeService mBluetoothLeService;
    static String mDeviceAddress;
    static String mDeviceName;
    public static Map<Integer, Fragment> mFragmentTags;
    public static ArrayList<ArrayList<BluetoothGattCharacteristic>> mGattCharacteristics = new ArrayList<>();
    public static GoogleApiClient mGoogleApiClient;
    public static HoloCircularProgressBar mHoloCircularProgressBar;
    public static LocationRequest mLocationRequest;
    public static GoogleMap mMap;
    public static BluetoothGattCharacteristic mNotifyCharacteristic;
    public static ObjectAnimator mProgressBarAnimator;
    public static ViewPager mViewPager;
    public static LinearLayout mask_ll;
    public static LinearLayout menu;
    public static ListView menuListView;
    /* access modifiers changed from: private */
    public static boolean menuOut = false;
    static TextView min;
    public static String minutes = "00";
    public static ToggleButton port1;
    public static boolean port1checked = false;
    public static ToggleButton port2;
    public static boolean port2checked = false;
    public static int portval = 0;
    public static TextView privacy;
    public static ProgressDialog progress;
    public static int screenHeight = 0;
    public static int selectedPositionFromMenu;
    public static TextView settings;
    public static ImageView settings_img;
    public static TextView version;
    /* access modifiers changed from: private */
    public boolean isPause = false;
    private double lat;
    private double lng;
    private Location location;
    private final BroadcastReceiver mGattUpdateReceiver = new BroadcastReceiver() {
        /* JADX WARNING: Code restructure failed: missing block: B:51:0x01ee, code lost:
            com.incipio.tumiandroid.tumibank.activities.DeviceControlActivity_Above.portval = 0;
         */
        /* JADX WARNING: Failed to process nested try/catch */
        /* Code decompiled incorrectly, please refer to instructions dump. */
        public void onReceive(Context r15, Intent r16) {
            /*
                r14 = this;
                java.lang.String r0 = r16.getAction()
                java.lang.String r9 = "com.example.bluetooth.le.ACTION_GATT_CONNECTED"
                boolean r9 = r9.equals(r0)
                if (r9 == 0) goto L_0x0045
                r9 = 1
                com.incipio.tumiandroid.tumibank.activities.DeviceControlActivity_Above.connectBool = r9
                java.lang.String r9 = ""
                com.incipio.tumiandroid.tumibank.activities.DeviceControlActivity_Above.chargedata = r9
                java.lang.String r9 = "TAG"
                java.lang.String r10 = "connected_dialog-------------------------------------"
                android.util.Log.v(r9, r10)
                com.incipio.tumiandroid.tumibank.activities.DeviceControlActivity_Above r9 = com.incipio.tumiandroid.tumibank.activities.DeviceControlActivity_Above.this
                r10 = 2131165277(0x7f07005d, float:1.7944767E38)
                r9.updateConnectionState(r10)
                com.incipio.tumiandroid.tumibank.activities.DeviceControlActivity_Above r9 = com.incipio.tumiandroid.tumibank.activities.DeviceControlActivity_Above.this
                r9.connected_dialog()
                android.widget.TextView r9 = com.incipio.tumiandroid.tumibank.activities.DeviceControlActivity_Above.imhr
                r10 = 0
                r9.setVisibility(r10)
                android.widget.TextView r9 = com.incipio.tumiandroid.tumibank.activities.DeviceControlActivity_Above.immin
                r10 = 0
                r9.setVisibility(r10)
                android.widget.ImageView r9 = com.incipio.tumiandroid.tumibank.activities.DeviceControlActivity_Above.anim_timer
                r10 = 0
                r9.setVisibility(r10)
                android.widget.TextView r9 = com.incipio.tumiandroid.tumibank.activities.DeviceControlActivity_Above.charge_txt
                r10 = 0
                r9.setVisibility(r10)
                com.incipio.tumiandroid.tumibank.activities.DeviceControlActivity_Above r9 = com.incipio.tumiandroid.tumibank.activities.DeviceControlActivity_Above.this
                r9.invalidateOptionsMenu()
            L_0x0044:
                return
            L_0x0045:
                java.lang.String r9 = "com.example.bluetooth.le.ACTION_GATT_DISCONNECTED"
                boolean r9 = r9.equals(r0)
                if (r9 == 0) goto L_0x0135
                java.lang.String r9 = "TAG"
                java.lang.StringBuilder r10 = new java.lang.StringBuilder
                r10.<init>()
                java.lang.String r11 = "Disconnected------------------------------------- "
                java.lang.StringBuilder r10 = r10.append(r11)
                boolean r11 = com.incipio.tumiandroid.tumibank.activities.DeviceControlActivity_Above.connectBool
                java.lang.StringBuilder r10 = r10.append(r11)
                java.lang.String r10 = r10.toString()
                android.util.Log.v(r9, r10)
                r9 = 1
                com.incipio.tumiandroid.tumibank.activities.DeviceControlActivity_Above.isdisconnected = r9
                com.incipio.tumiandroid.tumibank.activities.DeviceControlActivity_Above r9 = com.incipio.tumiandroid.tumibank.activities.DeviceControlActivity_Above.this
                r10 = 2131165291(0x7f07006b, float:1.7944795E38)
                r9.updateConnectionState(r10)
                com.incipio.tumiandroid.tumibank.activities.DeviceControlActivity_Above r9 = com.incipio.tumiandroid.tumibank.activities.DeviceControlActivity_Above.this
                r9.invalidateOptionsMenu()
                boolean r9 = com.incipio.tumiandroid.tumibank.activities.DeviceControlActivity_Above.isInbackground     // Catch:{ Exception -> 0x02f8 }
                if (r9 != 0) goto L_0x00d5
                java.lang.String r9 = "TAG"
                java.lang.StringBuilder r10 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x02f8 }
                r10.<init>()     // Catch:{ Exception -> 0x02f8 }
                java.lang.String r11 = "isInbackground 0 = "
                java.lang.StringBuilder r10 = r10.append(r11)     // Catch:{ Exception -> 0x02f8 }
                boolean r11 = com.incipio.tumiandroid.tumibank.activities.DeviceControlActivity_Above.isInbackground     // Catch:{ Exception -> 0x02f8 }
                java.lang.StringBuilder r10 = r10.append(r11)     // Catch:{ Exception -> 0x02f8 }
                java.lang.String r10 = r10.toString()     // Catch:{ Exception -> 0x02f8 }
                android.util.Log.v(r9, r10)     // Catch:{ Exception -> 0x02f8 }
                java.lang.String r9 = "0"
                com.incipio.tumiandroid.tumibank.activities.DeviceControlActivity_Above.batteryvaluet = r9     // Catch:{ Exception -> 0x02f8 }
                java.lang.String r9 = "Not Connected"
                com.incipio.tumiandroid.tumibank.activities.DeviceControlActivity_Above.minutes = r9     // Catch:{ Exception -> 0x02f8 }
                java.lang.String r9 = ""
                com.incipio.tumiandroid.tumibank.activities.DeviceControlActivity_Above.hours = r9     // Catch:{ Exception -> 0x02f8 }
                java.lang.String r9 = ""
                com.incipio.tumiandroid.tumibank.activities.DeviceControlActivity_Above.charging_status = r9     // Catch:{ Exception -> 0x02f8 }
                com.incipio.tumiandroid.tumibank.activities.DeviceControlActivity_Above r9 = com.incipio.tumiandroid.tumibank.activities.DeviceControlActivity_Above.this     // Catch:{ Exception -> 0x02f8 }
                java.lang.String r10 = "0"
                java.lang.String r11 = "Not Connected"
                java.lang.String r12 = ""
                java.lang.String r13 = ""
                r9.displayData(r10, r11, r12, r13)     // Catch:{ Exception -> 0x02f8 }
                android.widget.TextView r9 = com.incipio.tumiandroid.tumibank.activities.DeviceControlActivity_Above.battery     // Catch:{ Exception -> 0x02f8 }
                java.lang.String r10 = "-"
                r9.setText(r10)     // Catch:{ Exception -> 0x02f8 }
                android.widget.TextView r9 = com.incipio.tumiandroid.tumibank.activities.DeviceControlActivity_Above.imhr     // Catch:{ Exception -> 0x02f8 }
                r10 = 4
                r9.setVisibility(r10)     // Catch:{ Exception -> 0x02f8 }
                android.widget.TextView r9 = com.incipio.tumiandroid.tumibank.activities.DeviceControlActivity_Above.immin     // Catch:{ Exception -> 0x02f8 }
                r10 = 4
                r9.setVisibility(r10)     // Catch:{ Exception -> 0x02f8 }
                android.widget.ImageView r9 = com.incipio.tumiandroid.tumibank.activities.DeviceControlActivity_Above.anim_timer     // Catch:{ Exception -> 0x02f8 }
                r10 = 4
                r9.setVisibility(r10)     // Catch:{ Exception -> 0x02f8 }
                android.widget.TextView r9 = com.incipio.tumiandroid.tumibank.activities.DeviceControlActivity_Above.charge_txt     // Catch:{ Exception -> 0x02f8 }
                r10 = 4
                r9.setVisibility(r10)     // Catch:{ Exception -> 0x02f8 }
            L_0x00d5:
                boolean r9 = com.incipio.tumiandroid.tumibank.activities.DeviceControlActivity_Above.connectBool
                r10 = 1
                if (r9 != r10) goto L_0x0044
                r9 = 0
                com.incipio.tumiandroid.tumibank.activities.DeviceControlActivity_Above.connectBool = r9
                com.incipio.tumiandroid.tumibank.utils.SavedPreferences r9 = com.incipio.tumiandroid.tumibank.activities.DeviceScanActivity.session
                java.lang.String r10 = "Notifications"
                boolean r9 = r9.getBLENotifications(r10)
                if (r9 == 0) goto L_0x010c
                java.lang.String r9 = "INFO"
                java.lang.StringBuilder r10 = new java.lang.StringBuilder
                r10.<init>()
                java.lang.String r11 = "disconnected1"
                java.lang.StringBuilder r10 = r10.append(r11)
                com.incipio.tumiandroid.tumibank.utils.SavedPreferences r11 = com.incipio.tumiandroid.tumibank.activities.DeviceScanActivity.session
                java.lang.String r12 = "Notifications"
                boolean r11 = r11.getBLENotifications(r12)
                java.lang.StringBuilder r10 = r10.append(r11)
                java.lang.String r10 = r10.toString()
                android.util.Log.v(r9, r10)
                com.incipio.tumiandroid.tumibank.activities.DeviceControlActivity_Above r9 = com.incipio.tumiandroid.tumibank.activities.DeviceControlActivity_Above.this
                r9.create_alert()
            L_0x010c:
                com.incipio.tumiandroid.tumibank.activities.DeviceControlActivity_Above r9 = com.incipio.tumiandroid.tumibank.activities.DeviceControlActivity_Above.this
                boolean r9 = r9.isPause
                if (r9 != 0) goto L_0x012b
                android.content.Intent r6 = new android.content.Intent
                com.incipio.tumiandroid.tumibank.activities.DeviceControlActivity_Above r9 = com.incipio.tumiandroid.tumibank.activities.DeviceControlActivity_Above.this
                java.lang.Class<com.incipio.tumiandroid.tumibank.activities.RecordLocation> r10 = com.incipio.tumiandroid.tumibank.activities.RecordLocation.class
                r6.<init>(r9, r10)
                com.incipio.tumiandroid.tumibank.activities.DeviceControlActivity_Above r9 = com.incipio.tumiandroid.tumibank.activities.DeviceControlActivity_Above.this
                r10 = 2000(0x7d0, float:2.803E-42)
                r9.startActivityForResult(r6, r10)
                java.lang.String r9 = "INFO"
                java.lang.String r10 = "RecordLocation"
                android.util.Log.v(r9, r10)
            L_0x012b:
                android.app.ProgressDialog r9 = com.incipio.tumiandroid.tumibank.activities.DeviceControlActivity_Above.progress     // Catch:{ Exception -> 0x0132 }
                r9.cancel()     // Catch:{ Exception -> 0x0132 }
                goto L_0x0044
            L_0x0132:
                r9 = move-exception
                goto L_0x0044
            L_0x0135:
                java.lang.String r9 = "com.example.bluetooth.le.ACTION_GATT_SERVICES_DISCOVERED"
                boolean r9 = r9.equals(r0)
                if (r9 == 0) goto L_0x014d
                com.incipio.tumiandroid.tumibank.activities.DeviceControlActivity_Above r9 = com.incipio.tumiandroid.tumibank.activities.DeviceControlActivity_Above.this     // Catch:{ Exception -> 0x014a }
                com.incipio.tumiandroid.tumibank.activities.BluetoothLeService r10 = com.incipio.tumiandroid.tumibank.activities.DeviceControlActivity_Above.mBluetoothLeService     // Catch:{ Exception -> 0x014a }
                java.util.List r10 = r10.getSupportedGattServices()     // Catch:{ Exception -> 0x014a }
                r9.check_forservices(r10)     // Catch:{ Exception -> 0x014a }
                goto L_0x0044
            L_0x014a:
                r9 = move-exception
                goto L_0x0044
            L_0x014d:
                java.lang.String r9 = "com.example.bluetooth.le.ACTION_DATA_AVAILABLE"
                boolean r9 = r9.equals(r0)
                if (r9 == 0) goto L_0x0044
                android.support.v4.view.ViewPager r9 = com.incipio.tumiandroid.tumibank.activities.DeviceControlActivity_Above.mViewPager
                int r7 = r9.getCurrentItem()
                android.support.v4.view.ViewPager r9 = com.incipio.tumiandroid.tumibank.activities.DeviceControlActivity_Above.mViewPager
                android.support.v4.view.PagerAdapter r1 = r9.getAdapter()
                com.incipio.tumiandroid.tumibank.activities.DeviceControlActivity_Above$SectionsPagerAdapter r1 = (com.incipio.tumiandroid.tumibank.activities.DeviceControlActivity_Above.SectionsPagerAdapter) r1
                r9 = 0
                android.support.v4.app.Fragment r5 = r1.getFragment(r9)
                com.incipio.tumiandroid.tumibank.activities.DeviceControlActivity_Above$DummySectionFragment r5 = (com.incipio.tumiandroid.tumibank.activities.DeviceControlActivity_Above.DummySectionFragment) r5
                android.view.View r9 = r5.getView()     // Catch:{ Exception -> 0x02f5 }
                r10 = 2131558588(0x7f0d00bc, float:1.8742496E38)
                android.view.View r9 = r9.findViewById(r10)     // Catch:{ Exception -> 0x02f5 }
                android.widget.ToggleButton r9 = (android.widget.ToggleButton) r9     // Catch:{ Exception -> 0x02f5 }
                com.incipio.tumiandroid.tumibank.activities.DeviceControlActivity_Above.port1 = r9     // Catch:{ Exception -> 0x02f5 }
                android.view.View r9 = r5.getView()     // Catch:{ Exception -> 0x02f5 }
                r10 = 2131558594(0x7f0d00c2, float:1.8742508E38)
                android.view.View r9 = r9.findViewById(r10)     // Catch:{ Exception -> 0x02f5 }
                android.widget.ToggleButton r9 = (android.widget.ToggleButton) r9     // Catch:{ Exception -> 0x02f5 }
                com.incipio.tumiandroid.tumibank.activities.DeviceControlActivity_Above.port2 = r9     // Catch:{ Exception -> 0x02f5 }
            L_0x0188:
                r8 = 0
                android.os.Bundle r9 = r16.getExtras()     // Catch:{ Exception -> 0x01ea }
                java.lang.String r10 = "com.example.bluetooth.le.EXTRA_PORTDATA"
                java.lang.String r9 = r9.getString(r10)     // Catch:{ Exception -> 0x01ea }
                int r8 = java.lang.Integer.parseInt(r9)     // Catch:{ Exception -> 0x01ea }
                java.lang.String r9 = "INFO"
                java.lang.String r10 = "mGattUpdateReceiver = "
                android.util.Log.v(r9, r10)     // Catch:{ Exception -> 0x01ea }
                java.lang.String r9 = "INFO"
                java.lang.StringBuilder r10 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x01ea }
                r10.<init>()     // Catch:{ Exception -> 0x01ea }
                java.lang.String r11 = "portdata = "
                java.lang.StringBuilder r10 = r10.append(r11)     // Catch:{ Exception -> 0x01ea }
                java.lang.StringBuilder r10 = r10.append(r8)     // Catch:{ Exception -> 0x01ea }
                java.lang.String r10 = r10.toString()     // Catch:{ Exception -> 0x01ea }
                android.util.Log.v(r9, r10)     // Catch:{ Exception -> 0x01ea }
                r9 = 21
                if (r8 == r9) goto L_0x01c6
                r9 = 23
                if (r8 == r9) goto L_0x01c6
                r9 = 24
                if (r8 == r9) goto L_0x01c6
                r9 = 26
                if (r8 != r9) goto L_0x022e
            L_0x01c6:
                android.os.Bundle r9 = r16.getExtras()     // Catch:{ Exception -> 0x01ed }
                java.lang.String r10 = "com.example.bluetooth.le.EXTRA_PORTDATA"
                java.lang.String r9 = r9.getString(r10)     // Catch:{ Exception -> 0x01ed }
                int r9 = java.lang.Integer.parseInt(r9)     // Catch:{ Exception -> 0x01ed }
                com.incipio.tumiandroid.tumibank.activities.DeviceControlActivity_Above.portval = r9     // Catch:{ Exception -> 0x01ed }
            L_0x01d6:
                int r9 = com.incipio.tumiandroid.tumibank.activities.DeviceControlActivity_Above.portval     // Catch:{ Exception -> 0x01ea }
                r10 = 21
                if (r9 != r10) goto L_0x01f2
                android.widget.ToggleButton r9 = com.incipio.tumiandroid.tumibank.activities.DeviceControlActivity_Above.port1     // Catch:{ Exception -> 0x01ea }
                r10 = 0
                r9.setChecked(r10)     // Catch:{ Exception -> 0x01ea }
                android.widget.ToggleButton r9 = com.incipio.tumiandroid.tumibank.activities.DeviceControlActivity_Above.port2     // Catch:{ Exception -> 0x01ea }
                r10 = 0
                r9.setChecked(r10)     // Catch:{ Exception -> 0x01ea }
                goto L_0x0044
            L_0x01ea:
                r9 = move-exception
                goto L_0x0044
            L_0x01ed:
                r4 = move-exception
                r9 = 0
                com.incipio.tumiandroid.tumibank.activities.DeviceControlActivity_Above.portval = r9     // Catch:{ Exception -> 0x01ea }
                goto L_0x01d6
            L_0x01f2:
                int r9 = com.incipio.tumiandroid.tumibank.activities.DeviceControlActivity_Above.portval     // Catch:{ Exception -> 0x01ea }
                r10 = 26
                if (r9 != r10) goto L_0x0206
                android.widget.ToggleButton r9 = com.incipio.tumiandroid.tumibank.activities.DeviceControlActivity_Above.port1     // Catch:{ Exception -> 0x01ea }
                r10 = 1
                r9.setChecked(r10)     // Catch:{ Exception -> 0x01ea }
                android.widget.ToggleButton r9 = com.incipio.tumiandroid.tumibank.activities.DeviceControlActivity_Above.port2     // Catch:{ Exception -> 0x01ea }
                r10 = 0
                r9.setChecked(r10)     // Catch:{ Exception -> 0x01ea }
                goto L_0x0044
            L_0x0206:
                int r9 = com.incipio.tumiandroid.tumibank.activities.DeviceControlActivity_Above.portval     // Catch:{ Exception -> 0x01ea }
                r10 = 24
                if (r9 != r10) goto L_0x021a
                android.widget.ToggleButton r9 = com.incipio.tumiandroid.tumibank.activities.DeviceControlActivity_Above.port1     // Catch:{ Exception -> 0x01ea }
                r10 = 0
                r9.setChecked(r10)     // Catch:{ Exception -> 0x01ea }
                android.widget.ToggleButton r9 = com.incipio.tumiandroid.tumibank.activities.DeviceControlActivity_Above.port2     // Catch:{ Exception -> 0x01ea }
                r10 = 1
                r9.setChecked(r10)     // Catch:{ Exception -> 0x01ea }
                goto L_0x0044
            L_0x021a:
                int r9 = com.incipio.tumiandroid.tumibank.activities.DeviceControlActivity_Above.portval     // Catch:{ Exception -> 0x01ea }
                r10 = 23
                if (r9 != r10) goto L_0x0044
                android.widget.ToggleButton r9 = com.incipio.tumiandroid.tumibank.activities.DeviceControlActivity_Above.port1     // Catch:{ Exception -> 0x01ea }
                r10 = 1
                r9.setChecked(r10)     // Catch:{ Exception -> 0x01ea }
                android.widget.ToggleButton r9 = com.incipio.tumiandroid.tumibank.activities.DeviceControlActivity_Above.port2     // Catch:{ Exception -> 0x01ea }
                r10 = 1
                r9.setChecked(r10)     // Catch:{ Exception -> 0x01ea }
                goto L_0x0044
            L_0x022e:
                r9 = 100
                if (r8 != r9) goto L_0x0044
                android.os.Bundle r2 = r16.getExtras()     // Catch:{ Exception -> 0x01ea }
                java.lang.String r9 = "com.example.bluetooth.le.EXTRA_DATA"
                java.lang.String r3 = r2.getString(r9)     // Catch:{ Exception -> 0x01ea }
                java.lang.String r9 = "com.example.bluetooth.le.EXTRA_MIN"
                java.lang.String r9 = r2.getString(r9)     // Catch:{ Exception -> 0x02f3 }
                java.lang.String r10 = "null"
                boolean r9 = r9.equals(r10)     // Catch:{ Exception -> 0x02f3 }
                if (r9 != 0) goto L_0x0284
                java.lang.String r9 = "com.example.bluetooth.le.EXTRA_MIN"
                java.lang.String r9 = r2.getString(r9)     // Catch:{ Exception -> 0x02f3 }
                com.incipio.tumiandroid.tumibank.activities.DeviceControlActivity_Above.minutes = r9     // Catch:{ Exception -> 0x02f3 }
                java.lang.String r9 = "com.example.bluetooth.le.EXTRA_HOURS"
                java.lang.String r9 = r2.getString(r9)     // Catch:{ Exception -> 0x02f3 }
                com.incipio.tumiandroid.tumibank.activities.DeviceControlActivity_Above.hours = r9     // Catch:{ Exception -> 0x02f3 }
                java.lang.String r9 = "com.example.bluetooth.le.EXTRA_CHARGE_STATUS"
                java.lang.String r9 = r2.getString(r9)     // Catch:{ Exception -> 0x02f3 }
                com.incipio.tumiandroid.tumibank.activities.DeviceControlActivity_Above.charging_status = r9     // Catch:{ Exception -> 0x02f3 }
                java.lang.String r9 = "TAG"
                java.lang.StringBuilder r10 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x02f3 }
                r10.<init>()     // Catch:{ Exception -> 0x02f3 }
                java.lang.String r11 = "charging_status 1 = "
                java.lang.StringBuilder r10 = r10.append(r11)     // Catch:{ Exception -> 0x02f3 }
                java.lang.String r11 = com.incipio.tumiandroid.tumibank.activities.DeviceControlActivity_Above.charging_status     // Catch:{ Exception -> 0x02f3 }
                java.lang.StringBuilder r10 = r10.append(r11)     // Catch:{ Exception -> 0x02f3 }
                java.lang.String r10 = r10.toString()     // Catch:{ Exception -> 0x02f3 }
                android.util.Log.v(r9, r10)     // Catch:{ Exception -> 0x02f3 }
                java.lang.String r9 = "com.example.bluetooth.le.EXTRA_LOWBYTES"
                java.lang.String r9 = r2.getString(r9)     // Catch:{ Exception -> 0x02f3 }
                com.incipio.tumiandroid.tumibank.activities.DeviceControlActivity_Above.lowbyte = r9     // Catch:{ Exception -> 0x02f3 }
            L_0x0284:
                com.incipio.tumiandroid.tumibank.activities.DeviceControlActivity_Above.batteryvaluet = r3     // Catch:{ Exception -> 0x02ed }
                java.lang.String r9 = "TAG"
                java.lang.StringBuilder r10 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x02ed }
                r10.<init>()     // Catch:{ Exception -> 0x02ed }
                java.lang.String r11 = "hours 1 = "
                java.lang.StringBuilder r10 = r10.append(r11)     // Catch:{ Exception -> 0x02ed }
                java.lang.String r11 = com.incipio.tumiandroid.tumibank.activities.DeviceControlActivity_Above.hours     // Catch:{ Exception -> 0x02ed }
                java.lang.StringBuilder r10 = r10.append(r11)     // Catch:{ Exception -> 0x02ed }
                java.lang.String r10 = r10.toString()     // Catch:{ Exception -> 0x02ed }
                android.util.Log.v(r9, r10)     // Catch:{ Exception -> 0x02ed }
                java.lang.String r9 = "TAG"
                java.lang.StringBuilder r10 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x02ed }
                r10.<init>()     // Catch:{ Exception -> 0x02ed }
                java.lang.String r11 = "minutes 1 = "
                java.lang.StringBuilder r10 = r10.append(r11)     // Catch:{ Exception -> 0x02ed }
                java.lang.String r11 = com.incipio.tumiandroid.tumibank.activities.DeviceControlActivity_Above.minutes     // Catch:{ Exception -> 0x02ed }
                java.lang.StringBuilder r10 = r10.append(r11)     // Catch:{ Exception -> 0x02ed }
                java.lang.String r10 = r10.toString()     // Catch:{ Exception -> 0x02ed }
                android.util.Log.v(r9, r10)     // Catch:{ Exception -> 0x02ed }
                java.lang.String r9 = "INFO"
                java.lang.StringBuilder r10 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x02ed }
                r10.<init>()     // Catch:{ Exception -> 0x02ed }
                java.lang.String r11 = "hours 1 = "
                java.lang.StringBuilder r10 = r10.append(r11)     // Catch:{ Exception -> 0x02ed }
                java.lang.String r11 = com.incipio.tumiandroid.tumibank.activities.DeviceControlActivity_Above.hours     // Catch:{ Exception -> 0x02ed }
                java.lang.StringBuilder r10 = r10.append(r11)     // Catch:{ Exception -> 0x02ed }
                java.lang.String r11 = " minutes 1 = "
                java.lang.StringBuilder r10 = r10.append(r11)     // Catch:{ Exception -> 0x02ed }
                java.lang.String r11 = com.incipio.tumiandroid.tumibank.activities.DeviceControlActivity_Above.minutes     // Catch:{ Exception -> 0x02ed }
                java.lang.StringBuilder r10 = r10.append(r11)     // Catch:{ Exception -> 0x02ed }
                java.lang.String r10 = r10.toString()     // Catch:{ Exception -> 0x02ed }
                android.util.Log.v(r9, r10)     // Catch:{ Exception -> 0x02ed }
                com.incipio.tumiandroid.tumibank.activities.DeviceControlActivity_Above r9 = com.incipio.tumiandroid.tumibank.activities.DeviceControlActivity_Above.this     // Catch:{ Exception -> 0x02ed }
                java.lang.String r10 = com.incipio.tumiandroid.tumibank.activities.DeviceControlActivity_Above.minutes     // Catch:{ Exception -> 0x02ed }
                java.lang.String r11 = com.incipio.tumiandroid.tumibank.activities.DeviceControlActivity_Above.hours     // Catch:{ Exception -> 0x02ed }
                java.lang.String r12 = com.incipio.tumiandroid.tumibank.activities.DeviceControlActivity_Above.charging_status     // Catch:{ Exception -> 0x02ed }
                r9.displayData(r3, r10, r11, r12)     // Catch:{ Exception -> 0x02ed }
                goto L_0x0044
            L_0x02ed:
                r4 = move-exception
                r4.printStackTrace()     // Catch:{ Exception -> 0x01ea }
                goto L_0x0044
            L_0x02f3:
                r9 = move-exception
                goto L_0x0284
            L_0x02f5:
                r9 = move-exception
                goto L_0x0188
            L_0x02f8:
                r9 = move-exception
                goto L_0x00d5
            */
            throw new UnsupportedOperationException("Method not decompiled: com.incipio.tumiandroid.tumibank.activities.DeviceControlActivity_Above.AnonymousClass2.onReceive(android.content.Context, android.content.Intent):void");
        }
    };
    public SectionsPagerAdapter mSectionsPagerAdapter;
    public ServiceConnection mServiceConnection = new ServiceConnection() {
        public void onServiceConnected(ComponentName componentName, IBinder service) {
            DeviceControlActivity_Above.mBluetoothLeService = ((BluetoothLeService.LocalBinder) service).getService();
            if (!DeviceControlActivity_Above.mBluetoothLeService.initialize()) {
            }
            DeviceControlActivity_Above.mBluetoothLeService.connect(DeviceControlActivity_Above.mDeviceAddress);
        }

        public void onServiceDisconnected(ComponentName componentName) {
            DeviceControlActivity_Above.mBluetoothLeService = null;
        }
    };
    private SavedPreferences savedPreferences;

    public interface Updateable {
        void update();
    }

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Log.v("INFO", "onCreate");
        Log.v("INFO", "activity = " + getLocalClassName());
        setContentView(R.layout.section);
        this.mSectionsPagerAdapter = new SectionsPagerAdapter(getSupportFragmentManager());
        mGoogleApiClient = new GoogleApiClient.Builder(this).addConnectionCallbacks(this).addOnConnectionFailedListener(this).addApi(LocationServices.API).build();
        mLocationRequest = LocationRequest.create().setPriority(100).setInterval(3600000).setFastestInterval(60000);
        int status = GooglePlayServicesUtil.isGooglePlayServicesAvailable(getBaseContext());
        if (status != 0) {
            GooglePlayServicesUtil.getErrorDialog(status, this, 10).show();
        } else if (!((LocationManager) getSystemService(Headers.LOCATION)).isProviderEnabled("gps")) {
            buildAlertMessageNoGps();
        }
        this.savedPreferences = new SavedPreferences(getApplicationContext());
        String latlng = this.savedPreferences.getLastLocation(RecordLocation.LATITUDE, RecordLocation.LONGITUDE);
        this.lat = Double.parseDouble(latlng.split(":")[0]);
        this.lng = Double.parseDouble(latlng.split(":")[1]);
        Log.v("TEST", "lat*****************" + this.lat);
        Log.v("TEST", "lng*****************" + this.lng);
        is_showing = getIntent().getExtras().getString("Activity").equals("First");
        mViewPager = (ViewPager) findViewById(R.id.myviewpager);
        int index = 0;
        mViewPager.setOnTouchListener(new View.OnTouchListener() {
            public boolean onTouch(View v, MotionEvent event) {
                return true;
            }
        });
        try {
            mDeviceName = getIntent().getStringExtra(EXTRAS_DEVICE_NAME);
            mDeviceAddress = getIntent().getStringExtra(EXTRAS_DEVICE_ADDRESS);
            int count = Integer.parseInt(DeviceScanActivity.session.loadSavedPreferences("COUNT"));
            for (int i = 0; i < count; i++) {
                String tempAddress = DeviceScanActivity.session.loadSavedPreferences("DEVICEBLEADDRESS_" + (i + 1));
                if (!(tempAddress == null || mDeviceAddress == null || !tempAddress.trim().equalsIgnoreCase(mDeviceAddress.trim()))) {
                    index = i;
                }
            }
        } catch (Exception e) {
            index = 0;
            e.printStackTrace();
        }
        try {
            mViewPager.setAdapter(this.mSectionsPagerAdapter);
        } catch (Exception e2) {
        }
        ViewPager.OnPageChangeListener pageChangeListener = new ViewPager.OnPageChangeListener() {
            public void onPageScrollStateChanged(int arg0) {
            }

            public void onPageScrolled(int arg0, float arg1, int arg2) {
            }

            public void onPageSelected(int arg0) {
                DeviceControlActivity_Above.batteryvaluet = "0";
                DeviceControlActivity_Above.minutes = "0";
                DeviceControlActivity_Above.hours = "0";
                DeviceControlActivity_Above.charging_status = "0";
                Log.v("TAG", "charging_status 3 = " + DeviceControlActivity_Above.charging_status);
                DeviceControlActivity_Above.current_page = arg0 + 1;
                Log.v("TAG", "current_page 0 = " + DeviceControlActivity_Above.current_page);
                DeviceControlActivity_Above.isdisconnected = 0;
                try {
                    DeviceControlActivity_Above.mDeviceAddress = DeviceScanActivity.session.loadSavedPreferences("DEVICEBLEADDRESS_" + (arg0 + 1));
                } catch (Exception e) {
                }
                DeviceControlActivity_Above.mViewPager.invalidate();
                DeviceControlActivity_Above.this.mSectionsPagerAdapter.notifyDataSetChanged();
            }
        };
        mViewPager.setOnPageChangeListener(pageChangeListener);
        try {
            pageChangeListener.onPageSelected(index);
        } catch (Exception e3) {
        }
        mViewPager.setCurrentItem(index);
        checkFirstRun();
        mViewPager.setOnTouchListener(new View.OnTouchListener() {
            public boolean onTouch(View v, MotionEvent event) {
                return true;
            }
        });
    }

    /* access modifiers changed from: protected */
    public void onResume() {
        mGoogleApiClient.connect();
        Log.v("INFO", "onResume");
        this.isPause = false;
        super.onResume();
        progress = new ProgressDialog(this);
        registerReceiver(this.mGattUpdateReceiver, makeGattUpdateIntentFilter());
        if (mBluetoothLeService != null) {
            boolean result = mBluetoothLeService.connect(mDeviceAddress);
        }
        bindService(new Intent(this, BluetoothLeService.class), this.mServiceConnection, 1);
    }

    /* access modifiers changed from: protected */
    public void onPause() {
        super.onPause();
        Log.v("INFO", "onPause");
        if (mGoogleApiClient.isConnected()) {
            LocationServices.FusedLocationApi.removeLocationUpdates(mGoogleApiClient, (LocationListener) this);
            mGoogleApiClient.disconnect();
        }
    }

    /* access modifiers changed from: protected */
    public void onStop() {
        super.onStop();
        this.isPause = true;
        Log.v("INFO", "onStop");
        if (AccelerometerManager.isListening()) {
            AccelerometerManager.stopListening();
        }
    }

    /* access modifiers changed from: protected */
    public void onDestroy() {
        super.onDestroy();
        Log.v("INFO", "onDestroy");
        isInbackground = true;
        connectBool = false;
        unbindService(this.mServiceConnection);
        mBluetoothLeService = null;
        try {
            unregisterReceiver(this.mGattUpdateReceiver);
        } catch (Exception e) {
        }
    }

    /* access modifiers changed from: private */
    public void updateConnectionState(int resourceId) {
        runOnUiThread(new Runnable() {
            public void run() {
            }
        });
    }

    /* JADX WARNING: Code restructure failed: missing block: B:31:0x016a, code lost:
        if (r20.equals(chargeColor) == false) goto L_0x016c;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:63:0x0281, code lost:
        if (r20.equals(chargeColor) == false) goto L_0x0283;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void displayData(String r17, String r18, String r19, String r20) {
        /*
            r16 = this;
            if (r17 == 0) goto L_0x01bf
            android.support.v4.view.ViewPager r12 = mViewPager
            int r6 = r12.getCurrentItem()
            android.support.v4.view.ViewPager r12 = mViewPager
            android.support.v4.view.PagerAdapter r1 = r12.getAdapter()
            com.incipio.tumiandroid.tumibank.activities.DeviceControlActivity_Above$SectionsPagerAdapter r1 = (com.incipio.tumiandroid.tumibank.activities.DeviceControlActivity_Above.SectionsPagerAdapter) r1
            android.support.v4.app.Fragment r5 = r1.getFragment(r6)
            com.incipio.tumiandroid.tumibank.activities.DeviceControlActivity_Above$DummySectionFragment r5 = (com.incipio.tumiandroid.tumibank.activities.DeviceControlActivity_Above.DummySectionFragment) r5
            android.widget.TextView r12 = battery
            if (r12 != 0) goto L_0x0029
            android.view.View r12 = r5.getView()
            r13 = 2131558575(0x7f0d00af, float:1.874247E38)
            android.view.View r12 = r12.findViewById(r13)
            android.widget.TextView r12 = (android.widget.TextView) r12
            battery = r12
        L_0x0029:
            android.widget.TextView r12 = battery
            r0 = r17
            r12.setText(r0)
            java.lang.String r12 = "INFO"
            java.lang.StringBuilder r13 = new java.lang.StringBuilder
            r13.<init>()
            java.lang.String r14 = "charge "
            java.lang.StringBuilder r13 = r13.append(r14)
            r0 = r20
            java.lang.StringBuilder r13 = r13.append(r0)
            java.lang.String r13 = r13.toString()
            android.util.Log.v(r12, r13)
            isCharge = r20
            android.widget.TextView r12 = hrs
            if (r12 != 0) goto L_0x005f
            android.view.View r12 = r5.getView()
            r13 = 2131558577(0x7f0d00b1, float:1.8742474E38)
            android.view.View r12 = r12.findViewById(r13)
            android.widget.TextView r12 = (android.widget.TextView) r12
            hrs = r12
        L_0x005f:
            android.widget.TextView r12 = min
            if (r12 != 0) goto L_0x0072
            android.view.View r12 = r5.getView()
            r13 = 2131558657(0x7f0d0101, float:1.8742636E38)
            android.view.View r12 = r12.findViewById(r13)
            android.widget.TextView r12 = (android.widget.TextView) r12
            min = r12
        L_0x0072:
            android.widget.TextView r12 = hrs
            r0 = r19
            r12.setText(r0)
            android.widget.TextView r12 = min
            r0 = r18
            r12.setText(r0)
            android.widget.TextView r12 = devname
            if (r12 != 0) goto L_0x0093
            android.view.View r12 = r5.getView()
            r13 = 2131558569(0x7f0d00a9, float:1.8742458E38)
            android.view.View r12 = r12.findViewById(r13)
            android.widget.TextView r12 = (android.widget.TextView) r12
            devname = r12
        L_0x0093:
            android.widget.TextView r12 = devname
            com.incipio.tumiandroid.tumibank.utils.SavedPreferences r13 = com.incipio.tumiandroid.tumibank.activities.DeviceScanActivity.session
            java.lang.StringBuilder r14 = new java.lang.StringBuilder
            r14.<init>()
            java.lang.String r15 = "DEVICENAME_"
            java.lang.StringBuilder r14 = r14.append(r15)
            int r15 = current_page
            java.lang.StringBuilder r14 = r14.append(r15)
            java.lang.String r14 = r14.toString()
            java.lang.String r13 = r13.loadSavedPreferences(r14)
            r12.setText(r13)
            java.lang.String r3 = "fonts/proximanova_regular.otf"
            java.lang.String r4 = "fonts/gothammedium.otf"
            android.content.res.AssetManager r12 = r16.getAssets()
            android.graphics.Typeface r9 = android.graphics.Typeface.createFromAsset(r12, r3)
            android.content.res.AssetManager r12 = r16.getAssets()
            android.graphics.Typeface r10 = android.graphics.Typeface.createFromAsset(r12, r4)
            android.widget.TextView r12 = devname
            r12.setTypeface(r9)
            android.widget.TextView r12 = battery
            r12.setTypeface(r9)
            android.widget.TextView r12 = hrs
            r12.setTypeface(r10)
            android.widget.TextView r12 = min
            r12.setTypeface(r10)
            de.passsy.holocircularprogressbar.HoloCircularProgressBar r12 = mHoloCircularProgressBar
            if (r12 != 0) goto L_0x00ee
            android.view.View r12 = r5.getView()
            r13 = 2131558574(0x7f0d00ae, float:1.8742468E38)
            android.view.View r12 = r12.findViewById(r13)
            de.passsy.holocircularprogressbar.HoloCircularProgressBar r12 = (de.passsy.holocircularprogressbar.HoloCircularProgressBar) r12
            mHoloCircularProgressBar = r12
        L_0x00ee:
            android.widget.ImageView r12 = batterylevel_iv
            if (r12 != 0) goto L_0x0101
            android.view.View r12 = r5.getView()
            r13 = 2131558573(0x7f0d00ad, float:1.8742466E38)
            android.view.View r12 = r12.findViewById(r13)
            android.widget.ImageView r12 = (android.widget.ImageView) r12
            batterylevel_iv = r12
        L_0x0101:
            android.widget.TextView r12 = charge_txt
            if (r12 != 0) goto L_0x0114
            android.view.View r12 = r5.getView()
            r13 = 2131558578(0x7f0d00b2, float:1.8742476E38)
            android.view.View r12 = r12.findViewById(r13)
            android.widget.TextView r12 = (android.widget.TextView) r12
            charge_txt = r12
        L_0x0114:
            java.lang.String r12 = "11"
            r0 = r20
            boolean r12 = r0.equalsIgnoreCase(r12)     // Catch:{ Exception -> 0x02c5 }
            if (r12 == 0) goto L_0x025d
            java.lang.String r12 = "INFO"
            java.lang.StringBuilder r13 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x02c5 }
            r13.<init>()     // Catch:{ Exception -> 0x02c5 }
            java.lang.String r14 = " chargedata "
            java.lang.StringBuilder r13 = r13.append(r14)     // Catch:{ Exception -> 0x02c5 }
            java.lang.String r14 = chargedata     // Catch:{ Exception -> 0x02c5 }
            java.lang.StringBuilder r13 = r13.append(r14)     // Catch:{ Exception -> 0x02c5 }
            java.lang.String r14 = " data "
            java.lang.StringBuilder r13 = r13.append(r14)     // Catch:{ Exception -> 0x02c5 }
            r0 = r17
            java.lang.StringBuilder r13 = r13.append(r0)     // Catch:{ Exception -> 0x02c5 }
            java.lang.String r14 = " color "
            java.lang.StringBuilder r13 = r13.append(r14)     // Catch:{ Exception -> 0x02c5 }
            java.lang.String r14 = chargeColor     // Catch:{ Exception -> 0x02c5 }
            java.lang.StringBuilder r13 = r13.append(r14)     // Catch:{ Exception -> 0x02c5 }
            java.lang.String r13 = r13.toString()     // Catch:{ Exception -> 0x02c5 }
            android.util.Log.v(r12, r13)     // Catch:{ Exception -> 0x02c5 }
            java.lang.String r12 = chargedata     // Catch:{ Exception -> 0x02c5 }
            int r12 = r12.length()     // Catch:{ Exception -> 0x02c5 }
            if (r12 == 0) goto L_0x016c
            java.lang.String r12 = chargedata     // Catch:{ Exception -> 0x02c5 }
            r0 = r17
            boolean r12 = r12.equals(r0)     // Catch:{ Exception -> 0x02c5 }
            if (r12 == 0) goto L_0x016c
            java.lang.String r12 = chargeColor     // Catch:{ Exception -> 0x02c5 }
            r0 = r20
            boolean r12 = r0.equals(r12)     // Catch:{ Exception -> 0x02c5 }
            if (r12 != 0) goto L_0x0193
        L_0x016c:
            java.lang.String r12 = "INFO"
            java.lang.String r13 = "----------------------------"
            android.util.Log.v(r12, r13)     // Catch:{ Exception -> 0x02c5 }
            chargedata = r17     // Catch:{ Exception -> 0x02c5 }
            java.lang.String r12 = "11"
            chargeColor = r12     // Catch:{ Exception -> 0x02c5 }
            de.passsy.holocircularprogressbar.HoloCircularProgressBar r12 = mHoloCircularProgressBar     // Catch:{ Exception -> 0x02c5 }
            r13 = 0
            float r14 = java.lang.Float.parseFloat(r17)     // Catch:{ Exception -> 0x02c5 }
            r15 = 1120403456(0x42c80000, float:100.0)
            float r14 = r14 / r15
            r15 = 300(0x12c, float:4.2E-43)
            animate(r12, r13, r14, r15)     // Catch:{ Exception -> 0x02c5 }
            de.passsy.holocircularprogressbar.HoloCircularProgressBar r12 = mHoloCircularProgressBar     // Catch:{ Exception -> 0x02c5 }
            java.lang.String r13 = "#b70e30"
            int r13 = android.graphics.Color.parseColor(r13)     // Catch:{ Exception -> 0x02c5 }
            r12.setProgressColor(r13)     // Catch:{ Exception -> 0x02c5 }
        L_0x0193:
            android.widget.ImageView r12 = batterylevel_iv     // Catch:{ Exception -> 0x02c5 }
            r13 = 2130837585(0x7f020051, float:1.7280128E38)
            r12.setBackgroundResource(r13)     // Catch:{ Exception -> 0x02c5 }
            android.widget.TextView r12 = charge_txt     // Catch:{ Exception -> 0x02c5 }
            java.lang.String r13 = "TO DISCHARGE"
            r12.setText(r13)     // Catch:{ Exception -> 0x02c5 }
            android.graphics.drawable.AnimationDrawable r12 = animation_green     // Catch:{ Exception -> 0x02c5 }
            r12.stop()     // Catch:{ Exception -> 0x02c5 }
            android.widget.ImageView r12 = anim_timer     // Catch:{ Exception -> 0x02c5 }
            android.graphics.drawable.AnimationDrawable r13 = animation     // Catch:{ Exception -> 0x02c5 }
            r12.setBackgroundDrawable(r13)     // Catch:{ Exception -> 0x02c5 }
            android.graphics.drawable.AnimationDrawable r12 = animation     // Catch:{ Exception -> 0x02c5 }
            r12.start()     // Catch:{ Exception -> 0x02c5 }
        L_0x01b3:
            android.support.v4.view.ViewPager r12 = mViewPager
            r12.invalidate()
            r0 = r16
            com.incipio.tumiandroid.tumibank.activities.DeviceControlActivity_Above$SectionsPagerAdapter r12 = r0.mSectionsPagerAdapter
            r12.notifyDataSetChanged()
        L_0x01bf:
            com.incipio.tumiandroid.tumibank.utils.SavedPreferences r12 = com.incipio.tumiandroid.tumibank.activities.DeviceScanActivity.session
            java.lang.StringBuilder r13 = new java.lang.StringBuilder
            r13.<init>()
            java.lang.String r14 = "DEVICENAME_"
            java.lang.StringBuilder r13 = r13.append(r14)
            int r14 = current_page
            java.lang.StringBuilder r13 = r13.append(r14)
            java.lang.String r14 = "|THRESHOLD"
            java.lang.StringBuilder r13 = r13.append(r14)
            java.lang.String r13 = r13.toString()
            java.lang.String r12 = r12.loadSavedPreferences3(r13)
            int r11 = java.lang.Integer.parseInt(r12)
            com.incipio.tumiandroid.tumibank.utils.SavedPreferences r12 = com.incipio.tumiandroid.tumibank.activities.DeviceScanActivity.session
            java.lang.StringBuilder r13 = new java.lang.StringBuilder
            r13.<init>()
            java.lang.String r14 = "DEVICENAME_"
            java.lang.StringBuilder r13 = r13.append(r14)
            int r14 = current_page
            java.lang.StringBuilder r13 = r13.append(r14)
            java.lang.String r14 = "|CHECKED"
            java.lang.StringBuilder r13 = r13.append(r14)
            java.lang.String r13 = r13.toString()
            java.lang.String r12 = r12.loadSavedPreferences2(r13)
            boolean r12 = java.lang.Boolean.parseBoolean(r12)
            java.lang.Boolean r8 = java.lang.Boolean.valueOf(r12)
            com.incipio.tumiandroid.tumibank.utils.SavedPreferences r12 = com.incipio.tumiandroid.tumibank.activities.DeviceScanActivity.session
            java.lang.String r13 = "SAVEDBATTERY"
            java.lang.String r7 = r12.loadSavedPreferencesforbattery(r13)
            java.lang.String r12 = chargedata     // Catch:{ NumberFormatException -> 0x02d3 }
            int r12 = java.lang.Integer.parseInt(r12)     // Catch:{ NumberFormatException -> 0x02d3 }
            if (r12 > r11) goto L_0x0237
            boolean r12 = r8.booleanValue()     // Catch:{ NumberFormatException -> 0x02d3 }
            if (r12 == 0) goto L_0x0237
            com.incipio.tumiandroid.tumibank.utils.SavedPreferences r12 = com.incipio.tumiandroid.tumibank.activities.DeviceScanActivity.session     // Catch:{ NumberFormatException -> 0x02d3 }
            java.lang.String r13 = "SAVEDBATTERY"
            java.lang.String r12 = r12.loadSavedPreferencesforbattery(r13)     // Catch:{ NumberFormatException -> 0x02d3 }
            java.lang.String r13 = chargedata     // Catch:{ NumberFormatException -> 0x02d3 }
            boolean r12 = r12.equalsIgnoreCase(r13)     // Catch:{ NumberFormatException -> 0x02d3 }
            if (r12 != 0) goto L_0x0237
            boolean r12 = port1checked     // Catch:{ NumberFormatException -> 0x02d3 }
            if (r12 != 0) goto L_0x0259
        L_0x0237:
            java.lang.String r12 = chargedata     // Catch:{ NumberFormatException -> 0x02d3 }
            int r12 = java.lang.Integer.parseInt(r12)     // Catch:{ NumberFormatException -> 0x02d3 }
            if (r12 > r11) goto L_0x025c
            boolean r12 = r8.booleanValue()     // Catch:{ NumberFormatException -> 0x02d3 }
            if (r12 == 0) goto L_0x025c
            com.incipio.tumiandroid.tumibank.utils.SavedPreferences r12 = com.incipio.tumiandroid.tumibank.activities.DeviceScanActivity.session     // Catch:{ NumberFormatException -> 0x02d3 }
            java.lang.String r13 = "SAVEDBATTERY"
            java.lang.String r12 = r12.loadSavedPreferencesforbattery(r13)     // Catch:{ NumberFormatException -> 0x02d3 }
            java.lang.String r13 = chargedata     // Catch:{ NumberFormatException -> 0x02d3 }
            boolean r12 = r12.equalsIgnoreCase(r13)     // Catch:{ NumberFormatException -> 0x02d3 }
            if (r12 != 0) goto L_0x025c
            boolean r12 = port2checked     // Catch:{ NumberFormatException -> 0x02d3 }
            if (r12 == 0) goto L_0x025c
        L_0x0259:
            r16.threshold_alert()     // Catch:{ NumberFormatException -> 0x02d3 }
        L_0x025c:
            return
        L_0x025d:
            java.lang.String r12 = "10"
            r0 = r20
            boolean r12 = r0.equalsIgnoreCase(r12)     // Catch:{ Exception -> 0x02c5 }
            if (r12 == 0) goto L_0x02c8
            java.lang.String r12 = chargedata     // Catch:{ Exception -> 0x02c5 }
            int r12 = r12.length()     // Catch:{ Exception -> 0x02c5 }
            if (r12 == 0) goto L_0x0283
            java.lang.String r12 = chargedata     // Catch:{ Exception -> 0x02c5 }
            r0 = r17
            boolean r12 = r12.equals(r0)     // Catch:{ Exception -> 0x02c5 }
            if (r12 == 0) goto L_0x0283
            java.lang.String r12 = chargeColor     // Catch:{ Exception -> 0x02c5 }
            r0 = r20
            boolean r12 = r0.equals(r12)     // Catch:{ Exception -> 0x02c5 }
            if (r12 != 0) goto L_0x02a3
        L_0x0283:
            chargedata = r17     // Catch:{ Exception -> 0x02c5 }
            java.lang.String r12 = "10"
            chargeColor = r12     // Catch:{ Exception -> 0x02c5 }
            de.passsy.holocircularprogressbar.HoloCircularProgressBar r12 = mHoloCircularProgressBar     // Catch:{ Exception -> 0x02c5 }
            r13 = 0
            float r14 = java.lang.Float.parseFloat(r17)     // Catch:{ Exception -> 0x02c5 }
            r15 = 1120403456(0x42c80000, float:100.0)
            float r14 = r14 / r15
            r15 = 300(0x12c, float:4.2E-43)
            animate(r12, r13, r14, r15)     // Catch:{ Exception -> 0x02c5 }
            de.passsy.holocircularprogressbar.HoloCircularProgressBar r12 = mHoloCircularProgressBar     // Catch:{ Exception -> 0x02c5 }
            java.lang.String r13 = "#484848"
            int r13 = android.graphics.Color.parseColor(r13)     // Catch:{ Exception -> 0x02c5 }
            r12.setProgressColor(r13)     // Catch:{ Exception -> 0x02c5 }
        L_0x02a3:
            android.widget.ImageView r12 = batterylevel_iv     // Catch:{ Exception -> 0x02c5 }
            r13 = 2130837584(0x7f020050, float:1.7280126E38)
            r12.setBackgroundResource(r13)     // Catch:{ Exception -> 0x02c5 }
            android.widget.TextView r12 = charge_txt     // Catch:{ Exception -> 0x02c5 }
            java.lang.String r13 = "TO CHARGE"
            r12.setText(r13)     // Catch:{ Exception -> 0x02c5 }
            android.graphics.drawable.AnimationDrawable r12 = animation     // Catch:{ Exception -> 0x02c5 }
            r12.stop()     // Catch:{ Exception -> 0x02c5 }
            android.widget.ImageView r12 = anim_timer     // Catch:{ Exception -> 0x02c5 }
            android.graphics.drawable.AnimationDrawable r13 = animation_green     // Catch:{ Exception -> 0x02c5 }
            r12.setBackgroundDrawable(r13)     // Catch:{ Exception -> 0x02c5 }
            android.graphics.drawable.AnimationDrawable r12 = animation_green     // Catch:{ Exception -> 0x02c5 }
            r12.start()     // Catch:{ Exception -> 0x02c5 }
            goto L_0x01b3
        L_0x02c5:
            r12 = move-exception
            goto L_0x01b3
        L_0x02c8:
            de.passsy.holocircularprogressbar.HoloCircularProgressBar r12 = mHoloCircularProgressBar     // Catch:{ Exception -> 0x02c5 }
            r13 = 0
            r14 = 0
            r15 = 10
            animate(r12, r13, r14, r15)     // Catch:{ Exception -> 0x02c5 }
            goto L_0x01b3
        L_0x02d3:
            r2 = move-exception
            r2.printStackTrace()
            goto L_0x025c
        */
        throw new UnsupportedOperationException("Method not decompiled: com.incipio.tumiandroid.tumibank.activities.DeviceControlActivity_Above.displayData(java.lang.String, java.lang.String, java.lang.String, java.lang.String):void");
    }

    /* access modifiers changed from: private */
    public void check_forservices(List<BluetoothGattService> gattServices) {
        if (gattServices != null) {
            String unknownServiceString = getResources().getString(R.string.unknown_service);
            String unknownCharaString = getResources().getString(R.string.unknown_characteristic);
            ArrayList<HashMap<String, String>> gattServiceData = new ArrayList<>();
            ArrayList<ArrayList<HashMap<String, String>>> gattCharacteristicData = new ArrayList<>();
            mGattCharacteristics = new ArrayList<>();
            for (BluetoothGattService gattService : gattServices) {
                HashMap<String, String> currentServiceData = new HashMap<>();
                String uuid = gattService.getUuid().toString();
                if (uuid.equalsIgnoreCase(SampleGattAttributes.BATTERY_SERVICE)) {
                    currentServiceData.put(LIST_NAME, SampleGattAttributes.lookup(uuid, unknownServiceString));
                    currentServiceData.put(LIST_UUID, uuid);
                    gattServiceData.add(currentServiceData);
                    ArrayList<HashMap<String, String>> gattCharacteristicGroupData = new ArrayList<>();
                    List<BluetoothGattCharacteristic> gattCharacteristics = gattService.getCharacteristics();
                    ArrayList<BluetoothGattCharacteristic> charas = new ArrayList<>();
                    for (BluetoothGattCharacteristic gattCharacteristic : gattCharacteristics) {
                        charas.add(gattCharacteristic);
                        HashMap<String, String> currentCharaData = new HashMap<>();
                        String uuid2 = gattCharacteristic.getUuid().toString();
                        currentCharaData.put(LIST_NAME, SampleGattAttributes.lookup(uuid2, unknownCharaString));
                        currentCharaData.put(LIST_UUID, uuid2);
                        gattCharacteristicGroupData.add(currentCharaData);
                        int charrec = gattCharacteristic.getProperties();
                        if ((charrec | 8) > 0 && uuid2.equalsIgnoreCase(SampleGattAttributes.OffGRID_CHARACTERISITC.toString())) {
                            hascharac = 1;
                        }
                        if (uuid2.equalsIgnoreCase(SampleGattAttributes.NOTIFY_CHARACTERISTIC.toString())) {
                            if ((charrec | 2) > 0) {
                                if (mNotifyCharacteristic != null) {
                                    mBluetoothLeService.setCharacteristicNotification(mNotifyCharacteristic, false);
                                    mNotifyCharacteristic = gattCharacteristic;
                                }
                                mBluetoothLeService.readCharacteristic(gattCharacteristic);
                            }
                            if ((charrec | 16) > 0) {
                                mNotifyCharacteristic = gattCharacteristic;
                                mBluetoothLeService.setCharacteristicNotification(gattCharacteristic, true);
                                mBluetoothLeService.writeCharacteristic(new byte[]{9, Ascii.SI, 34, 70, 70, 70, 70, 70, 98}, 1);
                            }
                        }
                        if (uuid2.equalsIgnoreCase(SampleGattAttributes.PORT_NOTIFY_CHARACTERISTIC.toString()) && (charrec | 16) > 0) {
                            mNotifyCharacteristic = gattCharacteristic;
                            mBluetoothLeService.setCharacteristicNotification(gattCharacteristic, true);
                        }
                        mGattCharacteristics.add(charas);
                        gattCharacteristicData.add(gattCharacteristicGroupData);
                    }
                }
            }
        }
    }

    private static IntentFilter makeGattUpdateIntentFilter() {
        IntentFilter intentFilter = new IntentFilter();
        intentFilter.addAction(BluetoothLeService.ACTION_GATT_CONNECTED);
        intentFilter.addAction(BluetoothLeService.ACTION_GATT_DISCONNECTED);
        intentFilter.addAction(BluetoothLeService.ACTION_GATT_SERVICES_DISCOVERED);
        intentFilter.addAction(BluetoothLeService.ACTION_DATA_AVAILABLE);
        return intentFilter;
    }

    public void onContentChanged() {
        super.onContentChanged();
    }

    /* access modifiers changed from: protected */
    public void onRestart() {
        super.onRestart();
        Log.v("INFO", "on restart");
    }

    public boolean onCreateOptionsMenu(Menu menu2) {
        return true;
    }

    public boolean onOptionsItemSelected(MenuItem item) {
        return super.onOptionsItemSelected(item);
    }

    private void animate(HoloCircularProgressBar progressBar, Animator.AnimatorListener listener) {
        animate(progressBar, listener, (float) (Math.random() * 2.0d), 3000);
    }

    /* access modifiers changed from: private */
    public static void animate(final HoloCircularProgressBar progressBar, Animator.AnimatorListener listener, final float progress2, int duration) {
        mProgressBarAnimator = ObjectAnimator.ofFloat(progressBar, "progress", new float[]{progress2});
        mProgressBarAnimator.setDuration((long) duration);
        mProgressBarAnimator.addListener(new Animator.AnimatorListener() {
            public void onAnimationCancel(Animator animation) {
            }

            public void onAnimationEnd(Animator animation) {
                progressBar.setProgress(progress2);
            }

            public void onAnimationRepeat(Animator animation) {
            }

            public void onAnimationStart(Animator animation) {
            }
        });
        if (listener != null) {
            mProgressBarAnimator.addListener(listener);
        }
        mProgressBarAnimator.reverse();
        mProgressBarAnimator.addUpdateListener(new ValueAnimator.AnimatorUpdateListener() {
            public void onAnimationUpdate(ValueAnimator animation) {
                progressBar.setProgress(((Float) animation.getAnimatedValue()).floatValue());
            }
        });
        progressBar.setMarkerProgress(progress2);
        mProgressBarAnimator.start();
    }

    private void animate2(final HoloCircularProgressBar progressBar, Animator.AnimatorListener listener, final float progress2, int duration) {
        mProgressBarAnimator = ObjectAnimator.ofFloat(progressBar, "progress", new float[]{progress2});
        mProgressBarAnimator.setDuration((long) duration);
        mProgressBarAnimator.addListener(new Animator.AnimatorListener() {
            public void onAnimationCancel(Animator animation) {
            }

            public void onAnimationEnd(Animator animation) {
                progressBar.setProgress(progress2);
            }

            public void onAnimationRepeat(Animator animation) {
            }

            public void onAnimationStart(Animator animation) {
            }
        });
        if (listener != null) {
            mProgressBarAnimator.addListener(listener);
        }
        mProgressBarAnimator.reverse();
        mProgressBarAnimator.addUpdateListener(new ValueAnimator.AnimatorUpdateListener() {
            public void onAnimationUpdate(ValueAnimator animation) {
                progressBar.setProgress(((Float) animation.getAnimatedValue()).floatValue());
            }
        });
        progressBar.setMarkerProgress(progress2);
        mProgressBarAnimator.start();
    }

    public void connected_dialog() {
        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(this);
        alertDialogBuilder.setMessage("Successfully connected to the device.").setCancelable(false).setPositiveButton("Ok", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
            }
        });
        alertDialog = alertDialogBuilder.create();
        alertDialog.requestWindowFeature(1);
        try {
            Log.v("INFO", "page = " + is_showing);
            if (is_showing) {
                is_showing = false;
                alertDialog.show();
            }
        } catch (Exception e) {
            Log.v("INFO", "page1 = " + current_page);
            e.printStackTrace();
        }
    }

    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if (keyCode == 4) {
            Log.v("INFO", "KEYCODE_BACK");
            startActivityForResult(new Intent(this, RecordLocation.class), 3000);
            Log.v("INFO", "RecordLocation");
            return true;
        } else if (keyCode != 3) {
            return true;
        } else {
            Log.v("INFO", "KEYCODE_HOME");
            return true;
        }
    }

    /* access modifiers changed from: protected */
    public void onUserLeaveHint() {
        super.onUserLeaveHint();
        Log.v("INFO", "onUserLeaveHint");
    }

    /* access modifiers changed from: protected */
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        Log.v("INFO", "onActivityResult");
        if (requestCode == 199) {
            from_settings = 1;
        }
        if (requestCode == 2000) {
            Log.v("INFO", "requestCode==2000");
            SharedPreferences prefs2 = getApplicationContext().getSharedPreferences(SHARED_PREFERENCES_NAME, 1);
            double d = (double) prefs2.getFloat(RecordLocation.LATITUDE, 0.0f);
            double d2 = (double) prefs2.getFloat(RecordLocation.LONGITUDE, 0.0f);
        }
        if (requestCode == 1000 && data != null) {
            Log.v("TAG", "finish ");
            finish();
        }
        if (requestCode == 3000) {
            SharedPreferences prefs22 = getApplicationContext().getSharedPreferences(SHARED_PREFERENCES_NAME, 1);
            double d3 = (double) prefs22.getFloat(RecordLocation.LATITUDE, 0.0f);
            double d4 = (double) prefs22.getFloat(RecordLocation.LONGITUDE, 0.0f);
            Log.v("TAG", "finish1 ");
            finish();
        }
    }

    private void checkFirstRun() {
        new Time(Time.getCurrentTimezone()).setToNow();
    }

    public static String getDate(long milliSeconds, String dateFormat) {
        SimpleDateFormat formatter = new SimpleDateFormat(dateFormat);
        Calendar calendar = Calendar.getInstance();
        calendar.setTimeInMillis(milliSeconds);
        return formatter.format(calendar.getTime());
    }

    public void onAnimationStart(Animation animation2) {
    }

    public void onAnimationEnd(Animation animation2) {
        menuOut = !menuOut;
        if (!menuOut) {
            backLayout_operations.setVisibility(4);
            mask_ll.setVisibility(8);
            isInbackground = false;
        }
        layoutApp(menuOut);
        if (isMenuSelected) {
            isMenuSelected = false;
            if (selectedPositionFromMenu == 0) {
                menuListView.setAdapter(new MenuListViewAdapter(this, 0));
                Intent intent = new Intent(this, DeviceScanActivity.class);
                intent.setFlags(DriveFile.MODE_READ_ONLY);
                intent.setFlags(67108864);
                startActivity(intent);
            } else if (selectedPositionFromMenu == 1) {
                menuListView.setAdapter(new MenuListViewAdapter(this, 1));
                //Intent intent2 = new Intent(this, AddAlbumFragment1.class);
                //intent2.setFlags(DriveFile.MODE_READ_ONLY);
                //intent2.setFlags(67108864);
                //startActivity(intent2);
            } else if (selectedPositionFromMenu == 2) {
                menuListView.setAdapter(new MenuListViewAdapter(this, 2));
                Intent intent3 = new Intent(this, WebPageActivity.class);
                intent3.setFlags(DriveFile.MODE_READ_ONLY);
                intent3.setFlags(67108864);
                intent3.putExtra("PageUrl", "CustomerService");
                startActivity(intent3);
            } else if (selectedPositionFromMenu == 3) {
                menuListView.setAdapter(new MenuListViewAdapter(this, 3));
                Intent intent4 = new Intent(this, WebPageActivity.class);
                intent4.setFlags(DriveFile.MODE_READ_ONLY);
                intent4.setFlags(67108864);
                intent4.putExtra("PageUrl", "About");
                startActivity(intent4);
            } else if (selectedPositionFromMenu == 4) {
                menuListView.setAdapter(new MenuListViewAdapter(this, 4));
                Intent intent5 = new Intent(this, WebPageActivity.class);
                intent5.setFlags(DriveFile.MODE_READ_ONLY);
                intent5.setFlags(67108864);
                intent5.putExtra("PageUrl", "StoreLocator");
                startActivity(intent5);
            } else if (selectedPositionFromMenu == 5) {
                menuListView.setAdapter(new MenuListViewAdapter(this, 5));
                Intent intent6 = new Intent(this, WebPageActivity.class);
                intent6.setFlags(DriveFile.MODE_READ_ONLY);
                intent6.setFlags(67108864);
                intent6.putExtra("PageUrl", "Shop");
                startActivity(intent6);
            } else if (selectedPositionFromMenu == 10) {
                Intent intent7 = new Intent(this, WebActivity.class);
                intent7.setFlags(DriveFile.MODE_READ_ONLY);
                intent7.setFlags(67108864);
                startActivity(intent7);
            }
        }
    }

    public void onAnimationRepeat(Animation animation2) {
    }

    public class SectionsPagerAdapter extends FragmentPagerAdapter {
        public SectionsPagerAdapter(FragmentManager fm) {
            super(fm);
            DeviceControlActivity_Above.mFragmentTags = new HashMap();
        }

        public Fragment getItem(int position) {
            Fragment myFragment = new DummySectionFragment().newInstance(position);
            DeviceControlActivity_Above.mFragmentTags.put(Integer.valueOf(position), myFragment);
            return myFragment;
        }

        public int getCount() {
            return 1;
        }

        public Fragment getFragment(int position) {
            return DeviceControlActivity_Above.mFragmentTags.get(Integer.valueOf(position));
        }
    }

    public static class DummySectionFragment extends Fragment implements Updateable {
        public static final String ARG_SECTION_NUMBER = "section_number";

        public Fragment newInstance(int position) {
            DummySectionFragment fragmentFirst = new DummySectionFragment();
            Bundle args = new Bundle();
            args.putInt(ARG_SECTION_NUMBER, position + 1);
            fragmentFirst.setArguments(args);
            return fragmentFirst;
        }

        public void onActivityCreated(Bundle savedInstanceState) {
            super.onActivityCreated(savedInstanceState);
            FragmentManager fm = getChildFragmentManager();
            DeviceControlActivity_Above.fragment = (SupportMapFragment) fm.findFragmentById(R.id.map);
            if (DeviceControlActivity_Above.fragment == null) {
                DeviceControlActivity_Above.fragment = SupportMapFragment.newInstance();
                fm.beginTransaction().replace(R.id.map, DeviceControlActivity_Above.fragment).commit();
            }
        }

        public void onResume() {
            super.onResume();
            DeviceControlActivity_Above.fragment.getMapAsync(new OnMapReadyCallback() {
                @Override
                public void onMapReady(GoogleMap googleMap) {
                    DeviceControlActivity_Above.mMap = googleMap;
                }
            });
            DeviceControlActivity_Above.menuListView.setAdapter(new MenuListViewAdapter(DeviceControlActivity_Above.context, 0));
        }

        public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
            View rootView = inflater.inflate(R.layout.gatt_services_characteristics_for_above, container, false);
            int connectedval = getArguments().getInt(ARG_SECTION_NUMBER);
            getActivity().getWindowManager().getDefaultDisplay().getMetrics(new DisplayMetrics());
            DeviceControlActivity_Above.context = getActivity();
            try {
                DeviceControlActivity_Above.current_page = Integer.parseInt(getActivity().getIntent().getExtras().get("PAGE").toString());
            } catch (Exception e) {
            }
            Log.v("TAG", "current_page 1 = " + DeviceControlActivity_Above.current_page);
            if (getActivity().getIntent().getExtras().getBoolean("Alert")) {
                new CustomDialogClass(DeviceControlActivity_Above.context).show();
            }
            DeviceControlActivity_Above.backLayout_operations = (LinearLayout) rootView.findViewById(R.id.backLayout_operations);
            DeviceControlActivity_Above.mask_ll = (LinearLayout) rootView.findViewById(R.id.mask_ll);
            DeviceControlActivity_Above.frontLayout_operations = (FrameLayout) rootView.findViewById(R.id.frontLayout_operations);
            DeviceControlActivity_Above.menuListView = (ListView) rootView.findViewById(R.id.menuListView);
            DeviceControlActivity_Above.menuListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                public void onItemClick(AdapterView<?> adapterView, View arg1, int position, long arg3) {
                    DeviceControlActivity_Above.isMenuSelected = true;
                    DeviceControlActivity_Above.selectedPositionFromMenu = position;
                    DeviceControlActivity_Above.startAnimation();
                }
            });
            DeviceControlActivity_Above.mask_ll.setOnClickListener(new View.OnClickListener() {
                public void onClick(View v) {
                    DeviceControlActivity_Above.startAnimation();
                }
            });
            DeviceControlActivity_Above.privacy = (TextView) rootView.findViewById(R.id.privacypolicy);
            DeviceControlActivity_Above.privacy.setText(Html.fromHtml("<u>Privacy Policy</u>"));
            DeviceControlActivity_Above.privacy.setOnClickListener(new View.OnClickListener() {
                public void onClick(View v) {
                    DeviceControlActivity_Above.isMenuSelected = true;
                    DeviceControlActivity_Above.selectedPositionFromMenu = 10;
                    DeviceControlActivity_Above.startAnimation();
                }
            });
            String versionName = null;
            try {
                versionName = DeviceControlActivity_Above.context.getPackageManager().getPackageInfo(DeviceControlActivity_Above.context.getPackageName(), 0).versionName;
            } catch (PackageManager.NameNotFoundException e2) {
                e2.printStackTrace();
            }
            DeviceControlActivity_Above.version = (TextView) rootView.findViewById(R.id.versionnumber);
            DeviceControlActivity_Above.version.setText("v " + versionName);
            DeviceControlActivity_Above.header_img_ll = (LinearLayout) rootView.findViewById(R.id.header_img_ll);
            DeviceControlActivity_Above.header_img_ll.setVisibility(0);
            DeviceControlActivity_Above.header = (TextView) rootView.findViewById(R.id.header);
            DeviceControlActivity_Above.header.setVisibility(8);
            DeviceControlActivity_Above.header_img = (ImageView) rootView.findViewById(R.id.header_img);
            DeviceControlActivity_Above.header_img.setBackgroundResource(R.drawable.power);
            DeviceControlActivity_Above.back = (LinearLayout) rootView.findViewById(R.id.back);
            DeviceControlActivity_Above.back.setVisibility(8);
            DeviceControlActivity_Above.menu = (LinearLayout) rootView.findViewById(R.id.menu);
            DeviceControlActivity_Above.menu.setVisibility(0);
            DeviceControlActivity_Above.settings_img = (ImageView) rootView.findViewById(R.id.settings_img);
            DeviceControlActivity_Above.settings_img.setVisibility(0);
            DeviceControlActivity_Above.settings = (TextView) rootView.findViewById(R.id.settings);
            DeviceControlActivity_Above.settings.setVisibility(8);
            DeviceControlActivity_Above.settings_img.setOnClickListener(new View.OnClickListener() {
                public void onClick(View v) {
                    if (DeviceControlActivity_Above.connectBool) {
                        Intent i = new Intent(DummySectionFragment.this.getActivity(), Setting.class);
                        i.setFlags(DriveFile.MODE_READ_ONLY);
                        i.setFlags(67108864);
                        i.putExtra("fromscan", 0);
                        DummySectionFragment.this.startActivity(i);
                        Log.v("TAG", "finish 3");
                    }
                }
            });
            DeviceControlActivity_Above.help = (ImageView) rootView.findViewById(R.id.help);
            DeviceControlActivity_Above.help.setOnClickListener(new View.OnClickListener() {
                public void onClick(View v) {
                    Intent intent1 = new Intent(DeviceControlActivity_Above.context, HelpScreen.class);
                    intent1.putExtra("activity", "1");
                    DummySectionFragment.this.startActivity(intent1);
                }
            });
            DeviceControlActivity_Above.menu.setOnClickListener(new View.OnClickListener() {
                public void onClick(View v) {
                    DeviceControlActivity_Above.startAnimation();
                }
            });
            DeviceControlActivity_Above.battery = (TextView) rootView.findViewById(R.id.batterylevel);
            DeviceControlActivity_Above.imhr = (TextView) rootView.findViewById(R.id.imhr);
            DeviceControlActivity_Above.immin = (TextView) rootView.findViewById(R.id.immin);
            DeviceControlActivity_Above.charge_txt = (TextView) rootView.findViewById(R.id.charge_txt);
            DeviceControlActivity_Above.port1 = (ToggleButton) rootView.findViewById(R.id.port1);
            DeviceControlActivity_Above.port2 = (ToggleButton) rootView.findViewById(R.id.port2);
            DeviceControlActivity_Above.findme = (TextView) rootView.findViewById(R.id.findme);
            DeviceControlActivity_Above.findme.setBackgroundColor(0);
            DeviceControlActivity_Above.hrs = (TextView) rootView.findViewById(R.id.hrs);
            DeviceControlActivity_Above.min = (TextView) rootView.findViewById(R.id.min);
            DeviceControlActivity_Above.info = (LinearLayout) rootView.findViewById(R.id.info);
            DeviceControlActivity_Above.devname = (TextView) rootView.findViewById(R.id.devname);
            DeviceControlActivity_Above.connect = (TextView) rootView.findViewById(R.id.connectagain);
            DeviceControlActivity_Above.anim_timer = (ImageView) rootView.findViewById(R.id.animtimer);
            DeviceControlActivity_Above.connect.setTypeface(Typeface.createFromAsset(getActivity().getAssets(), "fonts/proximanova_regular.otf"));
            DeviceControlActivity_Above.animation = new AnimationDrawable();
            DeviceControlActivity_Above.animation.addFrame(getResources().getDrawable(R.drawable.discharging1), 1000);
            DeviceControlActivity_Above.animation.addFrame(getResources().getDrawable(R.drawable.discharging2), 1000);
            DeviceControlActivity_Above.animation.addFrame(getResources().getDrawable(R.drawable.discharging3), 1000);
            DeviceControlActivity_Above.animation.setOneShot(false);
            DeviceControlActivity_Above.animation_green = new AnimationDrawable();
            DeviceControlActivity_Above.animation_green.addFrame(getResources().getDrawable(R.drawable.charging1), 1000);
            DeviceControlActivity_Above.animation_green.addFrame(getResources().getDrawable(R.drawable.charging2), 1000);
            DeviceControlActivity_Above.animation_green.addFrame(getResources().getDrawable(R.drawable.charging3), 1000);
            DeviceControlActivity_Above.animation_green.setOneShot(false);
            DeviceControlActivity_Above.anim_timer.setBackgroundDrawable(DeviceControlActivity_Above.animation);
            DeviceControlActivity_Above.animation.start();
            DeviceControlActivity_Above.devname.setText(DeviceScanActivity.session.loadSavedPreferences("DEVICENAME_" + connectedval));
            DeviceControlActivity_Above.connect.setOnClickListener(new View.OnClickListener() {
                public void onClick(View v) {
                    Log.v("TAG", "finish 2");
                    DummySectionFragment.this.getActivity().finish();
                }
            });
            DeviceControlActivity_Above.info.bringToFront();
            DeviceControlActivity_Above.info.setOnClickListener(new View.OnClickListener() {
                public void onClick(View v) {
                    final Dialog dialog = new Dialog(DeviceControlActivity_Above.context);
                    dialog.requestWindowFeature(1);
                    dialog.setContentView(R.layout.usage);
                    dialog.setTitle("USAGE INFO");
                    TextView text = (TextView) dialog.findViewById(R.id.chargecycles);
                    text.setText(DeviceControlActivity_Above.lowbyte);
                    text.setTextColor(Color.parseColor("#ba0c2f"));
                    TextView dateval = (TextView) dialog.findViewById(R.id.date);
                    dateval.setTextColor(Color.parseColor("#ba0c2f"));
                    TextView totaltimeinuse = (TextView) dialog.findViewById(R.id.totaltime);
                    totaltimeinuse.setTextColor(Color.parseColor("#ba0c2f"));
                    ((RelativeLayout) dialog.findViewById(R.id.parent_rl)).setBackgroundDrawable(new ColorDrawable(0));
                    dialog.getWindow().setBackgroundDrawable(new ColorDrawable(0));
                    Typeface gotham_gold = Typeface.createFromAsset(DeviceControlActivity_Above.context.getAssets(), "fonts/gotham_bold.otf");
                    ((TextView) dialog.findViewById(R.id.title)).setTypeface(gotham_gold);
                    ((TextView) dialog.findViewById(R.id.total_time_use_txt)).setTypeface(gotham_gold);
                    ((TextView) dialog.findViewById(R.id.charge_cycles_txt)).setTypeface(gotham_gold);
                    ((TextView) dialog.findViewById(R.id.date_registered_txt)).setTypeface(gotham_gold);
                    ((ImageView) dialog.findViewById(R.id.close)).setOnClickListener(new View.OnClickListener() {
                        public void onClick(View v) {
                            dialog.dismiss();
                        }
                    });
                    ApplicationInfo appInfo = null;
                    try {
                        appInfo = DeviceControlActivity_Above.context.getPackageManager().getApplicationInfo(BuildConfig.APPLICATION_ID, 0);
                    } catch (PackageManager.NameNotFoundException e) {
                        e.printStackTrace();
                    }
                    long installed = new File(appInfo.sourceDir).lastModified();
                    dateval.setText(DeviceControlActivity_Above.getDate(installed, "MM/dd/yyyy"));
                    long diff = new Date().getTime() - installed;
                    long j = (diff / 1000) % 60;
                    totaltimeinuse.setText((((int) diff) / 86400000) + "d  " + (diff / 3600000) + "h  " + ((diff / 60000) % 60) + "m");
                    dialog.show();
                }
            });
            DeviceControlActivity_Above.mHoloCircularProgressBar = (HoloCircularProgressBar) rootView.findViewById(R.id.holoCircularProgressBar1);
            DeviceControlActivity_Above.batterylevel_iv = (ImageView) rootView.findViewById(R.id.batterylevel_iv);
            DeviceControlActivity_Above.charge_txt = (TextView) rootView.findViewById(R.id.charge_txt);
            DeviceControlActivity_Above.animate(DeviceControlActivity_Above.mHoloCircularProgressBar, (Animator.AnimatorListener) null, 0.0f, 1000);
            DeviceControlActivity_Above.mHoloCircularProgressBar.setProgressColor(Color.parseColor("#b70e30"));
            new RotateAnimation(0.0f, 90.0f, 250.0f, 273.0f).setFillAfter(true);
            DeviceControlActivity_Above.bottom_ll = (RelativeLayout) rootView.findViewById(R.id.bottom_ll);
            DeviceControlActivity_Above.content = (ViewGroup) rootView.findViewById(R.id.content);
            DeviceControlActivity_Above.find_me_ll = (LinearLayout) rootView.findViewById(R.id.find_me_ll);
            DeviceControlActivity_Above.find_ll = (LinearLayout) rootView.findViewById(R.id.find_ll);
            DeviceControlActivity_Above.arrow_img = (ImageView) rootView.findViewById(R.id.arrow_img);
            DeviceControlActivity_Above.bottom_ll.getViewTreeObserver().addOnGlobalLayoutListener(new ViewTreeObserver.OnGlobalLayoutListener() {
                public void onGlobalLayout() {
                    DeviceControlActivity_Above.screenHeight = DeviceControlActivity_Above.bottom_ll.getHeight();
                    Log.v("TAG", "drawer.getHeight() = " + DeviceControlActivity_Above.screenHeight);
                    DeviceControlActivity_Above.bottom_ll.getViewTreeObserver().removeGlobalOnLayoutListener(this);
                }
            });
            DeviceControlActivity_Above.find_ll.setOnClickListener(new View.OnClickListener() {
                public void onClick(View v) {
                    DeviceControlActivity_Above.slideUpDown(DummySectionFragment.this.getActivity());
                }
            });
            DeviceControlActivity_Above.find_me_ll.setOnClickListener(new View.OnClickListener() {
                public void onClick(View arg0) {
                    if (DeviceControlActivity_Above.hascharac == 1) {
                        try {
                            DeviceControlActivity_Above.mBluetoothLeService.writeCharacteristic(new byte[]{9, Ascii.FF, 34, -1, -1, -1, 104, -122, 54}, 1);
                        } catch (Exception e) {
                        }
                    }
                }
            });
            if (DeviceControlActivity_Above.port1checked) {
                DeviceControlActivity_Above.port1.setChecked(true);
            } else {
                DeviceControlActivity_Above.port1.setChecked(false);
            }
            if (DeviceControlActivity_Above.port2checked) {
                DeviceControlActivity_Above.port2.setChecked(true);
            } else {
                DeviceControlActivity_Above.port2.setChecked(false);
            }
            DeviceControlActivity_Above.port1.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
                public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                    DeviceControlActivity_Above.port1checked = isChecked;
                    if (isChecked && DeviceControlActivity_Above.hascharac == 1) {
                        DeviceControlActivity_Above.port1checked = isChecked;
                        DeviceScanActivity.session.savePreferences("SAVEDBATTERY", "0");
                        Log.v("INFO", "if isChecked = " + isChecked);
                        final byte[] val = {9, 6, 34, 79, 78, -1, 49, 48, -46};
                        new Handler().postDelayed(new Runnable() {
                            public void run() {
                                DeviceControlActivity_Above.mBluetoothLeService.writeCharacteristic(val, 2);
                            }
                        }, 0);
                    } else if (!isChecked && DeviceControlActivity_Above.hascharac == 1) {
                        Log.v("INFO", "else isChecked = " + isChecked);
                        final byte[] val2 = {9, 4, 34, 79, 70, 70, 49, 48, 97};
                        new Handler().postDelayed(new Runnable() {
                            public void run() {
                                DeviceControlActivity_Above.mBluetoothLeService.writeCharacteristic(val2, 2);
                            }
                        }, 0);
                    }
                }
            });
            DeviceControlActivity_Above.port2.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
                public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                    DeviceControlActivity_Above.port2checked = isChecked;
                    DeviceScanActivity.session.savePreferences("SAVEDBATTERY", "0");
                    if (isChecked && DeviceControlActivity_Above.hascharac == 1) {
                        final byte[] val = {9, 5, 34, 79, 78, -1, 50, 52, -42};
                        new Handler().postDelayed(new Runnable() {
                            public void run() {
                                DeviceControlActivity_Above.mBluetoothLeService.writeCharacteristic(val, 1);
                            }
                        }, 0);
                    } else if (!isChecked && DeviceControlActivity_Above.hascharac == 1) {
                        final byte[] val2 = {9, 7, 34, 79, 70, 70, 50, 52, 101};
                        new Handler().postDelayed(new Runnable() {
                            public void run() {
                                DeviceControlActivity_Above.mBluetoothLeService.writeCharacteristic(val2, 1);
                            }
                        }, 0);
                    }
                }
            });
            return rootView;
        }

        public void onPause() {
            super.onPause();
            if (DeviceControlActivity_Above.menuOut) {
                DeviceControlActivity_Above.startAnimation();
            }
        }

        public void update() {
            Toast.makeText(getActivity(), "Update!" + getArguments().getInt(ARG_SECTION_NUMBER), 1).show();
            DeviceControlActivity_Above.hrs = (TextView) getView().findViewById(R.id.hrs);
            DeviceControlActivity_Above.min = (TextView) getView().findViewById(R.id.min);
        }
    }

    protected static void slideUpDown(Context fragmentActivity) {
        if (!isOpen) {
            Log.v("TAG", "getTop = " + (bottom_ll.getTop() - ((screenHeight * 25) / 100)));
            Log.v("TAG", "getBottom = " + (bottom_ll.getBottom() - ((screenHeight * 25) / 100)));
            Log.v("TAG", "(screenHeight * 25/100) = " + ((screenHeight * 25) / 100));
            bottom_ll.layout(bottom_ll.getLeft(), bottom_ll.getTop() - ((screenHeight * 25) / 100), bottom_ll.getRight(), bottom_ll.getBottom() - ((screenHeight * 25) / 100));
            content.setVisibility(0);
            bottom_ll.startAnimation(AnimationUtils.loadAnimation(fragmentActivity, R.anim.bottom_up));
            isOpen = true;
            arrow_img.setBackgroundResource(R.drawable.arrow_down_icon);
            return;
        }
        isOpen = false;
        Animation bottomDown = AnimationUtils.loadAnimation(fragmentActivity, R.anim.top_down);
        AnimationUtils.loadAnimation(fragmentActivity, R.anim.top_down);
        bottomDown.setAnimationListener(new Animation.AnimationListener() {
            public void onAnimationStart(Animation arg0) {
            }

            public void onAnimationRepeat(Animation arg0) {
            }

            public void onAnimationEnd(Animation arg0) {
                Log.v("TAG", "getTop = " + (DeviceControlActivity_Above.bottom_ll.getTop() + ((DeviceControlActivity_Above.screenHeight * 25) / 100)));
                Log.v("TAG", "getBottom = " + (DeviceControlActivity_Above.bottom_ll.getBottom() + ((DeviceControlActivity_Above.screenHeight * 25) / 100)));
                DeviceControlActivity_Above.content.setVisibility(8);
                DeviceControlActivity_Above.arrow_img.setBackgroundResource(R.drawable.arrowup_icon);
                DeviceControlActivity_Above.bottom_ll.layout(DeviceControlActivity_Above.bottom_ll.getLeft(), DeviceControlActivity_Above.bottom_ll.getTop() + ((DeviceControlActivity_Above.screenHeight * 25) / 100), DeviceControlActivity_Above.bottom_ll.getRight(), DeviceControlActivity_Above.bottom_ll.getBottom() + ((DeviceControlActivity_Above.screenHeight * 25) / 100));
            }
        });
        bottom_ll.startAnimation(bottomDown);
    }

    private void handleNewLocation(double lat2, double lng2) {
        LatLng latLng = new LatLng(lat2, lng2);
        Log.v("TEST", "currentLatitude = " + lat2);
        Log.v("TEST", "currentLongitude = " + lng2);
        MarkerOptions options = new MarkerOptions().position(latLng).draggable(false);
        mMap.clear();
        mMap.addMarker(options);
        mMap.moveCamera(CameraUpdateFactory.newLatLng(latLng));
        mMap.animateCamera(CameraUpdateFactory.zoomTo(13.0f));
    }

    private void buildAlertMessageNoGps() {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setMessage("Your GPS seems to be disabled, do you want to enable it?").setCancelable(false).setPositiveButton("Yes", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
                DeviceControlActivity_Above.this.startActivity(new Intent("android.settings.LOCATION_SOURCE_SETTINGS"));
            }
        }).setNegativeButton("No", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
                dialog.cancel();
            }
        });
        builder.create().show();
    }

    public void onConnectionFailed(ConnectionResult connectionResult) {
        if (connectionResult.hasResolution()) {
            try {
                connectionResult.startResolutionForResult(this, CONNECTION_FAILURE_RESOLUTION_REQUEST);
            } catch (IntentSender.SendIntentException e) {
                e.printStackTrace();
            }
        } else {
            Log.v("TAG", "Location services connection failed with code " + connectionResult.getErrorCode());
        }
    }

    public void onConnected(Bundle arg0) {
        if (ActivityCompat.checkSelfPermission(this, "android.permission.ACCESS_FINE_LOCATION") == 0 || ActivityCompat.checkSelfPermission(this, "android.permission.ACCESS_COARSE_LOCATION") == 0) {
            this.location = LocationServices.FusedLocationApi.getLastLocation(mGoogleApiClient);
            if (this.location == null) {
                Log.v("TEST", "onConnected if");
                LocationServices.FusedLocationApi.requestLocationUpdates(mGoogleApiClient, mLocationRequest, (LocationListener) this);
                return;
            }
            Log.v("TEST", "onConnected else");
            if (this.lat == 0.0d && this.lng == 0.0d) {
                handleNewLocation(this.location.getLatitude(), this.location.getLongitude());
                DeviceScanActivity.session.setLastLocation(RecordLocation.LATITUDE, this.location.getLatitude(), RecordLocation.LONGITUDE, this.location.getLongitude());
                return;
            }
            handleNewLocation(this.lat, this.lng);
        }
    }

    public void onConnectionSuspended(int arg0) {
    }

    public void onLocationChanged(Location location2) {
        Log.v("TEST", "onLocationChanged");
        if (this.lat == 0.0d && this.lng == 0.0d) {
            handleNewLocation(location2.getLatitude(), location2.getLongitude());
            DeviceScanActivity.session.setLastLocation(RecordLocation.LATITUDE, location2.getLatitude(), RecordLocation.LONGITUDE, location2.getLongitude());
            return;
        }
        handleNewLocation(this.lat, this.lng);
    }

    /* access modifiers changed from: package-private */
    public void threshold_alert() {
        if (this.isPause) {
            Intent intent = new Intent(this, DeviceControlActivity_Above.class);
            intent.setFlags(DriveFile.MODE_READ_ONLY);
            intent.setFlags(67108864);
            Notification noti = new Notification.Builder(this).setContentTitle("Reserve Threshold Met").setSmallIcon(R.drawable.ic_launcher).setContentIntent(PendingIntent.getActivity(this, (int) System.currentTimeMillis(), intent, 0)).setStyle(new Notification.BigTextStyle().bigText("Your Reserve Threshold value has been met. Do you wish to continue charging instead?")).build();
            noti.flags |= 16;
            ((NotificationManager) getSystemService("notification")).notify(0, noti);
            return;
        }
        Log.v("INFO", "Reserve Threshold Met 1");
        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(this);
        alertDialogBuilder.setTitle("Reserve Threshold Met");
        alertDialogBuilder.setMessage("Your Reserve Threshold value has been met. Do you wish to continue charging instead?").setCancelable(false).setPositiveButton("NO", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
                final byte[] val = {9, 4, 34, 79, 70, 70, 49, 48, 97};
                new Handler().postDelayed(new Runnable() {
                    public void run() {
                        DeviceControlActivity_Above.mBluetoothLeService.writeCharacteristic(val, 2);
                    }
                }, 1000);
                final byte[] val2 = {9, 7, 34, 79, 70, 70, 50, 52, 101};
                new Handler().postDelayed(new Runnable() {
                    public void run() {
                        DeviceControlActivity_Above.mBluetoothLeService.writeCharacteristic(val2, 1);
                    }
                }, 2000);
                DeviceControlActivity_Above.port1.setChecked(false);
                DeviceControlActivity_Above.port2.setChecked(false);
                DeviceControlActivity_Above.port1checked = false;
                DeviceControlActivity_Above.port2checked = false;
                DeviceScanActivity.session.savePreferences("SAVEDBATTERY", DeviceControlActivity_Above.chargedata);
            }
        }).setNegativeButton("YES", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
            }
        });
        AlertDialog alertDialog2 = alertDialogBuilder.create();
        try {
            if (!isShowing) {
                isShowing = true;
                alertDialog2.show();
            }
        } catch (Exception e) {
        }
    }

    /* access modifiers changed from: package-private */
    public void create_alert() {
        if (this.isPause) {
            Intent intent = new Intent(this, DeviceControlActivity_Above.class);
            intent.setFlags(DriveFile.MODE_READ_ONLY);
            intent.setFlags(67108864);
            Notification noti = new Notification.Builder(this).setContentTitle("TUMI BLE Connection").setSmallIcon(R.drawable.ic_launcher).setContentIntent(PendingIntent.getActivity(this, (int) System.currentTimeMillis(), intent, 0)).setStyle(new Notification.BigTextStyle().bigText("Your TUMI Smart Battery Bank is out of Bluetooth range and will be unavailable for control. Locate the Last Known Location of your bank and re-establish the connection.")).build();
            noti.flags |= 16;
            ((NotificationManager) getSystemService("notification")).notify(0, noti);
            return;
        }
        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(this);
        alertDialogBuilder.setTitle("Alert");
        alertDialogBuilder.setMessage("Your TUMI Smart Battery Bank is out of Bluetooth range and will be unavailable for control. Locate the Last Known Location of your bank and re-establish the connection.").setCancelable(false).setPositiveButton("Ok", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog1, int id) {
                Log.v("TAG", "finish 4");
                DeviceControlActivity_Above.this.finish();
            }
        }).setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog1, int which) {
                dialog1.dismiss();
                if (DeviceControlActivity_Above.content.getVisibility() == 8) {
                    DeviceControlActivity_Above.slideUpDown(DeviceControlActivity_Above.this);
                }
            }
        });
        try {
            alertDialogBuilder.create().show();
        } catch (Exception e) {
        }
    }

    public static void startAnimation() {
        Animation anim;
        int w = frontLayout_operations.getMeasuredWidth();
        int h = frontLayout_operations.getMeasuredHeight();
        int left = (int) (((double) frontLayout_operations.getMeasuredWidth()) * 0.5d);
        if (!menuOut) {
            anim = new TranslateAnimation(0.0f, (float) left, 0.0f, 0.0f);
            backLayout_operations.setVisibility(0);
            mask_ll.setVisibility(0);
            isInbackground = true;
            animParams.init(left, 0, left + w, h);
            anim.setDuration(300);
        } else {
            anim = new TranslateAnimation(0.0f, (float) (-left), 0.0f, 0.0f);
            animParams.init(0, 0, w, h);
            anim.setDuration(10);
        }
        anim.setAnimationListener((Animation.AnimationListener) context);
        anim.setFillAfter(true);
        frontLayout_operations.startAnimation(anim);
        Log.v("TAG", "startAnimation" + menuOut);
    }

    /* access modifiers changed from: package-private */
    public void layoutApp(boolean menuOut2) {
        frontLayout_operations.layout(animParams.left, animParams.top, animParams.right, animParams.bottom);
        frontLayout_operations.clearAnimation();
    }

    public static class CustomDialogClass extends Dialog implements View.OnClickListener {
        /* access modifiers changed from: private */
        public Context c;
        private Dialog d;
        /* access modifiers changed from: private */
        public EditText deviceName_et;
        private Button no;
        private TextView title;
        private Button yes;

        public CustomDialogClass(Context a) {
            super(a);
            this.c = a;
        }

        /* access modifiers changed from: protected */
        public void onCreate(Bundle savedInstanceState) {
            super.onCreate(savedInstanceState);
            requestWindowFeature(1);
            getWindow().setBackgroundDrawable(new ColorDrawable(0));
            setContentView(R.layout.custom_dialog);
            this.title = (TextView) findViewById(R.id.title);
            this.yes = (Button) findViewById(R.id.dialogButtonOK);
            this.no = (Button) findViewById(R.id.dialogButtonCancel);
            this.deviceName_et = (EditText) findViewById(R.id.playlist_name);
            this.deviceName_et.setHint("Enter battery name");
            this.deviceName_et.setText(DeviceControlActivity_Above.mDeviceName);
            this.yes.setOnClickListener(this);
            this.no.setOnClickListener(this);
            this.title.setText(DeviceControlActivity_Above.context.getResources().getString(R.string.text4));
            this.deviceName_et.setFilters(new InputFilter[]{new InputFilter() {
                public CharSequence filter(CharSequence src, int start, int end, Spanned dst, int dstart, int dend) {
                    return (!src.equals("") && !src.toString().matches("[\\x00-\\x7F]+")) ? "" : src;
                }
            }});
            this.deviceName_et.setFilters(new InputFilter[]{new InputFilter.LengthFilter(30)});
            this.deviceName_et.setOnClickListener(new View.OnClickListener() {
                public void onClick(View v) {
                    if (v.getId() == CustomDialogClass.this.deviceName_et.getId()) {
                        CustomDialogClass.this.deviceName_et.setCursorVisible(true);
                    }
                }
            });
            this.deviceName_et.setOnEditorActionListener(new TextView.OnEditorActionListener() {
                public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                    CustomDialogClass.this.deviceName_et.setCursorVisible(false);
                    if (event != null && event.getKeyCode() == 66) {
                        ((InputMethodManager) CustomDialogClass.this.c.getSystemService("input_method")).hideSoftInputFromWindow(CustomDialogClass.this.deviceName_et.getApplicationWindowToken(), 2);
                    }
                    return false;
                }
            });
        }

        public void onClick(View v) {
            switch (v.getId()) {
                case R.id.dialogButtonCancel /*2131558555*/:
                    dismiss();
                    return;
                case R.id.dialogButtonOK /*2131558556*/:
                    if (this.deviceName_et.getText().toString().trim().length() > 0) {
                        String deviceUUId = DeviceScanActivity.session.loadSavedPreferences("DEVICE_" + DeviceControlActivity_Above.current_page);
                        DeviceScanActivity.session.savePreferences("DEVICENAME_" + DeviceControlActivity_Above.current_page, this.deviceName_et.getText().toString().trim());
                        DeviceScanActivity.session.savePreferences("DEVICENAME_" + deviceUUId, this.deviceName_et.getText().toString().trim());
                        DeviceControlActivity_Above.EXTRAS_DEVICE_NAME = this.deviceName_et.getText().toString().trim();
                        dismiss();
                        if (!DeviceControlActivity_Above.mDeviceName.equals(this.deviceName_et.getText().toString().trim())) {
                            Toast.makeText(this.c, "Successfully device name changed.", 1).show();
                            return;
                        }
                        return;
                    }
                    Toast.makeText(this.c, "Device name should not be empty.", 0).show();
                    return;
                default:
                    return;
            }
        }
    }
}
