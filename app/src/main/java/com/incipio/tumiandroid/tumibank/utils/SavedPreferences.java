package com.incipio.tumiandroid.tumibank.utils;

import android.content.Context;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;
import java.util.Set;

public class SavedPreferences {
    static Context _context;
    static SharedPreferences sharedPreferences;
    String devicename;
    SharedPreferences.Editor editor;

    public SavedPreferences(Context c) {
        _context = c;
        sharedPreferences = PreferenceManager.getDefaultSharedPreferences(_context);
    }

    public synchronized void delPreferences(String val1, String val2, String val3, String val4, String val5, String val6) {
        this.editor = sharedPreferences.edit();
        System.out.println("removing......" + val1 + "..." + val2 + ".." + val3);
        this.editor.remove(val1);
        this.editor.remove(val2);
        this.editor.remove(val3);
        this.editor.remove(val4);
        this.editor.remove(val5);
        this.editor.remove(val6);
        this.editor.apply();
    }

    public synchronized void savePreferences(String key, String value) {
        this.editor = sharedPreferences.edit();
        this.editor.putString(key, value);
        this.editor.apply();
    }

    public synchronized void savePos(String key, String value) {
        this.editor = sharedPreferences.edit();
        this.editor.putString(key, value);
        this.editor.apply();
    }

    public synchronized void savedevicenames(String key, Set<String> value) {
        this.editor = sharedPreferences.edit();
        this.editor.putStringSet(key, value);
        this.editor.apply();
    }

    public synchronized void savedeviceuuids(String key, Set<String> value) {
        this.editor = sharedPreferences.edit();
        this.editor.putStringSet(key, value);
        this.editor.apply();
    }

    public synchronized void savedeviceaddress(String key, Set<String> value) {
        this.editor = sharedPreferences.edit();
        this.editor.putStringSet(key, value);
        this.editor.apply();
    }

    public synchronized void stopOffers(String key, Boolean value) {
        this.editor = sharedPreferences.edit();
        this.editor.putBoolean(key, value.booleanValue());
        this.editor.apply();
    }

    public synchronized void BLENotifications(String key, Boolean value) {
        this.editor = sharedPreferences.edit();
        this.editor.putBoolean(key, value.booleanValue());
        this.editor.apply();
    }

    public synchronized void setLastLocation(String key1, double currentLatitude, String key2, double currentLongitude) {
        this.editor = sharedPreferences.edit();
        this.editor.putLong(key1, Double.doubleToLongBits(currentLatitude));
        this.editor.putLong(key2, Double.doubleToLongBits(currentLongitude));
        this.editor.apply();
    }

    public synchronized void savePreferences2(String key1, String value1, String key2, String value2) {
        this.editor = sharedPreferences.edit();
        this.editor.putString(key1, value1);
        this.editor.putString(key2, value2);
        this.editor.apply();
    }

    public boolean getStopOffers(String key) {
        return sharedPreferences.getBoolean(key, false);
    }

    public boolean getBLENotifications(String key) {
        return sharedPreferences.getBoolean(key, true);
    }

    public String getLastLocation(String key1, String key2) {
        String lat = String.valueOf(Double.longBitsToDouble(sharedPreferences.getLong(key1, 0)));
        return lat + ":" + String.valueOf(Double.longBitsToDouble(sharedPreferences.getLong(key2, 0)));
    }

    public String loadSavedPreferences(String deviceUUID) {
        return sharedPreferences.getString(deviceUUID, this.devicename);
    }

    public String loadSavedPreferencesforbattery(String id) {
        return sharedPreferences.getString(id, "0");
    }

    public String loadSavedPreferences2(String value) {
        return sharedPreferences.getString(value, "false");
    }

    public String loadSavedPreferences3(String value) {
        return sharedPreferences.getString(value, "0");
    }

    public String loadSavedPreferencesnames(String value) {
        return sharedPreferences.getString(value, "0");
    }

    public String loadSavedPreferencescount(String deviceUUID) {
        return sharedPreferences.getString(deviceUUID, "0");
    }

    public String loadPosition(String pos) {
        return sharedPreferences.getString(pos, "0");
    }
}
