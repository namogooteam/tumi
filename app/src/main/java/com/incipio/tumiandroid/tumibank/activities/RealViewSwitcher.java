package com.incipio.tumiandroid.tumibank.activities;

import android.content.Context;
import android.util.AttributeSet;
import android.view.MotionEvent;
import android.view.VelocityTracker;
import android.view.View;
import android.view.ViewConfiguration;
import android.view.ViewGroup;
import android.widget.Scroller;

public class RealViewSwitcher extends ViewGroup {
    private static final int INVALID_SCREEN = -1;
    private static final int SNAP_VELOCITY = 1000;
    private static final int TOUCH_STATE_REST = 0;
    private static final int TOUCH_STATE_SCROLLING = 1;
    private int mCurrentScreen;
    private boolean mFirstLayout = true;
    private float mLastMotionX;
    private int mMaximumVelocity;
    private int mNextScreen = -1;
    private OnScreenSwitchListener mOnScreenSwitchListener;
    private Scroller mScroller;
    private int mTouchSlop;
    private int mTouchState = 0;
    private VelocityTracker mVelocityTracker;

    public interface OnScreenSwitchListener {
        void onScreenSwitched(int i);
    }

    public RealViewSwitcher(Context context) {
        super(context);
        init();
    }

    public RealViewSwitcher(Context context, AttributeSet attrs) {
        super(context, attrs);
        init();
    }

    private void init() {
        this.mScroller = new Scroller(getContext());
        ViewConfiguration configuration = ViewConfiguration.get(getContext());
        this.mTouchSlop = configuration.getScaledTouchSlop();
        this.mMaximumVelocity = configuration.getScaledMaximumFlingVelocity();
    }

    /* access modifiers changed from: protected */
    public void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
        super.onMeasure(widthMeasureSpec, heightMeasureSpec);
        int width = MeasureSpec.getSize(widthMeasureSpec);
        int mode = MeasureSpec.getMode(widthMeasureSpec);
        int mode2 = MeasureSpec.getMode(heightMeasureSpec);
        int count = getChildCount();
        for (int i = 0; i < count; i++) {
            getChildAt(i).measure(widthMeasureSpec, heightMeasureSpec);
        }
        if (this.mFirstLayout) {
            scrollTo(this.mCurrentScreen * width, 0);
            this.mFirstLayout = false;
        }
    }

    /* access modifiers changed from: protected */
    public void onLayout(boolean changed, int l, int t, int r, int b) {
        int childLeft = 0;
        int count = getChildCount();
        for (int i = 0; i < count; i++) {
            View child = getChildAt(i);
            if (child.getVisibility() != 8) {
                int childWidth = child.getMeasuredWidth();
                child.layout(childLeft, 0, childLeft + childWidth, child.getMeasuredHeight());
                childLeft += childWidth;
            }
        }
    }

    public boolean onTouchEvent(MotionEvent ev) {
        boolean xMoved;
        int availableToScroll;
        int i = 0;
        if (this.mVelocityTracker == null) {
            this.mVelocityTracker = VelocityTracker.obtain();
        }
        this.mVelocityTracker.addMovement(ev);
        int action = ev.getAction();
        float x = ev.getX();
        switch (action) {
            case 0:
                if (!this.mScroller.isFinished()) {
                    this.mScroller.abortAnimation();
                }
                this.mLastMotionX = x;
                if (!this.mScroller.isFinished()) {
                    i = 1;
                }
                this.mTouchState = i;
                break;
            case 1:
                if (this.mTouchState == 1) {
                    VelocityTracker velocityTracker = this.mVelocityTracker;
                    velocityTracker.computeCurrentVelocity(1000, (float) this.mMaximumVelocity);
                    int velocityX = (int) velocityTracker.getXVelocity();
                    if (velocityX > 1000 && this.mCurrentScreen > 0) {
                        snapToScreen(this.mCurrentScreen - 1);
                    } else if (velocityX >= -1000 || this.mCurrentScreen >= getChildCount() - 1) {
                        snapToDestination();
                    } else {
                        snapToScreen(this.mCurrentScreen + 1);
                    }
                    if (this.mVelocityTracker != null) {
                        this.mVelocityTracker.recycle();
                        this.mVelocityTracker = null;
                    }
                } else {
                    switchToScreen(this.mCurrentScreen + 1);
                }
                this.mTouchState = 0;
                break;
            case 2:
                if (((int) Math.abs(x - this.mLastMotionX)) > this.mTouchSlop) {
                    xMoved = true;
                } else {
                    xMoved = false;
                }
                if (xMoved) {
                    this.mTouchState = 1;
                }
                if (this.mTouchState == 1) {
                    int deltaX = (int) (this.mLastMotionX - x);
                    this.mLastMotionX = x;
                    int scrollX = getScrollX();
                    if (deltaX >= 0) {
                        if (deltaX > 0 && (availableToScroll = (getChildAt(getChildCount() - 1).getRight() - scrollX) - getWidth()) > 0) {
                            scrollBy(Math.min(availableToScroll, deltaX), 0);
                            break;
                        }
                    } else if (scrollX > 0) {
                        scrollBy(Math.max(-scrollX, deltaX), 0);
                        break;
                    }
                }
                break;
            case 3:
                this.mTouchState = 0;
                break;
        }
        return true;
    }

    private void switchToScreen(int whichScreen) {
        if (!this.mScroller.isFinished()) {
            this.mScroller.abortAnimation();
        }
        int whichScreen2 = Math.max(0, Math.min(whichScreen, getChildCount() - 1));
        this.mNextScreen = whichScreen2;
        this.mScroller.setFinalX(whichScreen2 * getWidth());
        invalidate();
    }

    private void snapToDestination() {
        int screenWidth = getWidth();
        snapToScreen((getScrollX() + (screenWidth / 2)) / screenWidth);
    }

    private void snapToScreen(int whichScreen) {
        if (this.mScroller.isFinished()) {
            int whichScreen2 = Math.max(0, Math.min(whichScreen, getChildCount() - 1));
            this.mNextScreen = whichScreen2;
            int delta = (whichScreen2 * getWidth()) - getScrollX();
            this.mScroller.startScroll(getScrollX(), 0, delta, 0, Math.abs(delta) * 2);
            invalidate();
        }
    }

    public void computeScroll() {
        if (this.mScroller.computeScrollOffset()) {
            scrollTo(this.mScroller.getCurrX(), this.mScroller.getCurrY());
            postInvalidate();
        } else if (this.mNextScreen != -1) {
            this.mCurrentScreen = Math.max(0, Math.min(this.mNextScreen, getChildCount() - 1));
            if (this.mOnScreenSwitchListener != null) {
                this.mOnScreenSwitchListener.onScreenSwitched(this.mCurrentScreen);
            }
            this.mNextScreen = -1;
        }
    }

    public int getCurrentScreen() {
        return this.mCurrentScreen;
    }

    public void setCurrentScreen(int currentScreen) {
        this.mCurrentScreen = Math.max(0, Math.min(currentScreen, getChildCount() - 1));
        scrollTo(this.mCurrentScreen * getWidth(), 0);
        invalidate();
    }

    public void setOnScreenSwitchListener(OnScreenSwitchListener onScreenSwitchListener) {
        this.mOnScreenSwitchListener = onScreenSwitchListener;
    }
}
