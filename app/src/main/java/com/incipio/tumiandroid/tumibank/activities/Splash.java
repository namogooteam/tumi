package com.incipio.tumiandroid.tumibank.activities;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Typeface;
import android.os.Bundle;
import android.os.Handler;
import android.widget.TextView;
import com.crashlytics.android.Crashlytics;
import com.incipio.tumiandroid.R;
import io.fabric.sdk.android.Fabric;

public class Splash extends Activity {
    /* access modifiers changed from: protected */
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Fabric.with(this, new Crashlytics());
        setContentView(R.layout.splash);
        ((TextView) findViewById(R.id.copywrite_text)).setTypeface(Typeface.createFromAsset(getAssets(), "fonts/gotham_bold.otf"));
        new Handler().postDelayed(new Runnable() {
            public void run() {
                Splash.this.finish();
                Splash.this.startActivity(new Intent(Splash.this, AcceptPolicy.class));
            }
        }, 2000);
    }
}
