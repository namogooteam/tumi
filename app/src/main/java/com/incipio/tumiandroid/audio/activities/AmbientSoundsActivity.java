package com.incipio.tumiandroid.audio.activities;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.Typeface;
import android.graphics.drawable.AnimationDrawable;
import android.media.MediaPlayer;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.TranslateAnimation;
import android.widget.AdapterView;
import android.widget.BaseAdapter;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;

import com.google.android.gms.drive.DriveFile;
import com.incipio.tumiandroid.R;
import com.incipio.tumiandroid.activities.WebPageActivity;
import com.incipio.tumiandroid.adapters.MenuListViewAdapter;
import com.incipio.tumiandroid.tumibank.activities.DeviceScanActivity;
import com.incipio.tumiandroid.tumibank.activities.WebActivity;
import com.incipio.tumiandroid.utils.AnimParams;
//import com.spotify.sdk.android.player.PlayerState;
//import com.spotify.sdk.android.player.PlayerStateCallback;
//import kaaes.spotify.webapi.samplesearch.AddAlbumFragment1;
//import kaaes.spotify.webapi.samplesearch.MediaPlayerService;

public class AmbientSoundsActivity extends Activity implements View.OnClickListener, Animation.AnimationListener {
    private static final long ONE_SECOND = 1000;
    public static MediaPlayer mp;
    /* access modifiers changed from: private */
    public static ImageView play;
    static int pos = -1;
    private ImageView ambient_sounds_img;
    private LinearLayout ambient_sounds_ll;
    private TextView ambient_sounds_txt;
    private AnimParams animParams = new AnimParams();
    private LinearLayout back;
    private LinearLayout backLayout_operations;
    /* access modifiers changed from: private */
    public CustomAdapter customAdapter;
    private FrameLayout frontLayout_operations;
    private TextView header;
    /* access modifiers changed from: private */
    public boolean isMenuSelected = false;
    private ListView list_ambient_sounds;
    private LinearLayout mask_ll;
    private LinearLayout menu;
    private ListView menuListView;
    private boolean menuOut = false;
    private ImageView play_music_img;
    private LinearLayout play_music_ll;
    private TextView play_music_txt;
    private TextView privacy;
    /* access modifiers changed from: private */
    public int selectedPositionFromMenu;
    private TextView settings;
    private String[] sounds = {"Ocean Waves", "Rainforest", "Thunderstorm", "Rainfall", "Country Garden"};
    private int stop_playback = 0;
    private TextView version;

    /* access modifiers changed from: protected */
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.ambient_sounds_layout);
        topBarInit();
        BottomBarInit();
        this.backLayout_operations = (LinearLayout) findViewById(R.id.backLayout_operations);
        this.frontLayout_operations = (FrameLayout) findViewById(R.id.frontLayout_operations);
        this.menuListView = (ListView) findViewById(R.id.menuListView);
        this.menuListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            public void onItemClick(AdapterView<?> adapterView, View arg1, int position, long arg3) {
                boolean unused = AmbientSoundsActivity.this.isMenuSelected = true;
                int unused2 = AmbientSoundsActivity.this.selectedPositionFromMenu = position;
                AmbientSoundsActivity.this.startAnimation();
            }
        });
        this.mask_ll = (LinearLayout) findViewById(R.id.mask_ll);
        this.mask_ll.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                AmbientSoundsActivity.this.startAnimation();
            }
        });
        this.list_ambient_sounds = (ListView) findViewById(R.id.listabmient);
        this.customAdapter = new CustomAdapter(this, this.sounds);
        this.list_ambient_sounds.setAdapter(this.customAdapter);
        play = (ImageView) findViewById(R.id.play);
        if (mp == null || !mp.isPlaying()) {
            play.setBackgroundResource(R.drawable.play_ambient);
        } else {
            play.setBackgroundResource(R.drawable.pause_ambient);
        }
        play.setOnClickListener(new View.OnClickListener() {
            public void onClick(View arg0) {
                /*
                if (AmbientSoundsActivity.mp == null) {
                    AmbientSoundsActivity.mp = MediaPlayer.create(AmbientSoundsActivity.this, R.raw.ocean_waves);
                    AmbientSoundsActivity.pos = 0;
                    MediaPlayerService mediaPlayerService = AddAlbumFragment1.mediaPlayerService;
                    if (MediaPlayerService.player != null) {
                        MediaPlayerService mediaPlayerService2 = AddAlbumFragment1.mediaPlayerService;
                        if (MediaPlayerService.player.isPlaying()) {
                            System.out.println("Reseting.....");
                            MediaPlayerService mediaPlayerService3 = AddAlbumFragment1.mediaPlayerService;
                            MediaPlayerService.player.pause();
                            AmbientSoundsActivity.this.playSong();
                        }
                    }
                    MediaPlayerService mediaPlayerService4 = AddAlbumFragment1.mediaPlayerService;
                    if (MediaPlayerService.mPlayer != null) {
                        try {
                            MediaPlayerService mediaPlayerService5 = AddAlbumFragment1.mediaPlayerService;
                            MediaPlayerService.mPlayer.getPlayerState(new PlayerStateCallback() {
                                public void onPlayerState(PlayerState playerState) {
                                    if (playerState.playing) {
                                        Log.v("TEST", " playerState.playing======" + playerState.playing);
                                        MediaPlayerService mediaPlayerService = AddAlbumFragment1.mediaPlayerService;
                                        MediaPlayerService.mPlayer.pause();
                                    }
                                }
                            });
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }
                    AmbientSoundsActivity.this.playSong();
                } else if (AmbientSoundsActivity.mp != null) {
                    MediaPlayerService mediaPlayerService6 = AddAlbumFragment1.mediaPlayerService;
                    if (MediaPlayerService.player.isPlaying()) {
                        System.out.println("Reseting.....");
                        MediaPlayerService mediaPlayerService7 = AddAlbumFragment1.mediaPlayerService;
                        MediaPlayerService.player.pause();
                    } else {
                        MediaPlayerService mediaPlayerService8 = AddAlbumFragment1.mediaPlayerService;
                        if (MediaPlayerService.mPlayer != null) {
                            try {
                                MediaPlayerService mediaPlayerService9 = AddAlbumFragment1.mediaPlayerService;
                                MediaPlayerService.mPlayer.getPlayerState(new PlayerStateCallback() {
                                    public void onPlayerState(PlayerState playerState) {
                                        if (playerState.playing) {
                                            Log.v("TEST", " playerState.playing======" + playerState.playing);
                                            MediaPlayerService mediaPlayerService = AddAlbumFragment1.mediaPlayerService;
                                            MediaPlayerService.mPlayer.pause();
                                        }
                                    }
                                });
                            } catch (Exception e2) {
                                e2.printStackTrace();
                            }
                        }
                    }
                    if (AmbientSoundsActivity.mp.isPlaying()) {
                        AmbientSoundsActivity.mp.pause();
                        AmbientSoundsActivity.this.customAdapter.notifyDataSetChanged();
                        AmbientSoundsActivity.play.setBackgroundResource(R.drawable.play_ambient);
                        return;
                    }
                    if (AmbientSoundsActivity.pos == 0) {
                        AmbientSoundsActivity.mp = MediaPlayer.create(AmbientSoundsActivity.this, R.raw.ocean_waves);
                        AmbientSoundsActivity.pos = 0;
                        AmbientSoundsActivity.this.playSong();
                    } else if (AmbientSoundsActivity.pos == 1) {
                        AmbientSoundsActivity.mp = MediaPlayer.create(AmbientSoundsActivity.this, R.raw.rainforest);
                        AmbientSoundsActivity.pos = 1;
                        AmbientSoundsActivity.this.playSong();
                    } else if (AmbientSoundsActivity.pos == 2) {
                        AmbientSoundsActivity.mp = MediaPlayer.create(AmbientSoundsActivity.this, R.raw.thunderstorm);
                        AmbientSoundsActivity.pos = 2;
                        AmbientSoundsActivity.this.playSong();
                    } else if (AmbientSoundsActivity.pos == 3) {
                        AmbientSoundsActivity.mp = MediaPlayer.create(AmbientSoundsActivity.this, R.raw.rainfall);
                        AmbientSoundsActivity.pos = 3;
                        AmbientSoundsActivity.this.playSong();
                    } else {
                        AmbientSoundsActivity.mp = MediaPlayer.create(AmbientSoundsActivity.this, R.raw.country_garden);
                        AmbientSoundsActivity.pos = 4;
                        AmbientSoundsActivity.this.playSong();
                    }
                    Log.v("TEST", " not null " + AmbientSoundsActivity.mp.isPlaying());
                }
                 */
            }
        });
        this.list_ambient_sounds.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            public void onItemClick(AdapterView<?> adapterView, View view, int position, long id) {
                AmbientSoundsActivity.this.cancelPlayer();
                switch (position) {
                    case 0:
                        AmbientSoundsActivity.mp = MediaPlayer.create(AmbientSoundsActivity.this, R.raw.ocean_waves);
                        AmbientSoundsActivity.pos = 0;
                        AmbientSoundsActivity.this.playSong();
                        return;
                    case 1:
                        AmbientSoundsActivity.mp = MediaPlayer.create(AmbientSoundsActivity.this, R.raw.rainforest);
                        AmbientSoundsActivity.pos = 1;
                        AmbientSoundsActivity.this.playSong();
                        return;
                    case 2:
                        AmbientSoundsActivity.mp = MediaPlayer.create(AmbientSoundsActivity.this, R.raw.thunderstorm);
                        AmbientSoundsActivity.pos = 2;
                        AmbientSoundsActivity.this.playSong();
                        return;
                    case 3:
                        AmbientSoundsActivity.mp = MediaPlayer.create(AmbientSoundsActivity.this, R.raw.rainfall);
                        AmbientSoundsActivity.pos = 3;
                        AmbientSoundsActivity.this.playSong();
                        return;
                    case 4:
                        AmbientSoundsActivity.mp = MediaPlayer.create(AmbientSoundsActivity.this, R.raw.country_garden);
                        AmbientSoundsActivity.pos = 4;
                        AmbientSoundsActivity.this.playSong();
                        return;
                    default:
                        return;
                }
            }
        });
    }

    /* access modifiers changed from: protected */
    public void onResume() {
        super.onResume();
        if (mp == null || !mp.isPlaying()) {
            play.setBackgroundResource(R.drawable.play_ambient);
        } else {
            play.setBackgroundResource(R.drawable.pause_ambient);
        }
        this.menuListView.setAdapter(new MenuListViewAdapter(this, 10));
    }

    public void playSong() {
        try {
            mp.setLooping(true);
            mp.start();
            play.setBackgroundResource(R.drawable.pause_ambient);
            this.customAdapter.notifyDataSetChanged();
        } catch (IllegalArgumentException e) {
            e.printStackTrace();
        } catch (IllegalStateException e2) {
            e2.printStackTrace();
        }
    }

    public static void stopSong() {
        try {
            mp.setLooping(false);
            mp.pause();
            play.setBackgroundResource(R.drawable.play_ambient);
        } catch (IllegalArgumentException e) {
            e.printStackTrace();
        } catch (IllegalStateException e2) {
            e2.printStackTrace();
        }
    }

    public void cancelPlayer() {
        try {
            if (mp.isPlaying()) {
                mp.stop();
            }
            mp.release();
            mp = null;
        } catch (Exception e) {
        }
        /*
        try {
            Log.v("TEST", "00000000000000000000000000000000000000");
            MediaPlayerService mediaPlayerService = AddAlbumFragment1.mediaPlayerService;
            if (MediaPlayerService.player.isPlaying()) {
                System.out.println("Reseting.....");
                MediaPlayerService mediaPlayerService2 = AddAlbumFragment1.mediaPlayerService;
                MediaPlayerService.player.pause();
            } else {
                MediaPlayerService mediaPlayerService3 = AddAlbumFragment1.mediaPlayerService;
                if (MediaPlayerService.mPlayer != null) {
                    try {
                        MediaPlayerService mediaPlayerService4 = AddAlbumFragment1.mediaPlayerService;
                        MediaPlayerService.mPlayer.getPlayerState(new PlayerStateCallback() {
                            public void onPlayerState(PlayerState playerState) {
                                if (playerState.playing) {
                                    Log.v("TEST", " playerState.playing======" + playerState.playing);
                                    MediaPlayerService mediaPlayerService = AddAlbumFragment1.mediaPlayerService;
                                    MediaPlayerService.mPlayer.pause();
                                }
                            }
                        });
                    } catch (Exception e2) {
                        e2.printStackTrace();
                    }
                }
            }
        } catch (Exception e3) {
        }

         */
        if (this.stop_playback == 0) {
            this.stop_playback = 1;
        }
    }

    private void topBarInit() {
        this.header = (TextView) findViewById(R.id.header);
        this.header.setText(getResources().getString(R.string.ambient_sounds));
        this.settings = (TextView) findViewById(R.id.settings);
        this.settings.setVisibility(4);
        this.back = (LinearLayout) findViewById(R.id.back);
        this.back.setVisibility(8);
        this.menu = (LinearLayout) findViewById(R.id.menu);
        this.menu.setVisibility(0);
        this.menu.setOnClickListener(this);
    }

    private void BottomBarInit() {
        this.play_music_ll = (LinearLayout) findViewById(R.id.play_music_ll);
        this.ambient_sounds_ll = (LinearLayout) findViewById(R.id.ambient_sounds_ll);
        this.play_music_txt = (TextView) findViewById(R.id.play_music_txt);
        this.ambient_sounds_txt = (TextView) findViewById(R.id.ambient_sounds_txt);
        this.play_music_img = (ImageView) findViewById(R.id.play_music_img);
        this.ambient_sounds_img = (ImageView) findViewById(R.id.ambient_sounds_img);
        this.play_music_img.setBackgroundResource(R.drawable.play_music_black);
        this.ambient_sounds_img.setBackgroundResource(R.drawable.ambient_sounds);
        this.play_music_txt.setTextColor(getResources().getColor(R.color.black_color));
        this.ambient_sounds_txt.setTextColor(getResources().getColor(R.color.red_color));
        this.play_music_ll.setOnClickListener(this);
        this.ambient_sounds_ll.setOnClickListener(this);
    }

    public void startAnimation() {
        Animation anim;
        int w = this.frontLayout_operations.getMeasuredWidth();
        int h = this.frontLayout_operations.getMeasuredHeight();
        int left = (int) (((double) this.frontLayout_operations.getMeasuredWidth()) * 0.5d);
        if (!this.menuOut) {
            anim = new TranslateAnimation(0.0f, (float) left, 0.0f, 0.0f);
            this.backLayout_operations.setVisibility(0);
            this.mask_ll.setVisibility(0);
            this.animParams.init(left, 0, left + w, h);
            anim.setDuration(300);
        } else {
            anim = new TranslateAnimation(0.0f, (float) (-left), 0.0f, 0.0f);
            this.animParams.init(0, 0, w, h);
            anim.setDuration(10);
        }
        anim.setAnimationListener(this);
        anim.setFillAfter(true);
        this.frontLayout_operations.startAnimation(anim);
    }

    /* access modifiers changed from: package-private */
    public void layoutApp(boolean menuOut2) {
        this.frontLayout_operations.layout(this.animParams.left, this.animParams.top, this.animParams.right, this.animParams.bottom);
        this.frontLayout_operations.clearAnimation();
    }

    public void onAnimationStart(Animation animation) {
    }

    public void onAnimationEnd(Animation animation) {
        this.menuOut = !this.menuOut;
        if (!this.menuOut) {
            this.backLayout_operations.setVisibility(4);
            this.mask_ll.setVisibility(8);
        }
        layoutApp(this.menuOut);
        if (this.isMenuSelected) {
            this.isMenuSelected = false;
            if (this.selectedPositionFromMenu == 0) {
                this.menuListView.setAdapter(new MenuListViewAdapter(this, 0));
                if (ContextCompat.checkSelfPermission(this, "android.permission.ACCESS_FINE_LOCATION") != 0) {
                    ActivityCompat.requestPermissions(this, new String[]{"android.permission.ACCESS_FINE_LOCATION"}, 103);
                    return;
                }
                Intent intent = new Intent(this, DeviceScanActivity.class);
                intent.setFlags(DriveFile.MODE_READ_ONLY);
                intent.setFlags(67108864);
                startActivity(intent);
            } else if (this.selectedPositionFromMenu == 1) {
                this.menuListView.setAdapter(new MenuListViewAdapter(this, 1));
                //Intent intent2 = new Intent(this, AddAlbumFragment1.class);
                //intent2.setFlags(DriveFile.MODE_READ_ONLY);
                //intent2.setFlags(67108864);
                //startActivity(intent2);
            } else if (this.selectedPositionFromMenu == 2) {
                this.menuListView.setAdapter(new MenuListViewAdapter(this, 2));
                Intent intent3 = new Intent(this, WebPageActivity.class);
                intent3.setFlags(DriveFile.MODE_READ_ONLY);
                intent3.setFlags(67108864);
                intent3.putExtra("PageUrl", "CustomerService");
                startActivity(intent3);
            } else if (this.selectedPositionFromMenu == 3) {
                this.menuListView.setAdapter(new MenuListViewAdapter(this, 3));
                Intent intent4 = new Intent(this, WebPageActivity.class);
                intent4.setFlags(DriveFile.MODE_READ_ONLY);
                intent4.setFlags(67108864);
                intent4.putExtra("PageUrl", "About");
                startActivity(intent4);
            } else if (this.selectedPositionFromMenu == 4) {
                this.menuListView.setAdapter(new MenuListViewAdapter(this, 4));
                Intent intent5 = new Intent(this, WebPageActivity.class);
                intent5.setFlags(DriveFile.MODE_READ_ONLY);
                intent5.setFlags(67108864);
                intent5.putExtra("PageUrl", "StoreLocator");
                startActivity(intent5);
            } else if (this.selectedPositionFromMenu == 5) {
                this.menuListView.setAdapter(new MenuListViewAdapter(this, 5));
                Intent intent6 = new Intent(this, WebPageActivity.class);
                intent6.setFlags(DriveFile.MODE_READ_ONLY);
                intent6.setFlags(67108864);
                intent6.putExtra("PageUrl", "Shop");
                startActivity(intent6);
            } else if (this.selectedPositionFromMenu == 10) {
                Intent intent7 = new Intent(this, WebActivity.class);
                intent7.setFlags(DriveFile.MODE_READ_ONLY);
                intent7.setFlags(67108864);
                startActivity(intent7);
            }
        }
    }

    public void onAnimationRepeat(Animation animation) {
    }

    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.play_music_ll /*2131558546*/:
                finish();
                return;
            case R.id.menu /*2131558733*/:
                startAnimation();
                return;
            default:
                return;
        }
    }

    private class CustomAdapter extends BaseAdapter {
        private Context context;
        private String[] sounds;

        public CustomAdapter(Context context2, String[] sounds2) {
            this.context = context2;
            this.sounds = sounds2;
        }

        public int getCount() {
            return this.sounds.length;
        }

        public Object getItem(int position) {
            return this.sounds[position];
        }

        public long getItemId(int position) {
            return 0;
        }

        public View getView(int position, View convertView, ViewGroup parent) {
            View rowView = ((LayoutInflater) this.context.getSystemService("layout_inflater")).inflate(R.layout.list_item_spotify, (ViewGroup) null);
            TextView name = (TextView) rowView.findViewById(R.id.txt);
            ImageView img = (ImageView) rowView.findViewById(R.id.img);
            ImageView img1 = (ImageView) rowView.findViewById(R.id.img1);
            name.setTypeface(Typeface.createFromAsset(AmbientSoundsActivity.this.getAssets(), "fonts/futura_medium.ttf"));
            AnimationDrawable animation = new AnimationDrawable();
            animation.addFrame(AmbientSoundsActivity.this.getResources().getDrawable(R.drawable.volume_1), 1000);
            animation.addFrame(AmbientSoundsActivity.this.getResources().getDrawable(R.drawable.volume_2), 1000);
            animation.addFrame(AmbientSoundsActivity.this.getResources().getDrawable(R.drawable.volume_3), 1000);
            animation.setOneShot(false);
            img.setBackgroundDrawable(animation);
            animation.start();
            if (AmbientSoundsActivity.pos != position) {
                img.setVisibility(4);
                img1.setVisibility(4);
            } else if (AmbientSoundsActivity.mp == null || !AmbientSoundsActivity.mp.isPlaying()) {
                Log.v("TAG", "isPlay========================else");
                img.setVisibility(8);
                img1.setVisibility(0);
            } else {
                Log.v("TAG", "isPlay========================" + AmbientSoundsActivity.mp.isPlaying());
                img.setVisibility(0);
                img1.setVisibility(8);
            }
            name.setText(this.sounds[position]);
            return rowView;
        }
    }

    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        switch (requestCode) {
            case 103:
                if (grantResults.length <= 0 || grantResults[0] != 0) {
                    Toast.makeText(this, "Please grant app specific Location permission to use TUMI", 0).show();
                    return;
                }
                Intent intent = new Intent(this, DeviceScanActivity.class);
                intent.setFlags(DriveFile.MODE_READ_ONLY);
                intent.setFlags(67108864);
                startActivity(intent);
                return;
            default:
                return;
        }
    }
}
