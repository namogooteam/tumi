package com.incipio.tumiandroid.audio.dragndrop;

public interface DropListener {
    void onDrop(int i, int i2);
}
