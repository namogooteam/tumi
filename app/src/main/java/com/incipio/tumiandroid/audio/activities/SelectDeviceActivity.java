package com.incipio.tumiandroid.audio.activities;

import android.app.Activity;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.text.Html;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.TranslateAnimation;
import android.widget.AdapterView;
import android.widget.FrameLayout;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;

import com.google.android.gms.drive.DriveFile;
import com.incipio.tumiandroid.R;
import com.incipio.tumiandroid.activities.WebPageActivity;
import com.incipio.tumiandroid.adapters.MenuListViewAdapter;
import com.incipio.tumiandroid.tumibank.activities.DeviceScanActivity;
import com.incipio.tumiandroid.tumibank.activities.WebActivity;
import com.incipio.tumiandroid.utils.AnimParams;
//import kaaes.spotify.webapi.samplesearch.AddAlbumFragment1;

public class SelectDeviceActivity extends Activity implements View.OnClickListener, Animation.AnimationListener {
    private AnimParams animParams = new AnimParams();
    private LinearLayout back;
    private LinearLayout backLayout_operations;
    private FrameLayout frontLayout_operations;
    private TextView header;
    private boolean isInbackground = false;
    /* access modifiers changed from: private */
    public boolean isMenuSelected = false;
    private LinearLayout mask_ll;
    private LinearLayout menu;
    private ListView menuListView;
    private boolean menuOut = false;
    private LinearLayout portable_audio_ll;
    private LinearLayout portable_power_bank_ll;
    private TextView privacy;
    /* access modifiers changed from: private */
    public int selectedPositionFromMenu;
    private TextView settings;
    private TextView version;

    /* access modifiers changed from: protected */
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.select_device_layout);
        topBarInit();
        this.backLayout_operations = (LinearLayout) findViewById(R.id.backLayout_operations);
        this.frontLayout_operations = (FrameLayout) findViewById(R.id.frontLayout_operations);
        this.menuListView = (ListView) findViewById(R.id.menuListView);
        this.menuListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            public void onItemClick(AdapterView<?> adapterView, View arg1, int position, long arg3) {
                boolean unused = SelectDeviceActivity.this.isMenuSelected = true;
                int unused2 = SelectDeviceActivity.this.selectedPositionFromMenu = position;
                SelectDeviceActivity.this.startAnimation();
            }
        });
        this.mask_ll = (LinearLayout) findViewById(R.id.mask_ll);
        this.mask_ll.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                SelectDeviceActivity.this.startAnimation();
            }
        });
        this.privacy = (TextView) findViewById(R.id.privacypolicy);
        this.privacy.setText(Html.fromHtml("<u>Privacy Policy</u>"));
        this.privacy.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                boolean unused = SelectDeviceActivity.this.isMenuSelected = true;
                int unused2 = SelectDeviceActivity.this.selectedPositionFromMenu = 10;
                SelectDeviceActivity.this.startAnimation();
            }
        });
        String versionName = null;
        try {
            versionName = getPackageManager().getPackageInfo(getPackageName(), 0).versionName;
        } catch (PackageManager.NameNotFoundException e) {
            e.printStackTrace();
        }
        this.version = (TextView) findViewById(R.id.versionnumber);
        this.version.setText("v " + versionName);
        this.portable_power_bank_ll = (LinearLayout) findViewById(R.id.portable_power_bank_ll);
        this.portable_audio_ll = (LinearLayout) findViewById(R.id.portable_audio_ll);
        this.portable_power_bank_ll.setOnClickListener(this);
        this.portable_audio_ll.setOnClickListener(this);
    }

    /* access modifiers changed from: protected */
    public void onResume() {
        super.onResume();
        this.menuListView.setAdapter(new MenuListViewAdapter(this, 10));
    }

    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.menu /*2131558733*/:
                startAnimation();
                return;
            case R.id.portable_power_bank_ll /*2131558805*/:
                if (ContextCompat.checkSelfPermission(this, "android.permission.ACCESS_FINE_LOCATION") != 0) {
                    ActivityCompat.requestPermissions(this, new String[]{"android.permission.ACCESS_FINE_LOCATION"}, 103);
                    return;
                }
                Intent intent = new Intent(this, DeviceScanActivity.class);
                intent.setFlags(DriveFile.MODE_READ_ONLY);
                intent.setFlags(67108864);
                startActivity(intent);
                return;
            case R.id.portable_audio_ll /*2131558808*/:
                //Intent intent1 = new Intent(this, AddAlbumFragment1.class);
                //intent1.setFlags(DriveFile.MODE_READ_ONLY);
                //intent1.setFlags(67108864);
                //startActivity(intent1);
                return;
            default:
                return;
        }
    }

    public void onAnimationStart(Animation animation) {
    }

    public void onAnimationEnd(Animation animation) {
        this.menuOut = !this.menuOut;
        if (!this.menuOut) {
            this.backLayout_operations.setVisibility(4);
            this.mask_ll.setVisibility(8);
        }
        layoutApp(this.menuOut);
        if (this.isMenuSelected) {
            this.isMenuSelected = false;
            if (this.selectedPositionFromMenu == 0) {
                this.menuListView.setAdapter(new MenuListViewAdapter(this, 0));
                if (ContextCompat.checkSelfPermission(this, "android.permission.ACCESS_FINE_LOCATION") != 0) {
                    ActivityCompat.requestPermissions(this, new String[]{"android.permission.ACCESS_FINE_LOCATION"}, 103);
                    return;
                }
                Intent intent = new Intent(this, DeviceScanActivity.class);
                intent.setFlags(DriveFile.MODE_READ_ONLY);
                intent.setFlags(67108864);
                startActivity(intent);
            } else if (this.selectedPositionFromMenu == 1) {
                this.menuListView.setAdapter(new MenuListViewAdapter(this, 1));
                //Intent intent2 = new Intent(this, AddAlbumFragment1.class);
                //intent2.setFlags(DriveFile.MODE_READ_ONLY);
                //intent2.setFlags(67108864);
                //startActivity(intent2);
            } else if (this.selectedPositionFromMenu == 2) {
                this.menuListView.setAdapter(new MenuListViewAdapter(this, 2));
                Intent intent3 = new Intent(this, WebPageActivity.class);
                intent3.setFlags(DriveFile.MODE_READ_ONLY);
                intent3.setFlags(67108864);
                intent3.putExtra("PageUrl", "CustomerService");
                startActivity(intent3);
            } else if (this.selectedPositionFromMenu == 3) {
                this.menuListView.setAdapter(new MenuListViewAdapter(this, 3));
                Intent intent4 = new Intent(this, WebPageActivity.class);
                intent4.setFlags(DriveFile.MODE_READ_ONLY);
                intent4.setFlags(67108864);
                intent4.putExtra("PageUrl", "About");
                startActivity(intent4);
            } else if (this.selectedPositionFromMenu == 4) {
                this.menuListView.setAdapter(new MenuListViewAdapter(this, 4));
                Intent intent5 = new Intent(this, WebPageActivity.class);
                intent5.setFlags(DriveFile.MODE_READ_ONLY);
                intent5.setFlags(67108864);
                intent5.putExtra("PageUrl", "StoreLocator");
                startActivity(intent5);
            } else if (this.selectedPositionFromMenu == 5) {
                this.menuListView.setAdapter(new MenuListViewAdapter(this, 5));
                Intent intent6 = new Intent(this, WebPageActivity.class);
                intent6.setFlags(DriveFile.MODE_READ_ONLY);
                intent6.setFlags(67108864);
                intent6.putExtra("PageUrl", "Shop");
                startActivity(intent6);
            } else if (this.selectedPositionFromMenu == 10) {
                Intent intent7 = new Intent(this, WebActivity.class);
                intent7.setFlags(DriveFile.MODE_READ_ONLY);
                intent7.setFlags(67108864);
                startActivity(intent7);
            }
        }
    }

    public void onAnimationRepeat(Animation animation) {
    }

    private void topBarInit() {
        this.header = (TextView) findViewById(R.id.header);
        this.header.setText(getResources().getString(R.string.select_device));
        this.settings = (TextView) findViewById(R.id.settings);
        this.settings.setVisibility(4);
        this.back = (LinearLayout) findViewById(R.id.back);
        this.back.setVisibility(8);
        this.menu = (LinearLayout) findViewById(R.id.menu);
        this.menu.setVisibility(0);
        this.menu.setOnClickListener(this);
    }

    public void startAnimation() {
        Animation anim;
        int w = this.frontLayout_operations.getMeasuredWidth();
        int h = this.frontLayout_operations.getMeasuredHeight();
        int left = (int) (((double) this.frontLayout_operations.getMeasuredWidth()) * 0.5d);
        if (!this.menuOut) {
            anim = new TranslateAnimation(0.0f, (float) left, 0.0f, 0.0f);
            this.backLayout_operations.setVisibility(0);
            this.mask_ll.setVisibility(0);
            this.animParams.init(left, 0, left + w, h);
            anim.setDuration(300);
        } else {
            anim = new TranslateAnimation(0.0f, (float) (-left), 0.0f, 0.0f);
            this.animParams.init(0, 0, w, h);
            anim.setDuration(10);
        }
        anim.setAnimationListener(this);
        anim.setFillAfter(true);
        this.frontLayout_operations.startAnimation(anim);
    }

    /* access modifiers changed from: package-private */
    public void layoutApp(boolean menuOut2) {
        this.frontLayout_operations.layout(this.animParams.left, this.animParams.top, this.animParams.right, this.animParams.bottom);
        this.frontLayout_operations.clearAnimation();
    }

    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        switch (requestCode) {
            case 103:
                if (grantResults.length <= 0 || grantResults[0] != 0) {
                    Toast.makeText(this, "Please grant app specific Location permission to use TUMI", 0).show();
                    return;
                }
                Intent intent = new Intent(this, DeviceScanActivity.class);
                intent.setFlags(DriveFile.MODE_READ_ONLY);
                intent.setFlags(67108864);
                startActivity(intent);
                return;
            default:
                return;
        }
    }
}
