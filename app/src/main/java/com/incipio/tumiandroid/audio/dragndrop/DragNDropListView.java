package com.incipio.tumiandroid.audio.dragndrop;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.drawable.Drawable;
import android.util.AttributeSet;
import android.view.MotionEvent;
import android.view.View;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.ListView;

public class DragNDropListView extends ListView {
    public static int move = 0;
    private DragListener mDragListener;
    private boolean mDragMode;
    private int mDragPointOffset;
    private ImageView mDragView;
    private DropListener mDropListener;
    private int mEndPosition;
    private RemoveListener mRemoveListener;
    private int mStartPosition;

    public DragNDropListView(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public void setDropListener(DropListener l) {
        this.mDropListener = l;
        move = 1;
    }

    public void setRemoveListener(RemoveListener l) {
        this.mRemoveListener = l;
        move = 1;
    }

    public void setDragListener(DragListener l) {
        this.mDragListener = l;
        move = 1;
    }

    public void removeDropListener() {
        this.mDropListener = null;
        move = 0;
    }

    public void removeRemoveListener() {
        this.mRemoveListener = null;
        move = 0;
    }

    public void removeDragListener() {
        this.mDragListener = null;
        move = 0;
    }

    public boolean onTouchEvent(MotionEvent ev) {
        int action = ev.getAction();
        int x = (int) ev.getX();
        int y = (int) ev.getY();
        if (action == 0 && ((double) x) > ((double) getWidth()) * 0.8d && x < getWidth() && move == 1) {
            this.mDragMode = true;
        }
        try {
            if (!this.mDragMode) {
                return super.onTouchEvent(ev);
            }
        } catch (Exception e) {
        }
        switch (action) {
            case 0:
                System.out.println("action down");
                this.mStartPosition = pointToPosition(x, y);
                if (this.mStartPosition == -1 || move != 1) {
                    return true;
                }
                int mItemPosition = this.mStartPosition - getFirstVisiblePosition();
                this.mDragPointOffset = y - getChildAt(mItemPosition).getTop();
                this.mDragPointOffset -= ((int) ev.getRawY()) - y;
                startDrag(mItemPosition, y);
                drag(0, y);
                return true;
            case 1:
                break;
            case 2:
                System.out.println("action move");
                if (move != 1) {
                    return true;
                }
                drag(0, y);
                return true;
            case 3:
                System.out.println("action cancel");
                break;
        }
        System.out.println("action up");
        System.out.println("action default");
        this.mDragMode = false;
        this.mEndPosition = pointToPosition(x, y);
        stopDrag(this.mStartPosition - getFirstVisiblePosition());
        if (this.mDropListener == null || this.mStartPosition == -1 || this.mEndPosition == -1) {
            return true;
        }
        this.mDropListener.onDrop(this.mStartPosition, this.mEndPosition);
        return true;
    }

    private void drag(int x, int y) {
        if (this.mDragView != null && move == 1) {
            WindowManager.LayoutParams layoutParams = (WindowManager.LayoutParams) this.mDragView.getLayoutParams();
            layoutParams.x = x;
            layoutParams.y = y - this.mDragPointOffset;
            ((WindowManager) getContext().getSystemService("window")).updateViewLayout(this.mDragView, layoutParams);
            if (this.mDragListener != null && move == 1) {
                this.mDragListener.onDrag(x, y, (ListView) null);
            }
        }
    }

    private void startDrag(int itemIndex, int y) {
        stopDrag(itemIndex);
        View item = getChildAt(itemIndex);
        if (item != null) {
            item.setDrawingCacheEnabled(true);
            if (this.mDragListener != null && move == 1) {
                this.mDragListener.onStartDrag(item);
            }
            Bitmap bitmap = Bitmap.createBitmap(item.getDrawingCache());
            WindowManager.LayoutParams mWindowParams = new WindowManager.LayoutParams();
            mWindowParams.gravity = 48;
            mWindowParams.x = 0;
            mWindowParams.y = y - this.mDragPointOffset;
            mWindowParams.height = -2;
            mWindowParams.width = -2;
            mWindowParams.flags = 920;
            mWindowParams.format = -3;
            mWindowParams.windowAnimations = 0;
            Context context = getContext();
            ImageView v = new ImageView(context);
            v.setImageBitmap(bitmap);
            ((WindowManager) context.getSystemService("window")).addView(v, mWindowParams);
            this.mDragView = v;
        }
    }

    private void stopDrag(int itemIndex) {
        if (this.mDragView != null) {
            if (this.mDragListener != null) {
                this.mDragListener.onStopDrag(getChildAt(itemIndex));
            }
            this.mDragView.setVisibility(8);
            ((WindowManager) getContext().getSystemService("window")).removeView(this.mDragView);
            this.mDragView.setImageDrawable((Drawable) null);
            this.mDragView = null;
        }
    }
}
