package com.incipio.tumiandroid.audio.dragndrop;

import android.view.View;
import android.widget.ListView;

public interface DragListener {
    void onDrag(int i, int i2, ListView listView);

    void onStartDrag(View view);

    void onStopDrag(View view);
}
