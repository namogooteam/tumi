package com.incipio.tumiandroid.audio.dragndrop;

public interface RemoveListener {
    void onRemove(int i);
}
